package com.tsystems.mymobile.ServicesMockTests;

import com.tsystems.mymobile.dao.api.TariffDAO;
import com.tsystems.mymobile.model.Option;
import com.tsystems.mymobile.model.Tariff;
import com.tsystems.mymobile.service.dto.option.OptionDtoConverter;
import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import com.tsystems.mymobile.service.dto.tariff.TariffAdminDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDtoConverter;
import com.tsystems.mymobile.service.impl.TariffServiceImpl;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import com.tsystems.mymobile.utils.jms.Sender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TariffServiceMockTests {

    private TariffDto tariffDto;

    private Tariff tariff;

    private TariffAdminDto tariffAdminDto;

    private Option option;

    private OptionSimpleDto optionSimpleDto;

    @Mock
    private TariffDtoConverter converter;

    @Mock
    private OptionDtoConverter converter1;

    @Mock
    private Sender sender;

    @Mock
    private TariffDAO tariffDAO;

    @InjectMocks
    private TariffServiceImpl tariffService;

    @Before
    public void setup() {

        option = new Option();
        optionSimpleDto = new OptionSimpleDto();

        tariffDto = new TariffDto();
        tariffDto.setId(1L);
        tariffDto.setPrice(30);
        tariffDto.setIsDeleted(true);
        tariffDto.setTittle("Tittle");

        tariffAdminDto = new TariffAdminDto();
        tariffAdminDto.setId(1L);
        tariffAdminDto.setPrice(30);
        tariffAdminDto.setIsDeleted(true);
        tariffAdminDto.setTittle("Tittle");

        tariff = new Tariff();
        tariff.setId(1L);
        tariff.setPrice(30);
        tariff.setIsDeleted(true);
        tariff.setTittle("Tittle");


    }

    @Test
    public void testMockGetActiveTariffs() {
        when(tariffDAO.getActiveTariffs()).thenReturn(new ArrayList<>());
        tariffService.getActiveTariffs();
        verify(tariffDAO).getActiveTariffs();
    }

    @Test
    public void testMockGetTariffArchive() {
        when(tariffDAO.getTariffArchive()).thenReturn(new ArrayList<>());
        tariffService.getTariffArchive();
        verify(tariffDAO).getTariffArchive();
    }

    @Test
    public void testMockGetSimpleTariffs() {
        when(tariffDAO.getActiveTariffs()).thenReturn(new ArrayList<>());
        tariffService.getSimpleTariffs();
        verify(tariffDAO).getActiveTariffs();
    }

    @Test
    public void testMockGetTariffById(){
        when(tariffDAO.getTariffById(1L)).thenReturn(tariff);
        when(converter.convertDtoToEntity(tariffDto)).thenReturn(tariff);
        tariffService.getTariffById(tariffDto.getId());
        verify(tariffDAO).getTariffById(tariffDto.getId());
    }

    @Test
    public void testMockGetTariffAdminById() {
        when(tariffDAO.getTariffById(1L)).thenReturn(tariff);
        when(converter.convertTariffToAdminDto(tariff)).thenReturn(tariffAdminDto);
        tariffService.getTariffAdminById(tariffAdminDto.getId());
        verify(tariffDAO).getTariffById(tariffAdminDto.getId());
    }

    @Test
    public void testMockGetEnableTariffs() {
        when(tariffDAO.getActiveTariffs()).thenReturn(new ArrayList<>());
        tariffService.getEnableTariffs(tariffDto.getTittle());
        verify(tariffDAO).getActiveTariffs();
    }

    @Test
    public void testMockCreateTariff() {
        when(tariffDAO.createTariff(tariff,option)).thenReturn(tariffDto.getId());
        when(converter.convertLightDtoToEntity(tariffDto)).thenReturn(tariff);
        when(converter1.convertOptionSimpleDtoToEntity(optionSimpleDto)).thenReturn(option);
        tariffService.createTariff(tariffDto, optionSimpleDto);
        verify(tariffDAO).createTariff(tariff,option);
    }



    @Test
    public void testMockRemoveTariffById() {
        when(tariffDAO.removeTariffById(1L)).thenReturn(true);
        tariffService.removeTariffById(tariff.getId());
        verify(tariffDAO).removeTariffById(tariff.getId());
    }

    @Test
    public void testMockUpdateTariff() {
        when(tariffDAO.updateTariff(tariff)).thenReturn(tariff);
        when(converter.convertLightDtoToEntity(tariffDto)).thenReturn(tariff);
        tariffService.updateTariff(tariffDto);
        verify(tariffDAO).updateTariff(tariff);
    }

    @Test
    public void testMockIsExist() throws DataBaseException {
        when(tariffDAO.getTariffByTittle(tariffDto.getTittle())).thenReturn(tariff);
        tariffService.isExist(tariffDto.getTittle());
        verify(tariffDAO).getTariffByTittle(tariff.getTittle());
    }

}
