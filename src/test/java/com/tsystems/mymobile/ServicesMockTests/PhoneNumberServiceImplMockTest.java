package com.tsystems.mymobile.ServicesMockTests;

import com.tsystems.mymobile.dao.api.PhoneNumberDAO;
import com.tsystems.mymobile.model.PhoneNumber;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;
import com.tsystems.mymobile.service.impl.PhoneNumberServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhoneNumberServiceImplMockTest {

    private PhoneNumberDto phoneNumberDto;

    private PhoneNumber phoneNumber;

    @Mock
    private BaseEntityDtoConverter<PhoneNumber,PhoneNumberDto> converter;

    @Mock
    private PhoneNumberDAO phoneNumberDAO;

    @InjectMocks
    private PhoneNumberServiceImpl phoneNumberServiceImpl;

    @Before
    public void setup() {
        phoneNumberDto = new PhoneNumberDto();
        phoneNumberDto.setId(9852550008L);
        phoneNumberDto.setIsDeleted(true);

        phoneNumber = new PhoneNumber();
        phoneNumber.setId(9852550008L);

    }

    @Test
    public void testMockGetFreeNumbers(){
        when(phoneNumberDAO.getFreeNumbers()).thenReturn(new ArrayList<>());
        phoneNumberServiceImpl.getFreeNumbers();
        verify(phoneNumberDAO).getFreeNumbers();
    }

    @Test
    public void testMockGetNumberById() throws Exception {
        when(phoneNumberDAO.getNumberById(phoneNumberDto.getId())).thenReturn(phoneNumber);
        phoneNumberServiceImpl.getNumberById(phoneNumber.getId());
        verify(phoneNumberDAO).getNumberById(phoneNumber.getId());
    }

    @Test
    public void testMockGetUsedNumbers() throws Exception {
        when(phoneNumberDAO.getUsedNumbers()).thenReturn(new ArrayList<>());
        phoneNumberServiceImpl.getUsedNumbers();
        verify(phoneNumberDAO).getUsedNumbers();
    }



}
