package com.tsystems.mymobile.ServicesMockTests;


import com.tsystems.mymobile.dao.api.UserDAO;
import com.tsystems.mymobile.model.User;
import com.tsystems.mymobile.service.dto.user.UserDto;
import com.tsystems.mymobile.service.dto.user.UserDtoConverter;
import com.tsystems.mymobile.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplMockTest {

    private UserDto userDto;

    private User user;

    @Mock
    private UserDtoConverter converter;

    @Mock
    private UserDAO userDAO;

    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setup() {
        userDto = new UserDto();
        userDto.setId(1L);
        userDto.setEmail("email@email.com");
        userDto.setPassword("1");
        userDto.setIsDeleted(true);
        userDto.setPassport("11");

        user = new User();
        user.setId(1L);
        user.setEmail("email@email.com");
        user.setPassword("1");
        user.setIsDeleted(true);
        user.setPassport("11");
    }

    @Test
    public void testMockGetAllUsers(){
        when(userDAO.getAllUsers()).thenReturn(new ArrayList<>());
        userService.getAllUsers();
        verify(userDAO).getAllUsers();
    }

    @Test
    public void testMockGetUserByEmail(){
        when(userDAO.getUserByEmail(userDto.getEmail())).thenReturn(user);
        when(converter.convertDtoToEntity(userDto)).thenReturn(user);
        userService.getUserByEmail(userDto.getEmail());
        verify(userDAO).getUserByEmail(userDto.getEmail());
    }

    @Test
    public void testMockGetUserById(){
        when(userDAO.getUserById(1L)).thenReturn(user);
        when(converter.convertDtoToEntity(userDto)).thenReturn(user);
        userService.getUserById(userDto.getId());
        verify(userDAO).getUserById(userDto.getId());
    }

    @Test
    public void testMockAuthorize(){
        when(userDAO.authorise("email@email.com","1")).thenReturn(user);
        when(converter.convertDtoToEntity(userDto)).thenReturn(user);
        userService.authorise(userDto);
        verify(userDAO).authorise(userDto.getEmail(), userDto.getPassword());

    }

    @Test
    public void testMockCreateUser(){
        when(userDAO.createUser(user)).thenReturn(1L);
        when(converter.convertDtoToEntity(userDto)).thenReturn(user);
        userService.createUser(userDto);
        verify(userDAO).createUser(user);
    }

    @Test
    public void testMockIsExistUser(){
        when(userDAO.isExistUser("email@email.com","11")).thenReturn(true);
        userService.isExist(userDto.getEmail(),userDto.getPassport());
        verify(userDAO).isExistUser(user.getEmail(),user.getPassport());
    }

    @Test
    public void testMockRemoveUserById(){
        when(userDAO.removeUserById(userDto.getId())).thenReturn(true);
        userService.removeUserById(userDto.getId());
        verify(userDAO).removeUserById(userDto.getId());
    }
}
