INSERT INTO `number` VALUES (9852550000,TRUE),(9852550001,TRUE),(9852550002,TRUE),(9852550003,TRUE), (9852550004,TRUE),(9852550005,TRUE),(9852550006,TRUE);

ALTER TABLE `number` AUTO_INCREMENT = 9852550000;

INSERT INTO `number` VALUE ();

INSERT INTO `user` VALUES (1,'SPb','1980-02-12','sergei@gmail.com','Sergei','4566 768590','123','Ivanov',TRUE ,2),
                          (2,'Moscow','1960-04-10','alex@bk.ru','Alex','4533 835467','334','Petrov',TRUE ,2),
                          (3,'Spb','1999-04-04','vasya@mail.ru','Vasilii','4466 758009','111','Sidorov',TRUE ,2),
                          (4,'Spb','1995-07-04','admin@mobilecom.com','Admin','4466 758569','000','Adminov',TRUE ,1),
                          (5,'Spb','1988-10-04','nick@list.ru','Nikolay','2233 755000','qqq','Nikolaev',TRUE ,2),
                          (5,'Spb','1986-10-14','kate@list.ru','Kate','4303 755590','aaa','Gruber',TRUE ,2);

INSERT INTO `tariff` VALUES (1,20,'Best_connection',true),(2,30,'All_inclusive',true)
                           ,(3,40,'Maximum',TRUE ),(4,50,'International',TRUE ),(5,100,'StrongFingers',TRUE );

INSERT INTO `options` VALUES (1,10,20,'Unlimited',1,TRUE ),
                             (2,20,20,'Home connection',2,TRUE),
                             (3,30,20,'Maximum internet',3,TRUE),
                             (4,40,20,'Medium connection',3,TRUE),
                             (5,50,30,'Fifth option',2,TRUE),
                             (6,60,30,'1000 SMS',1,TRUE),
                             (7,70,30,'Cheapest Roaming',4,TRUE),
                             (8,80,30,'Unlimited internet',2,TRUE),
                             (9,90,35,'Maximum minutes',3,TRUE),
                             (10,100,40,'100 Mb Internet',1,TRUE),
                             (11,99,20,'Black list',2,TRUE),
                             (12,50,25,'Holidays',4,TRUE),
                             (13,45,50,'Unlimited internet',5,TRUE),
                             (14,100,100,'Unlimited SMS',5,TRUE),
                             (15,55,90,'10 Gb',5,TRUE),
                              (16,30,30,'Holidays',5,TRUE);

INSERT INTO `contract` VALUES (1,9852550000,1,1,3),(2,9852550001,1,1,3),
                              (3,9852550002,2,3,3),(4,9852550003,2,2,3),(5,9852550004,3,3,3),
                              (6,9852550005,1,1,3),(7,9852550006,2,4,3),(8,9852550007,1,1,3),
                              (9,9852550008,4,1,3);

INSERT INTO `contract_options` VALUES (1,1);

INSERT INTO `disabled_options` VALUES (1,10),(10,1);

INSERT INTO `required_options` VALUES (3,4);