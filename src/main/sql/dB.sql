SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema mobilecompany
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mobilecompany`;

-- -----------------------------------------------------
-- Schema mobilecompany
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mobilecompany`
  DEFAULT CHARACTER SET utf8;
USE `mobilecompany`;

-- -----------------------------------------------------
-- Table `mobilecompany`.`tariff`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mobilecompany`.`tariff`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`tariff` (
  `tariff_id`     BIGINT(20)  NOT NULL AUTO_INCREMENT,
  `tariff_price`  INT(11)     NOT NULL,
  `tariff_tittle` VARCHAR(30) NOT NULL,
  `tariff_deleted` TINYINT(1) NOT NULL DEFAULT true,
  PRIMARY KEY (`tariff_id`),
  UNIQUE INDEX `UK_gwt7suosmuqtpvo8ufnk9eff7` (`tariff_tittle` ASC)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `mobilecompany`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mobilecompany`.`user`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`user` (
  `user_id`        BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `user_address`   VARCHAR(50)  NOT NULL,
  `user_birthdate` DATE  NOT NULL,
  `user_email`     VARCHAR(30)  NOT NULL,
  `user_name`      VARCHAR(20)  NOT NULL,
  `user_passport`  VARCHAR(11)  NOT NULL,
  `user_password`  VARCHAR(255) NOT NULL,
  `user_surname`   VARCHAR(20)  NOT NULL,
  `user_deleted` TINYINT(1) NOT NULL DEFAULT TRUE ,
  `user_role`      ENUM ('ADMINISTRATOR', 'USER'),
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `UK_j09k2v8lxofv2vecxu2hde9so` (`user_email` ASC),
  UNIQUE INDEX `UK_9f2fxduc9xv1crn4h67s55exu` (`user_passport` ASC)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `mobilecompany`.`options`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mobilecompany`.`options`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`options` (
  `option_id`         BIGINT(20)  NOT NULL AUTO_INCREMENT,
  `option_cost`       INT(11)     NOT NULL,
  `option_price`      INT(11)     NOT NULL,
  `option_tittle`     VARCHAR(30) NOT NULL,
  `tariff_id`         BIGINT(20)  NOT NULL,
  `option_deleted` TINYINT(1) NOT NULL DEFAULT TRUE ,

  PRIMARY KEY (`option_id`),
  UNIQUE INDEX `UK_595yj17xsv0ts0wmpptvpe64t` (`option_tittle` ASC),
  INDEX `to_tariff_idx` (`tariff_id` ASC),
  CONSTRAINT `totariff`
  FOREIGN KEY (`tariff_id`)
  REFERENCES `mobilecompany`.`tariff` (`tariff_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- --------------------------------------------------
-- Table `mobilecompany`.`number`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `mobilecompany`.`number`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`number` (
  `number_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `number_deleted` TINYINT(1) NOT NULL DEFAULT TRUE ,
  PRIMARY KEY (`number_id`)
#   CONSTRAINT `to_contract`
#   FOREIGN KEY (`contract_id`)
#   REFERENCES `mobilecompany`.`contract` (`contract_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `mobilecompany`.`contract`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mobilecompany`.`contract`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`contract` (
  `contract_id`    BIGINT(20) NOT NULL AUTO_INCREMENT,
  `number_id`      BIGINT(10) NOT NULL,
  `tariff_id`      BIGINT(20) NOT NULL,
  `user_id`        BIGINT(20) NOT NULL,
  `contract_block` ENUM ('ADMIN_BLOCKING', 'USER_BLOCKING', 'UNBLOCKING'),
  PRIMARY KEY (`contract_id`),
  INDEX `to_user` (`user_id` ASC),
  INDEX `to_tariff` (`tariff_id` ASC),
  CONSTRAINT `to_tariff`
  FOREIGN KEY (`tariff_id`)
  REFERENCES `mobilecompany`.`tariff` (`tariff_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `to_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `mobilecompany`.`user` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `to_number`
  FOREIGN KEY (`number_id`)
  REFERENCES `mobilecompany`.`number` (`number_id`)
)
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mobilecompany`.`contract_options`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mobilecompany`.`contract_options`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`contract_options` (
  `contract_id`         BIGINT(20)  NOT NULL,
  `option_id`           BIGINT(20)  NOT NULL,
  PRIMARY KEY (`contract_id`,`option_id`),
  CONSTRAINT `fk_contract` FOREIGN KEY (`contract_id`) REFERENCES `mobilecompany`.`contract` (`contract_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_option` FOREIGN KEY (`option_id`) REFERENCES `mobilecompany`.`options` (`option_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `mobilecompany`.`disabled_options`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mobilecompany`.`disabled_options`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`disabled_options` (
  `option_id`  BIGINT(20) NOT NULL,
  `option_id1` BIGINT(20) NOT NULL,
  PRIMARY KEY (`option_id`, `option_id1`),
  INDEX `fk_disabled_options_options2_idx` (`option_id1` ASC),
  INDEX `fk_disabled_options_options1_idx` (`option_id` ASC),
  CONSTRAINT `disabled_options`
  FOREIGN KEY (`option_id`)
  REFERENCES `mobilecompany`.`options` (`option_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

  CONSTRAINT `fk_disabled_options_options2`
  FOREIGN KEY (`option_id1`)
  REFERENCES `mobilecompany`.`options` (`option_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- --------------------------------------------------
-- Table `mobilecompany`.`required_options`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `mobilecompany`.`required_options`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`required_options` (
  `option_id`  BIGINT(20) NOT NULL,
  `option_id1` BIGINT(20) NOT NULL,
  PRIMARY KEY (`option_id`, `option_id1`),
  INDEX `fk_required_options_idx` (`option_id1` ASC),
  INDEX `fk_required_options1_idx` (`option_id` ASC),
  CONSTRAINT `required_options`
  FOREIGN KEY (`option_id`)
  REFERENCES `mobilecompany`.`options` (`option_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_required_options_options2`
  FOREIGN KEY (`option_id1`)
  REFERENCES `mobilecompany`.`options` (`option_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- --------------------------------------------------
-- Table `mobilecompany`.`message`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `mobilecompany`.`message`;

CREATE TABLE IF NOT EXISTS `mobilecompany`.`message` (
  `message_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `user_email`     VARCHAR(30)  NOT NULL,
  `tariff_tittle`  VARCHAR(20)  NOT NULL,
  `number`  BIGINT(10)  NOT NULL,
  `isDeleted` BOOLEAN DEFAULT true,
  `date` TIMESTAMP ,
  PRIMARY KEY (`message_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;