package com.tsystems.mymobile.service.api;

import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import com.tsystems.mymobile.service.dto.tariff.TariffAdminDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.service.dto.tariff.TariffSimpleDto;

import java.util.List;

/**
 * The interface Tariff service.
 */
public interface TariffService {

    /**
     * Gets active tariffs.
     *
     * @return the active tariffs
     */
    List<TariffDto> getActiveTariffs();

    /**
     * Gets tariff archive.
     *
     * @return the tariff archive
     */
    List<TariffDto> getTariffArchive();

    /**
     * Gets simple tariffs.
     *
     * @return the simple tariffs
     */
    List<TariffSimpleDto> getSimpleTariffs();

    /**
     * Gets tariff by id.
     *
     * @param id the id
     * @return the tariff by id
     */
    TariffDto getTariffById(Long id) ;

    /**
     * Gets tariff tittle by id.
     *
     * @param id the id
     * @return the tariff tittle by id
     */
    TariffAdminDto getTariffAdminById(Long id);

    /**
     * Gets enable tariffs.
     *
     * @param tariffDtoTittle the tariff dto tittle
     * @return the enable tariffs
     */
    List<TariffDto> getEnableTariffs(String tariffDtoTittle);

    /**
     * Create tariff long.
     *
     * @param tariffDto       the tariff dto
     * @param optionSimpleDto the option simple dto
     * @return the long
     */
    Long createTariff(TariffDto tariffDto, OptionSimpleDto optionSimpleDto);

    /**
     * Remove tariff by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeTariffById(Long id);

    /**
     * Update tariff tariff dto.
     *
     * @param tariffDto the tariff dto
     * @return the tariff dto
     */
    TariffDto updateTariff(TariffDto tariffDto);

    /**
     * Is exist boolean.
     *
     * @param tittle the tittle
     * @return the boolean
     */
    Boolean isExist(String tittle);
}
