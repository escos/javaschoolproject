package com.tsystems.mymobile.service.api;


import com.tsystems.mymobile.service.dto.option.OptionAdminDto;
import com.tsystems.mymobile.service.dto.option.OptionDto;
import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import com.tsystems.mymobile.service.dto.option.OptionTittleDto;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;

import java.util.List;

/**
 * The interface Option service.
 */
public interface OptionService {

    /**
     * Create option for tariff option simple dto.
     *
     * @param tariffId  the tariff id
     * @param optionSimpleDto the option dto
     * @return the option simple dto
     * @throws Exception the exception
     */
    OptionSimpleDto createOptionForTariff(Long tariffId, OptionSimpleDto optionSimpleDto) ;

    /**
     * Update option option simple dto.
     *
     * @param optionSimpleDto the option simple dto
     * @return the option simple dto
     * @throws Exception the exception
     */
    OptionSimpleDto updateOption(OptionSimpleDto optionSimpleDto) throws DataBaseException;

    /**
     * Remove req option by id boolean.
     *
     * @param reqOptionTittle the req option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean removeReqOptionById(String reqOptionTittle, Long optionId);

    /**
     * Remove dis option by id boolean.
     *
     * @param disOptionTittle the dis option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean removeDisOptionById(String disOptionTittle, Long optionId);

    /**
     * Add req option by id boolean.
     *
     * @param reqOptionTittle the req option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean addReqOptionById(String reqOptionTittle, Long optionId);

    /**
     * Add dis option by id boolean.
     *
     * @param disOptionTittle the dis option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean addDisOptionById(String disOptionTittle, Long optionId);

    /**
     * Remove option by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeOptionById(Long id);

    /**
     * Gets option by id.
     *
     * @param id the id
     * @return the option by id
     * @throws Exception the exception
     */
    OptionDto getOptionById(Long id);

    /**
     * Gets admin option by id.
     *
     * @param id the id
     * @return the admin option by id
     * @throws Exception the exception
     */
    OptionAdminDto getAdminOptionById(Long id);

    /**
     * Gets option by tittle.
     *
     * @param tittle the tittle
     * @return the option by tittle
     * @throws Exception the exception
     */
    OptionTittleDto getOptionByTittle(String tittle);

    /**
     * Connect options by ids boolean.
     *
     * @param contractId the contract id
     * @param optionIds  the option ids
     * @return the boolean
     */
    Boolean connectOptionsByIds(Long contractId, List<Long> optionIds);

    /**
     * Disconnect option by id boolean.
     *
     * @param contractId the contract id
     * @param id         the id
     * @return the boolean
     */
    Boolean disconnectOptionById(Long contractId, Long id);

    /**
     * Is exist boolean.
     *
     * @param tittle the tittle
     * @return the boolean
     * @throws Exception the exception
     */
    Boolean isExist(String tittle);

}
