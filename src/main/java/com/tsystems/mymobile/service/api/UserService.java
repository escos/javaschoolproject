package com.tsystems.mymobile.service.api;

import com.tsystems.mymobile.service.dto.user.UserDto;

import java.util.List;

/**
 * The interface User service.
 */
public interface UserService {

    /**
     * Authorise user dto.
     *
     * @param userDto the user dto
     * @return the user dto
     */
    UserDto authorise(UserDto userDto);

    /**
     * Gets user by id.
     *
     * @param id the id
     * @return the user by id
     */
    UserDto getUserById(Long id);

    /**
     * Gets user by id.
     *
     * @param id the id
     * @return the user by id
     */
    UserDto getUserByEmail(String email);

    /**
     * Gets all users.
     *
     * @return the all users
     */
    List<UserDto> getAllUsers();

    /**
     * Create user long.
     *
     * @param userDto the user dto
     * @return the long
     */
    Long createUser(UserDto userDto) ;

    /**
     * Remove user by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeUserById(Long id);

    /**
     * Is exist boolean.
     *
     * @param email    the email
     * @param passport the passport
     * @return the boolean
     */
    Boolean isExist(String email, String passport);

}
