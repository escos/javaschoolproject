package com.tsystems.mymobile.service.api;

import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;

import java.util.List;

/**
 * The interface Phone number service.
 */
public interface PhoneNumberService {

    /**
     * Gets free numbers.
     *
     * @return the free numbers
     */
    List<PhoneNumberDto> getFreeNumbers();

    /**
     * Gets used numbers.
     *
     * @return the used numbers
     */
    List<PhoneNumberDto> getUsedNumbers();

    /**
     * Gets number by id.
     *
     * @param id the id
     * @return the number by id
     */
    PhoneNumberDto getNumberById(Long id);

    /**
     * Gets free number.
     *
     * @return the free number
     */
    PhoneNumberDto getFreeNumber();
}
