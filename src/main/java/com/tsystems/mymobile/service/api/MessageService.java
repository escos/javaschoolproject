package com.tsystems.mymobile.service.api;

import com.tsystems.mymobile.service.dto.message.MessageDto;

import java.util.List;

/**
 * The interface Message service.
 */
public interface MessageService {

    /**
     * Save message boolean.
     *
     * @param messageDto the message dto
     * @return the boolean
     */
    Boolean saveMessage(MessageDto messageDto);

    /**
     * Gets messages.
     *
     * @return the messages
     */
    List<MessageDto> getMessages();

    /**
     * Remove message boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeMessage(Long id);

    /**
     * Get message by id.
     *
     * @param id the id
     * @return the messageDto
     */
    MessageDto getMessage(Long id);
}
