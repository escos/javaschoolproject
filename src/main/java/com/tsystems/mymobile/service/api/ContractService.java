package com.tsystems.mymobile.service.api;

import com.tsystems.mymobile.service.dto.contract.ContractDto;
import com.tsystems.mymobile.service.dto.contract.ContractSimpleDto;

import java.util.List;

/**
 * The interface Contract service.
 */
public interface ContractService {

    /**
     * Create contract long.
     *
     * @param contractDto the contract dto
     * @return the long
     */
    Long createContract(ContractDto contractDto);

    /**
     * Gets all contracts.
     *
     * @return the all contracts
     */
    List<ContractSimpleDto> getAllContracts();

    /**
     * Gets contract by user id.
     *
     * @param userDtoId the user dto id
     * @return the contract by user id
     */
    List<ContractSimpleDto> getContractByUserId(Long userDtoId);

    /**
     * Gets contract by id.
     *
     * @param id the id
     * @return the contract by id
     */
    ContractDto getContractById(Long id);

    /**
     * Contract block by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean contractBlockById(Long id);

    /**
     * Contract admin block by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean contractAdminBlockById(Long id);

    /**
     * Remove contract by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeContractById(Long id);

    /**
     * Gets contract by number.
     *
     * @param numberId the number id
     * @return the contract by number
     */
    ContractSimpleDto getContractByNumber(Long numberId);

    /**
     * Gets contracts by user email.
     *
     * @param email the email
     * @return the contracts by user email
     */
    List<ContractSimpleDto> getContractsByUserEmail(String email);

    /**
     * Update contract by tariff boolean.
     *
     * @param contractId the contract id
     * @param tariffId   the tariff id
     * @return the boolean
     */
    Boolean updateContractByTariff(Long contractId, Long tariffId);

}
