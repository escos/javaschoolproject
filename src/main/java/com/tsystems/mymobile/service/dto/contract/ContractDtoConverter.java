package com.tsystems.mymobile.service.dto.contract;

import com.tsystems.mymobile.model.*;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;
import com.tsystems.mymobile.service.dto.option.OptionDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.service.dto.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ContractDtoConverter extends BaseEntityDtoConverter<Contract,ContractDto>{

    @Autowired
    private BaseEntityDtoConverter<Option,OptionDto> converter;

    @Autowired
    private BaseEntityDtoConverter<Tariff,TariffDto> tariffConverter;

    @Autowired
    private BaseEntityDtoConverter<User,UserDto> userConverter;

    @Autowired
    private BaseEntityDtoConverter<PhoneNumber,PhoneNumberDto> phoneConverter;

    @Override
    public ContractDto convertEntityToDto(Contract contract) {
        ContractDto contractDto = new ContractDto();
        contractDto.setId(contract.getId());
        contractDto.setPhoneNumberDto(phoneConverter.convertEntityToDto(contract.getPhoneNumber()));
        contractDto.setTariffDto(tariffConverter.convertEntityToDto(contract.getTariff()));
        contractDto.setUserDto(userConverter.convertEntityToDto(contract.getUser()));
        contractDto.setBlockStatus(contract.getBlockStatus());
        contractDto.setConnectedOptions(converter
                .convertSetEntitiesToSetDtos(contract.getConnectedOptions()));
        return contractDto;
    }

    @Override
    public Contract convertDtoToEntity(ContractDto contractDto) {
        Contract contract = new Contract();
        contract.setId(contractDto.getId());
        contract.setPhoneNumber(phoneConverter.convertDtoToEntity(contractDto.getPhoneNumberDto()));
        contract.setTariff(tariffConverter.convertDtoToEntity(contractDto.getTariffDto()));
        contract.setUser(userConverter.convertDtoToEntity(contractDto.getUserDto()));
        contract.setBlockStatus(contractDto.getBlockStatus());
        contract.setConnectedOptions(converter
                .convertSetDtosToSetEntities(contractDto.getConnectedOptions()));
        return contract;
    }

    public ContractSimpleDto convertContractToSimpleDto(Contract contract){
        ContractSimpleDto contractSimpleDto = new ContractSimpleDto();
        contractSimpleDto.setId(contract.getId());
        contractSimpleDto.setPhoneNumber(contract.getPhoneNumber().getId());
        contractSimpleDto.setTariff(contract.getTariff().getTittle());
        contractSimpleDto.setUserEmail(contract.getUser().getEmail());
        contractSimpleDto.setBlockStatus(contract.getBlockStatus().toString());
        contractSimpleDto.setConnectedOptions(converter
                .convertSetEntitiesToSetDtos(contract.getConnectedOptions()));
        return contractSimpleDto;
    }

    public List<ContractSimpleDto> convertListContractsToListSimpleDtos(List<Contract> contracts) {
        return contracts.stream()
                .map(this::convertContractToSimpleDto)
                .collect(Collectors.toList());
    }

}
