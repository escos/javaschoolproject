package com.tsystems.mymobile.service.dto.contract;

import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;
import com.tsystems.mymobile.service.dto.option.OptionDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.service.dto.user.UserDto;
import com.tsystems.mymobile.utils.Enums.BlockStatus;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

public class ContractDto implements Serializable {

    @Setter @Getter private Long id;
    @Setter @Getter private PhoneNumberDto phoneNumberDto;
    @Setter @Getter private TariffDto tariffDto;
    @Setter @Getter private UserDto userDto;
    @Setter @Getter private BlockStatus blockStatus;
    @Setter @Getter private Set<OptionDto> connectedOptions;
}
