package com.tsystems.mymobile.service.dto;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Base entity dto converter.
 *
 * @param <E> the type parameter
 * @param <T> the type parameter
 */
public abstract class BaseEntityDtoConverter <E,T>{

    /**
     * Convert entity to dto t.
     *
     * @param e the e
     * @return the t
     */
    public abstract T convertEntityToDto(E e);

    /**
     * Convert dto to entity e.
     *
     * @param t the t
     * @return the e
     */
    public abstract E convertDtoToEntity(T t);

    /**
     * Convert list entities to list dtos list.
     *
     * @param eList the e list
     * @return the list
     */
    public List<T> convertListEntitiesToListDtos(List<E> eList){
        return eList.stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    /**
     * Convert list dtos to list entities list.
     *
     * @param eList the e list
     * @return the list
     */
    public List<E> convertListDtosToListEntities(List<T> eList){
        return eList.stream()
                .map(this::convertDtoToEntity)
                .collect(Collectors.toList());
    }

    /**
     * Convert set entities to set dtos set.
     *
     * @param eSet the e set
     * @return the set
     */
    public Set<T> convertSetEntitiesToSetDtos(Set<E> eSet){
        return eSet.stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toSet());
    }

    /**
     * Convert set dtos to set entities set.
     *
     * @param eSet the e set
     * @return the set
     */
    public Set<E> convertSetDtosToSetEntities(Set<T> eSet){
        return eSet.stream()
                .map(this::convertDtoToEntity)
                .collect(Collectors.toSet());
    }

}
