package com.tsystems.mymobile.service.dto.option;

import com.tsystems.mymobile.model.Contract;
import com.tsystems.mymobile.model.Option;
import com.tsystems.mymobile.service.api.OptionService;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The type Option dto converter.
 */
@Component
public class OptionDtoConverter extends BaseEntityDtoConverter<Option, OptionDto> {

    @Autowired
    private OptionService service;

    @Override
    public Option convertDtoToEntity(OptionDto optionDto) {
        Option option = new Option();
        option.setId(optionDto.getId());
        option.setTittle(optionDto.getTittle());
        option.setCost(optionDto.getCost());
        option.setPrice(optionDto.getPrice());
        option.setTariff(optionDto.getTariff());
        option.setIsDeleted(optionDto.getIsDeleted());
        return option;
    }

    @Override
    public OptionDto convertEntityToDto(Option option) {
        OptionDto optionDto = new OptionDto();
        optionDto.setId(option.getId());
        optionDto.setTittle(option.getTittle());
        optionDto.setCost(option.getCost());
        optionDto.setPrice(option.getPrice());
        optionDto.setTariff(option.getTariff());
        optionDto.setIsDeleted(option.getIsDeleted());
        optionDto.setContractIds(convertSetContractsToSetIds(option.getContracts()));
        optionDto.setDisOptions(convertSetOptionsToSetIds(option.getDisOptions()));
        optionDto.setDisOptions1(convertSetOptionsToSetIds(option.getDisOptions1()));
        optionDto.setReqOptions(convertSetOptionsToSetIds(option.getReqOptions()));
        optionDto.setReqOptions1(convertSetOptionsToSetIds(option.getReqOptions1()));
        return optionDto;
    }

    /**
     * Convert option to simple dto option simple dto.
     *
     * @param option the option
     * @return the option simple dto
     */
    public OptionSimpleDto convertOptionToSimpleDto(Option option) {
        OptionSimpleDto optionSimpleDto = new OptionSimpleDto();
        optionSimpleDto.setId(option.getId());
        optionSimpleDto.setTittle(option.getTittle());
        optionSimpleDto.setCost(option.getCost());
        optionSimpleDto.setPrice(option.getPrice());
        return optionSimpleDto;
    }

    /**
     * Convert option to tittle dto option tittle dto.
     *
     * @param option the option
     * @return the option tittle dto
     */
    public OptionTittleDto convertOptionToTittleDto(Option option) {
        OptionTittleDto optionTittleDto = new OptionTittleDto();
        optionTittleDto.setId(option.getId());
        optionTittleDto.setTittle(option.getTittle());
        optionTittleDto.setCost(option.getCost());
        optionTittleDto.setPrice(option.getPrice());
        optionTittleDto.setTariff(option.getTariff());
        optionTittleDto.setIsDeleted(option.getIsDeleted());
        optionTittleDto.setContractIds(convertSetContractsToSetIds(option.getContracts()));
        optionTittleDto.setDisOptions(convertSetOptionsToSetTittles(option.getDisOptions()));
        optionTittleDto.setDisOptions1(convertSetOptionsToSetTittles(option.getDisOptions1()));
        optionTittleDto.setReqOptions(convertSetOptionsToSetTittles(option.getReqOptions()));
        optionTittleDto.setReqOptions1(convertSetOptionsToSetTittles(option.getReqOptions1()));
        return optionTittleDto;
    }

    /**
     * Convert option simple dto to entity option.
     *
     * @param optionSimpleDto the option simple dto
     * @return the option
     */
    public Option convertOptionSimpleDtoToEntity(OptionSimpleDto optionSimpleDto) {
        Option option = new Option();
        option.setId(optionSimpleDto.getId());
        option.setTittle(optionSimpleDto.getTittle());
        option.setCost(optionSimpleDto.getCost());
        option.setPrice(optionSimpleDto.getPrice());
        return option;
    }

    /**
     * Convert set options to set ids set.
     *
     * @param options the options
     * @return the set
     */
    public Set<Long> convertSetOptionsToSetIds(Set<Option> options) {
        return options.stream()
                .map(option -> option.getId())
                .collect(Collectors.toSet());
    }

    private Set<Long> convertSetContractsToSetIds(Set<Contract> contracts) {
        return contracts.stream()
                .map(contract -> contract.getId())
                .collect(Collectors.toSet());
    }

    /**
     * Convert set options to set tittles set.
     *
     * @param options the options
     * @return the set
     */
    public Set<String> convertSetOptionsToSetTittles(Set<Option> options) {
        return options.stream()
                .map(option -> option.getTittle())
                .collect(Collectors.toSet());
    }

    /**
     * Convert to admin dto option admin dto.
     *
     * @param optionTittleDto the option tittle dto
     * @return the option admin dto
     */
    public OptionAdminDto convertToAdminDto(OptionTittleDto optionTittleDto) {
        OptionAdminDto optionAdminDto = new OptionAdminDto();
        optionAdminDto.setId(optionTittleDto.getId());
        optionAdminDto.setTittle(optionTittleDto.getTittle());
        optionAdminDto.setCost(optionTittleDto.getCost());
        optionAdminDto.setPrice(optionTittleDto.getPrice());
        optionAdminDto.setTariff(optionTittleDto.getTariff());
        optionAdminDto.setIsDeleted(optionTittleDto.getIsDeleted());
        optionAdminDto.setContractIds(optionTittleDto.getContractIds());
        optionAdminDto.setDisOptions(optionTittleDto.getDisOptions());
        optionAdminDto.setDisOptions1(optionTittleDto.getDisOptions1());
        optionAdminDto.setReqOptions(optionTittleDto.getReqOptions());
        optionAdminDto.setReqOptions1(optionTittleDto.getReqOptions1());
        optionAdminDto.setEnableReqOptions(getEnableReqOptions(optionTittleDto));
        optionAdminDto.setEnableDisOptions(getEnableDisOptions(optionTittleDto));
        return optionAdminDto;
    }

    /**
     * Find all enabled options for set as required options.
     *
     * @param optionTittleDto the option tittle dto
     * @return Set<String> all option tittles which can be set as required options
     */
    private Set<String> getEnableReqOptions(OptionTittleDto optionTittleDto) {

        //find all options for tariff
        Set<String> allOptions = convertSetOptionsToSetTittles(optionTittleDto.getTariff().getOptions());
        //find required options for optionTittleDto
        Set<String> reqOptions = optionTittleDto.getReqOptions();
        //find options which consists optionTittleDto as required option
        Set<String> reqOptions1 = optionTittleDto.getReqOptions1();
        //find disabled options for optionTittleDto
        Set<String> disOptions = optionTittleDto.getDisOptions();
        //find options which consists optionTittleDto as disabled option
        Set<String> disOptions1 = optionTittleDto.getDisOptions1();
        //option chaining 3 or less
        if (!reqOptions1.isEmpty()) {
            for (String req : reqOptions1) {
                if (!service.getOptionByTittle(req).getReqOptions1().isEmpty()) {
                    return new HashSet<>();
                }
            }
        }

        Set<String> temp1 = allOptions.stream()
                //exclude optionTittleDto
                .filter(option1 -> !option1.equals(optionTittleDto.getTittle()))
                //exclude disabled, required options from all options for tariff
                .filter(option1 -> !disOptions.contains(option1))
                .filter(option1 -> !disOptions1.contains(option1))
                .filter(option1 -> !reqOptions1.contains(option1))
                .filter(option1 -> !reqOptions.contains(option1))
                .collect(Collectors.toSet());
        //filter all removed options
        Set<OptionTittleDto> temp2 = temp1.stream()
                .map(option -> service.getOptionByTittle(option))
                .collect(Collectors.toSet())
                .stream()
                .filter(OptionTittleDto::getIsDeleted).collect(Collectors.toSet());
        // converting required Set options tittles to Set OptionTittleDto objects
        Set<OptionTittleDto> reqOptionsTittle1 = reqOptions1.stream()
                .map(option -> service.getOptionByTittle(option)).collect(Collectors.toSet());
        //filter option if it has required option, which consists optionTittleDto.
        for (OptionTittleDto opt : reqOptionsTittle1) {
            temp2 = temp2.stream()
                    .filter(option -> !opt.getReqOptions1()
                            .contains(option.getTittle())).collect(Collectors.toSet());
        }

        return temp2.stream().map(OptionTittleDto::getTittle).collect(Collectors.toSet());

    }

    /**
     * Find all enabled options for set as disabled options.
     *
     * @param optionTittleDto is option for researching
     * @return Set<String> all option tittles which can be set as disabled options
     */
    private Set<String> getEnableDisOptions(OptionTittleDto optionTittleDto) {
        Set<String> allOptions = convertSetOptionsToSetTittles(optionTittleDto.getTariff().getOptions());
        Set<String> reqOptions = optionTittleDto.getReqOptions();
        Set<String> reqOptions1 = optionTittleDto.getReqOptions1();
        Set<String> disOptions = optionTittleDto.getDisOptions();
        Set<String> disOptions1 = optionTittleDto.getDisOptions1();

        Set<String> temp1 = allOptions.stream()
                .filter(option1 -> !option1.equals(optionTittleDto.getTittle()))
                .filter(option1 -> !reqOptions.contains(option1))
                .filter(option1 -> !reqOptions1.contains(option1))
                .filter(option1 -> !disOptions.contains(option1))
                .filter(option1 -> !disOptions1.contains(option1))
                .collect(Collectors.toSet());

        //filter all removed options
        Set<OptionTittleDto> temp2 = temp1.stream()
                .map(option -> service.getOptionByTittle(option))
                .collect(Collectors.toSet())
                .stream()
                .filter(OptionTittleDto::getIsDeleted).collect(Collectors.toSet());
        //filter option if next conditions: optionTittleDto has requiredOptions1 and
        // any of them has option as required

        for (String req1 : reqOptions1) {
            temp2 = temp2.stream()
                    .filter(option -> !service.getOptionByTittle(req1).getReqOptions().contains(option.getTittle()))
                    .collect(Collectors.toSet());
        }
        // converting required Set options tittles to Set OptionTittleDto objects
        Set<OptionTittleDto> reqOptionsTittle1 = reqOptions1.stream()
                .map(option -> service.getOptionByTittle(option)).collect(Collectors.toSet());
        //filter option if it has required option, which consists optionTittleDto.
        for (OptionTittleDto opt : reqOptionsTittle1) {
            temp2 = temp2.stream()
                    .filter(option -> !opt.getReqOptions1()
                            .contains(option.getTittle())).collect(Collectors.toSet());
        }

        return temp2.stream().map(OptionTittleDto::getTittle).collect(Collectors.toSet());
    }

    /**
     * Convert set tittles to set admin dtos set.
     *
     * @param options the options
     * @return the set
     */
    public Set<OptionAdminDto> convertSetTittlesToSetAdminDtos(Set<OptionTittleDto> options) {
        return options.stream()
                .map(this::convertToAdminDto)
                .collect(Collectors.toSet());
    }
}
