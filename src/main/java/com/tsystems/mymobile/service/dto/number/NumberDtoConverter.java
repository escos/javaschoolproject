package com.tsystems.mymobile.service.dto.number;

import com.tsystems.mymobile.model.Contract;
import com.tsystems.mymobile.model.PhoneNumber;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import com.tsystems.mymobile.service.dto.contract.ContractDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NumberDtoConverter extends BaseEntityDtoConverter<PhoneNumber,PhoneNumberDto>{

    @Autowired
    private BaseEntityDtoConverter<Contract,ContractDto> converter;

    @Override
    public PhoneNumberDto convertEntityToDto(PhoneNumber phoneNumber) {
        PhoneNumberDto phoneNumberDto = new PhoneNumberDto();
        phoneNumberDto.setId(phoneNumber.getId());
        phoneNumberDto.setIsDeleted(phoneNumber.getIsDeleted());
        return phoneNumberDto;
    }

    @Override
    public PhoneNumber convertDtoToEntity(PhoneNumberDto phoneNumberDto) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setId(phoneNumberDto.getId());
        phoneNumber.setIsDeleted(phoneNumberDto.getIsDeleted());
        return phoneNumber;
    }
}
