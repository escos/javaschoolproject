package com.tsystems.mymobile.service.dto.tariff;

import com.tsystems.mymobile.model.Tariff;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import com.tsystems.mymobile.service.dto.option.OptionDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Tariff dto converter.
 */
@Component
public class TariffDtoConverter extends BaseEntityDtoConverter<Tariff, TariffDto> {

    @Autowired
    private OptionDtoConverter odc;

    @Override
    public Tariff convertDtoToEntity(TariffDto tariffDto) {
        Tariff tariff = new Tariff();
        tariff.setId(tariffDto.getId());
        tariff.setTittle(tariffDto.getTittle());
        tariff.setPrice(tariffDto.getPrice());
        tariff.setOptions(odc.convertSetDtosToSetEntities(tariffDto.getOptionDtos()));
        tariff.setIsDeleted(tariffDto.getIsDeleted());
        return tariff;
    }

    @Override
    public TariffDto convertEntityToDto(Tariff tariff) {
        TariffDto tariffDto = new TariffDto();
        tariffDto.setId(tariff.getId());
        tariffDto.setTittle(tariff.getTittle());
        tariffDto.setPrice(tariff.getPrice());
        tariffDto.setOptionDtos(odc.convertSetEntitiesToSetDtos(tariff.getOptions()));
        tariffDto.setIsDeleted(tariff.getIsDeleted());
        return tariffDto;
    }

    /**
     * Convert tariff to simple dto tariff simple dto.
     *
     * @param tariff the tariff
     * @return the tariff simple dto
     */
    public TariffSimpleDto convertTariffToSimpleDto(Tariff tariff) {
        TariffSimpleDto tariffSimpleDto = new TariffSimpleDto();
        tariffSimpleDto.setId(tariff.getId());
        tariffSimpleDto.setTittle(tariff.getTittle());
        tariffSimpleDto.setPrice(tariff.getPrice());
        tariffSimpleDto.setOptions(tariff.getOptions().stream()
                .map(option -> odc.convertOptionToSimpleDto(option))
                .collect(Collectors.toSet()));
        return tariffSimpleDto;
    }

    /**
     * Convert tariff to tittle dto tariff tittle dto.
     *
     * @param tariff the tariff
     * @return the tariff tittle dto
     */
    public TariffTittleDto convertTariffToTittleDto(Tariff tariff){
        TariffTittleDto tariffTittleDto = new TariffTittleDto();
        tariffTittleDto.setId(tariff.getId());
        tariffTittleDto.setTittle(tariff.getTittle());
        tariffTittleDto.setPrice(tariff.getPrice());
        tariffTittleDto.setOptionTittleDtos(tariff.getOptions()
                .stream().map(option -> odc.convertOptionToTittleDto(option))
                .collect(Collectors.toSet()));
        tariffTittleDto.setIsDeleted(tariff.getIsDeleted());
        return tariffTittleDto;
    }

    /**
     * Convert list tariffs to simple dtos list.
     *
     * @param tariffs the tariffs
     * @return the list
     */
    public List<TariffSimpleDto> convertListTariffsToSimpleDtos(List<Tariff> tariffs) {
        return tariffs.stream()
                .map(this::convertTariffToSimpleDto)
                .collect(Collectors.toList());
    }

    /**
     * Convert light dto to entity tariff.
     *
     * @param tariffDto the tariff dto
     * @return the tariff
     */
    public Tariff convertLightDtoToEntity(TariffDto tariffDto){
        Tariff tariff = new Tariff();
        tariff.setId(tariffDto.getId());
        tariff.setTittle(tariffDto.getTittle());
        tariff.setPrice(tariffDto.getPrice());
        return tariff;
    }

    /**
     * Convert to admin dto tariff admin dto.
     *
     * @param tariffTittleDto the tariff tittle dto
     * @return the tariff admin dto
     */
    public TariffAdminDto convertToAdminDto(TariffTittleDto tariffTittleDto){
        TariffAdminDto tariffAdminDto = new TariffAdminDto();
        tariffAdminDto.setId(tariffTittleDto.getId());
        tariffAdminDto.setTittle(tariffTittleDto.getTittle());
        tariffAdminDto.setPrice(tariffTittleDto.getPrice());
        tariffAdminDto.setOptionAdminDtos(odc.convertSetTittlesToSetAdminDtos(tariffTittleDto.getOptionTittleDtos()));
        tariffAdminDto.setIsDeleted(tariffTittleDto.getIsDeleted());
        return tariffAdminDto;
    }

    /**
     * Convert tariff to admin dto tariff admin dto.
     *
     * @param tariff the tariff
     * @return the tariff admin dto
     */
    public TariffAdminDto convertTariffToAdminDto(Tariff tariff){
       return convertToAdminDto(convertTariffToTittleDto(tariff));
    }

}


