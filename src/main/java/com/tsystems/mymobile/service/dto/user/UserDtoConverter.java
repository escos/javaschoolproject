package com.tsystems.mymobile.service.dto.user;

import com.tsystems.mymobile.model.User;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type User dto converter.
 */
@Component
public class UserDtoConverter extends BaseEntityDtoConverter<User, UserDto> {

    private static final Logger logger = Logger.getLogger(UserDtoConverter.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public UserDto convertEntityToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setSurname(user.getSurname());
        userDto.setBirthDate( getConvertedDate(user.getBirthDate()));
        userDto.setPassport(user.getPassport());
        userDto.setAddress(user.getAddress());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setRole(user.getRole());
        userDto.setIsDeleted(user.getIsDeleted());
        return userDto;
    }

    public User convertDtoToEntity(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setBirthDate(getDateFromString(userDto.getBirthDate()));
        user.setPassport(userDto.getPassport());
        user.setAddress(userDto.getAddress());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setIsDeleted(userDto.getIsDeleted());
        user.setRole(userDto.getRole());
        return user;
    }

    private static Date getDateFromString(String birthDate) {
        try {
            return dateFormat.parse(birthDate);
        } catch (ParseException e) {
            logger.error(e.getMessage());
            return new Date();
        }
    }

    private static String getConvertedDate(Date date) {
        return dateFormat.format(date);
    }
}

