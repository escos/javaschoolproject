package com.tsystems.mymobile.service.dto.tariff;

import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

public class TariffSimpleDto implements Serializable {

    @Setter @Getter private Long id;

    @Setter @Getter private String tittle;

    @Setter @Getter private Integer price;

    @Setter @Getter private Set<OptionSimpleDto> options;
}
