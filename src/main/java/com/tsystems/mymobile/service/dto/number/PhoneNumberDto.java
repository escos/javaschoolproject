package com.tsystems.mymobile.service.dto.number;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class PhoneNumberDto implements Serializable {

    @Setter @Getter private Long id;
    @Setter @Getter private Boolean isDeleted;

}
