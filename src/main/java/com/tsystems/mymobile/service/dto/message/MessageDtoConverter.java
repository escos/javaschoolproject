package com.tsystems.mymobile.service.dto.message;

import com.tsystems.mymobile.model.Message;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Message dto converter.
 */
@Component
public class MessageDtoConverter extends BaseEntityDtoConverter<Message, MessageDto> {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Override
    public MessageDto convertEntityToDto(Message message) {
        MessageDto messageDto = new MessageDto();
        messageDto.setId(message.getId());
        messageDto.setEmail(message.getEmail());
        messageDto.setTittle(message.getTittle());
        messageDto.setNumber(message.getNumber());
        messageDto.setDate(getConvertedDate(message.getDate()));
        messageDto.setIsDeleted(message.getIsDeleted());
        return messageDto;
    }

    @Override
    public Message convertDtoToEntity(MessageDto messageDto) {
        Message message = new Message();
        message.setId(messageDto.getId());
        message.setEmail(messageDto.getEmail());
        message.setTittle(messageDto.getTittle());
        message.setNumber(messageDto.getNumber());
        message.setDate(getCurrentDate());
        message.setIsDeleted(messageDto.getIsDeleted());
        return message;
    }

    private static Date getCurrentDate() {
        Long currentTime = System.currentTimeMillis();
        return new Date(currentTime);
    }

    private static String getConvertedDate(Date date) {
        return dateFormat.format(date);
    }
}
