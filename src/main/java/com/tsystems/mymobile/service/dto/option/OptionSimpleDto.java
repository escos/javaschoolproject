package com.tsystems.mymobile.service.dto.option;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class OptionSimpleDto implements Serializable {

    public OptionSimpleDto(Long id, String tittle, Integer cost, Integer price) {
        this.id = id;
        this.tittle = tittle;
        this.cost = cost;
        this.price = price;
    }

    public OptionSimpleDto() {
    }

    @Setter @Getter private Long id;
    @Setter @Getter private String tittle;
    @Setter @Getter private Integer cost;
    @Setter @Getter private Integer price;
}
