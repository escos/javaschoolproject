package com.tsystems.mymobile.service.dto.user;

import com.tsystems.mymobile.utils.Enums.Role;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class UserDto implements Serializable {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String surname;

    @Getter
    @Setter
    private String birthDate;

    @Getter
    @Setter
    private String passport;

    @Getter
    @Setter
    private String address;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private Boolean isDeleted;

    @Getter
    @Setter
    private Role role;

}
