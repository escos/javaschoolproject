package com.tsystems.mymobile.service.dto.contract;

import com.tsystems.mymobile.service.dto.option.OptionDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

public class ContractSimpleDto implements Serializable {

    @Getter @Setter private Long id;
    @Getter @Setter private Long phoneNumber;
    @Getter @Setter private String tariff;
    @Getter @Setter private String userEmail;
    @Getter @Setter private String blockStatus;
    @Getter @Setter private Set<OptionDto> connectedOptions;
}
