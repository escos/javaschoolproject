package com.tsystems.mymobile.service.dto.option;

import com.tsystems.mymobile.model.Tariff;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class OptionAdminDto implements Serializable {
    @Setter
    @Getter
    private Long id;
    @Setter @Getter private String tittle;
    @Setter @Getter private Integer cost;
    @Setter @Getter private Integer price;
    @Setter @Getter private Tariff tariff;
    @Setter @Getter private Boolean isDeleted;
    @Setter @Getter private Set<Long> contractIds = new HashSet<>();
    @Setter @Getter private Set<String> reqOptions = new HashSet<>();
    @Setter @Getter private Set<String> reqOptions1 = new HashSet<>();
    @Setter @Getter private Set<String> disOptions = new HashSet<>();
    @Setter @Getter private Set<String> disOptions1 = new HashSet<>();
    @Setter @Getter private Set<String> enableDisOptions = new HashSet<>();
    @Setter @Getter private Set<String> enableReqOptions = new HashSet<>();

}
