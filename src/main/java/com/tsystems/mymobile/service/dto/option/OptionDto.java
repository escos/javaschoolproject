package com.tsystems.mymobile.service.dto.option;

import com.tsystems.mymobile.model.Tariff;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class OptionDto implements Serializable {

    @Setter @Getter private Long id;
    @Setter @Getter private String tittle;
    @Setter @Getter private Integer cost;
    @Setter @Getter private Integer price;
    @Setter @Getter private Tariff tariff;
    @Setter @Getter private Boolean isDeleted;
    @Setter @Getter private Set<Long> contractIds = new HashSet<>();
    @Setter @Getter private Set<Long> reqOptions = new HashSet<>();
    @Setter @Getter private Set<Long> reqOptions1 = new HashSet<>();
    @Setter @Getter private Set<Long> disOptions = new HashSet<>();
    @Setter @Getter private Set<Long> disOptions1 = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OptionDto optionDto = (OptionDto) o;

        return id.equals(optionDto.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
