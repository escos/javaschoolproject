package com.tsystems.mymobile.service.dto.message;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class MessageDto implements Serializable {

    @Getter @Setter private Long id;

    @Getter @Setter private String email;

    @Getter @Setter private String tittle;

    @Getter @Setter private Long number;

    @Getter @Setter private String date;

    @Getter @Setter private Boolean isDeleted;
}
