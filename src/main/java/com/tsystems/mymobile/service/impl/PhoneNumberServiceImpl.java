package com.tsystems.mymobile.service.impl;

import com.tsystems.mymobile.dao.api.PhoneNumberDAO;
import com.tsystems.mymobile.model.PhoneNumber;
import com.tsystems.mymobile.service.api.PhoneNumberService;
import com.tsystems.mymobile.service.dto.BaseEntityDtoConverter;
import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class PhoneNumberServiceImpl implements PhoneNumberService {

    @Autowired
    private PhoneNumberDAO phoneNumberDAO;
    @Autowired
    private BaseEntityDtoConverter<PhoneNumber,PhoneNumberDto> converter;

    @Override
    public List<PhoneNumberDto> getFreeNumbers() {
        List<PhoneNumber> phoneNumbers = phoneNumberDAO.getFreeNumbers();
        if (phoneNumbers.size() == 0){
            return new ArrayList<>();
        }
        return converter.convertListEntitiesToListDtos(phoneNumbers);
    }

    @Override
    public List<PhoneNumberDto> getUsedNumbers() {
        List<PhoneNumber> phoneNumbers = phoneNumberDAO.getUsedNumbers();
        if (phoneNumbers.size() == 0){
            return new ArrayList<>();
        }
        return converter.convertListEntitiesToListDtos(phoneNumbers);
    }

    @Override
    public PhoneNumberDto getNumberById(Long id) {
        PhoneNumber phoneNumber = phoneNumberDAO.getNumberById(id);
        if (phoneNumber.getId() == null){
            return new PhoneNumberDto();
        }
        return converter.convertEntityToDto(phoneNumber);
    }

    @Override
    public PhoneNumberDto getFreeNumber(){
        List<PhoneNumber> phoneNumbers = phoneNumberDAO.getFreeNumbers();
        if (phoneNumbers.size() == 0){
            return new PhoneNumberDto();
        }
        PhoneNumber number = phoneNumbers
                .get(new Random().nextInt(phoneNumbers.size()));
        return converter.convertEntityToDto(number);
    }
}
