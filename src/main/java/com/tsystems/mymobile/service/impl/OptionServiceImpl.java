package com.tsystems.mymobile.service.impl;

import com.tsystems.mymobile.dao.api.OptionDAO;
import com.tsystems.mymobile.model.Option;
import com.tsystems.mymobile.service.api.OptionService;
import com.tsystems.mymobile.service.dto.option.*;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import com.tsystems.mymobile.utils.jms.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The type Option service.
 */
@Service
@Transactional
public class OptionServiceImpl implements OptionService {

    @Autowired
    private OptionDAO optionDAO;

    @Autowired
    private OptionDtoConverter converter;

    @Autowired
    private Sender sender;

    @Override
    public Boolean connectOptionsByIds(Long contractId, List<Long> ids) {
        return optionDAO.connectOptionsByIds(contractId, ids);
    }

    @Override
    public Boolean disconnectOptionById(Long contractId, Long id) {
        sender.send("update", "option");
        return optionDAO.disconnectOptionById(contractId, id);
    }

    @Override
    public OptionSimpleDto createOptionForTariff(Long tariffId, OptionSimpleDto optionSimpleDto) {
        Option option = converter.convertOptionSimpleDtoToEntity(optionSimpleDto);
        Option newOption = optionDAO.createOptionForTariff(tariffId, option);
        if (newOption.getId() == null) {
            return new OptionSimpleDto();
        }
        sender.send("update", "option");
        return converter.convertOptionToSimpleDto(newOption);
    }

    @Override
    public OptionSimpleDto updateOption(OptionSimpleDto optionSimpleDto) throws DataBaseException {
        Option option = converter.convertOptionSimpleDtoToEntity(optionSimpleDto);
        Option newOption = optionDAO.updateOption(option);
        if (newOption.getId() == null) {
            return new OptionSimpleDto();
        }
        sender.send("update", "option");
        return converter.convertOptionToSimpleDto(newOption);
    }

    @Override
    public Boolean removeReqOptionById(String reqOptionTittle, Long optionId) {
        Boolean status = optionDAO.removeReqOptionById(reqOptionTittle, optionId);
        if (status) {
            sender.send("update", "option");
        }
        return status;
    }

    @Override
    public Boolean removeDisOptionById(String disOptionTittle, Long optionId) {
        Boolean status = optionDAO.removeDisOptionById(disOptionTittle, optionId);
        if (status) {
            sender.send("update", "option");
        }
        return status;
    }

    @Override
    public Boolean addReqOptionById(String reqOptionTittle, Long optionId) {
        Boolean status = optionDAO.addReqOptionById(reqOptionTittle, optionId);
        if (status) {
            sender.send("update", "option");
        }
        return status;
    }

    @Override
    public Boolean addDisOptionById(String disOptionTittle, Long optionId) {
        Boolean status = optionDAO.addDisOptionById(disOptionTittle, optionId);
        if (status) {
            sender.send("update", "option");
        }
        return status;
    }

    @Override
    public Boolean removeOptionById(Long optionId) {
        Boolean status = optionDAO.removeOptionById(optionId);
        if (status) {
            sender.send("update", "option");
        }
        return status;
    }

    @Override
    public OptionDto getOptionById(Long id) {
        Option option = optionDAO.getOptionById(id);
        if (option.getId() == null) {
            return new OptionDto();
        }
        return converter
                .convertEntityToDto(option);
    }

    @Override
    public OptionAdminDto getAdminOptionById(Long id) {
        return converter.convertToAdminDto(
                converter.convertOptionToTittleDto(optionDAO.getOptionById(id)));
    }

    @Override
    public OptionTittleDto getOptionByTittle(String tittle) {
        if (optionDAO.getOptionByTittle(tittle).getId() == null) {
            return new OptionTittleDto();
        }
        return converter.convertOptionToTittleDto(optionDAO.getOptionByTittle(tittle));
    }

    @Override
    public Boolean isExist(String tittle) {
        return optionDAO.getOptionByTittle(tittle).getId() != null;
    }

}
