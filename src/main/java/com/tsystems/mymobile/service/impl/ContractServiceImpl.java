package com.tsystems.mymobile.service.impl;

import com.tsystems.mymobile.dao.api.ContractDAO;
import com.tsystems.mymobile.model.Contract;
import com.tsystems.mymobile.service.api.ContractService;
import com.tsystems.mymobile.service.dto.contract.ContractDto;
import com.tsystems.mymobile.service.dto.contract.ContractDtoConverter;
import com.tsystems.mymobile.service.dto.contract.ContractSimpleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractDAO contractDAO;

    @Autowired
    private ContractDtoConverter converter;

    @Override
    public Long createContract(ContractDto contractDto){
        Contract contract = converter.convertDtoToEntity(contractDto);
        return contractDAO.createContract(contract);
    }

    @Override
    public List<ContractSimpleDto> getAllContracts() {
        List<Contract> contracts = contractDAO.getAllContracts();
        if (contracts.size()==0){
            return new ArrayList<>();
        }
        return converter.
                convertListContractsToListSimpleDtos(contracts);
    }

    @Override
    public List<ContractSimpleDto> getContractByUserId(Long userDtoId) {
        List<Contract> contracts = contractDAO.getContractByUserId(userDtoId);
        if(contracts.size()==0){
            return new ArrayList<>();
        }

        return converter.
                convertListContractsToListSimpleDtos(contracts);
    }

    @Override
    public ContractDto getContractById(Long id) {
        Contract contract = contractDAO.getContractById(id);
        if (contract.getId() == null){
            return new ContractDto();
        }
        return converter.convertEntityToDto(contractDAO.getContractById(id));
    }

    @Override
    public Boolean contractBlockById(Long contractId) {
        return contractDAO.contractBlockById(contractId) == 1;
    }

    @Override
    public Boolean contractAdminBlockById(Long contractDtoId) {
        return contractDAO.contractAdminBlockById(contractDtoId) == 1;
    }

    @Override
    public Boolean removeContractById(Long id) {
        return contractDAO.removeContractById(id);
    }

    @Override
    public ContractSimpleDto getContractByNumber(Long numberId) {
        Contract contract = contractDAO.getContractByNumber(numberId);
        if (contract.getPhoneNumber() == null){
            return new ContractSimpleDto();
        }
        return converter
                .convertContractToSimpleDto(contractDAO.getContractByNumber(numberId));
    }

    @Override
    public List<ContractSimpleDto> getContractsByUserEmail(String userEmail) {
        List<Contract> contracts = contractDAO.getContractsByUserEmail(userEmail);
        if(contracts.size()==0){
            return new ArrayList<>();
        }
        return converter
                .convertListContractsToListSimpleDtos(contracts);
    }

    @Override
    public Boolean updateContractByTariff(Long contractId, Long tariffId) {
        return contractDAO.updateContractByTariff(contractId,tariffId);
    }

}
