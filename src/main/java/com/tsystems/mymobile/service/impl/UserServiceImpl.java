package com.tsystems.mymobile.service.impl;

import com.tsystems.mymobile.controller.RestfulController;
import com.tsystems.mymobile.dao.api.UserDAO;
import com.tsystems.mymobile.model.User;
import com.tsystems.mymobile.service.api.UserService;
import com.tsystems.mymobile.service.dto.user.UserDto;
import com.tsystems.mymobile.service.dto.user.UserDtoConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;



@Service
@Transactional(readOnly = true,propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(RestfulController.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserDtoConverter converter;

    @Override
    public UserDto authorise(UserDto userDto) {
        User user = userDAO.authorise(userDto.getEmail(), userDto.getPassword());
        if (user.getId() == null) {
            return new UserDto();
        }
        return converter.convertEntityToDto(user);
    }

    @Override
    public UserDto getUserById(Long id){
        User user = userDAO.getUserById(id);
        if (user.getId() == null){
            return new UserDto();
        }
        return converter
                .convertEntityToDto(user);
    }

    @Override
    public UserDto getUserByEmail(String email) {
        User user = userDAO.getUserByEmail(email);
        if (user.getId() == null){
            return null;
        }
        return converter.convertEntityToDto(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<User> users = userDAO.getAllUsers();
        if (users.size() == 0) {
            return new ArrayList<>();
        }
        return converter.convertListEntitiesToListDtos(users);
    }

    @Override
    public Long createUser(UserDto userDto) {
        User user = converter.convertDtoToEntity(userDto);
        return userDAO.createUser(user);
    }

    @Override
    public Boolean removeUserById(Long id) {
        return userDAO.removeUserById(id);
    }

    @Override
    public Boolean isExist(String email, String passport){
        return userDAO.isExistUser(email, passport);
    }

}
