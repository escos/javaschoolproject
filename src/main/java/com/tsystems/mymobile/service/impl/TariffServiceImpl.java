package com.tsystems.mymobile.service.impl;

import com.tsystems.mymobile.dao.api.TariffDAO;
import com.tsystems.mymobile.model.Tariff;
import com.tsystems.mymobile.service.api.TariffService;
import com.tsystems.mymobile.service.dto.option.OptionDtoConverter;
import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import com.tsystems.mymobile.service.dto.tariff.TariffAdminDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDtoConverter;
import com.tsystems.mymobile.service.dto.tariff.TariffSimpleDto;
import com.tsystems.mymobile.utils.jms.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TariffServiceImpl implements TariffService{

    @Autowired
    private TariffDAO tariffDAO;

    @Autowired
    private TariffDtoConverter converter;

    @Autowired
    private OptionDtoConverter odc;

    @Autowired
    private Sender sender;

    @Override
    public List<TariffDto> getActiveTariffs() {
        List<Tariff> tariffs = tariffDAO.getActiveTariffs();
        if (tariffs.size() == 0){
            return new ArrayList<>();
        }
        return converter.
                convertListEntitiesToListDtos(tariffs);
    }

    @Override
    public List<TariffDto> getTariffArchive() {
        List<Tariff> tariffs = tariffDAO.getTariffArchive();
        if (tariffs.size() == 0){
            return new ArrayList<>();
        }
        return converter.
                convertListEntitiesToListDtos(tariffs);
    }

    @Override
    public List<TariffSimpleDto> getSimpleTariffs() {
        List<Tariff> tariffs = tariffDAO.getActiveTariffs();
        if (tariffs.size() == 0){
            return new ArrayList<>();
        }
        return converter.convertListTariffsToSimpleDtos(tariffs);
    }

    @Override
    public TariffDto getTariffById(Long id){
       Tariff tariff = tariffDAO.getTariffById(id);
       if (tariff.getId() == null){
           return new TariffDto();
       }
       return converter.convertEntityToDto(tariff);
    }

    @Override
    public TariffAdminDto getTariffAdminById(Long id) {
        Tariff tariff = tariffDAO.getTariffById(id);
        if (tariff.getId() == null){
            return new TariffAdminDto();
        }
        return converter.convertTariffToAdminDto(tariff);
    }

    @Override
    public List<TariffDto> getEnableTariffs(String tariffTittle) {
        return getActiveTariffs().stream()
                .filter(t->!t.getTittle().equals(tariffTittle))
                 .collect(Collectors.toList());
    }

    @Override
    public Long createTariff(TariffDto tariffDto, OptionSimpleDto optionSimpleDto) {
        Long id = tariffDAO.createTariff( converter.convertLightDtoToEntity(tariffDto),
                odc.convertOptionSimpleDtoToEntity(optionSimpleDto) );
        if (!id.equals(-1L)){
            sender.send("create",tariffDto.getTittle());
        }
        return id;
    }

    @Override
    public Boolean removeTariffById(Long id) {
        if (tariffDAO.getTariffById(id)!=null && tariffDAO.getTariffById(id).getId() !=null){
            String tittle = tariffDAO.getTariffById(id).getTittle();
            if(tariffDAO.removeTariffById(id)){
                sender.send("delete",tittle);
            }
        }
        return tariffDAO.removeTariffById(id);
    }

    @Override
    public TariffDto updateTariff(TariffDto tariffDto){
        Tariff tariff = tariffDAO.updateTariff(converter.convertLightDtoToEntity(tariffDto));
        if (tariff.getId() == null){
            return new TariffDto();
        }

        sender.send("update",tariff.getTittle());
        return converter.convertEntityToDto(tariff);
    }

    @Override
    public Boolean isExist(String tittle) {
        return tariffDAO.getTariffByTittle(tittle).getId() != null;
    }
}
