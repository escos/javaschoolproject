package com.tsystems.mymobile.service.impl;

import com.tsystems.mymobile.dao.api.MessageDAO;
import com.tsystems.mymobile.service.api.MessageService;
import com.tsystems.mymobile.service.dto.message.MessageDto;
import com.tsystems.mymobile.service.dto.message.MessageDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDAO messageDAO;

    @Autowired
    private MessageDtoConverter converter;

    @Override
    public Boolean saveMessage(MessageDto messageDto){
        return messageDAO.saveMessage(converter.convertDtoToEntity(messageDto));
    }

    @Override
    public List<MessageDto> getMessages() {
        if (messageDAO.getMessages().isEmpty()){
            return new ArrayList<>();
        }
        return converter.convertListEntitiesToListDtos(messageDAO.getMessages());
    }

    @Override
    public Boolean removeMessage(Long id) {
        return messageDAO.removeMessage(id);
    }

    @Override
    public MessageDto getMessage(Long id) {
        if (messageDAO.getMessage(id).getId() == null){
            return new MessageDto();
        }
        return converter.convertEntityToDto(messageDAO.getMessage(id));
    }
}
