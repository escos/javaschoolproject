package com.tsystems.mymobile.utils.Validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberValidator {

    private static final String NUMBER_PATTERN =
            "\\d{10}";

    public static boolean validate(final String number) {
        Pattern pattern = Pattern.compile(NUMBER_PATTERN);
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }
}
