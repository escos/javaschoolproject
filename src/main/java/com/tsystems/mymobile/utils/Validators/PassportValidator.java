package com.tsystems.mymobile.utils.Validators;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PassportValidator {

    private static final String PASSPORT_PATTERN = "\\d{4}\\s\\d{6}";

    public boolean validate(final String passport) {
        if (passport.isEmpty()){
            return true;
        }
        Pattern pattern = Pattern.compile(PASSPORT_PATTERN);
        Matcher matcher = pattern.matcher(passport);
        return matcher.matches();
    }
}
