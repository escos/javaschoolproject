package com.tsystems.mymobile.utils.Validators;

import com.tsystems.mymobile.service.dto.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserFormValidator implements Validator {

    @Autowired
    private DateValidator dateValidator;

    @Autowired
    private PassportValidator passportValidator;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDto.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDto userDto = (UserDto) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotNull.userDto.name");
        String name = userDto.getName();
        if ((name.length() < 2) && (name.length() > 0) || (name.length() > 15)) {
            errors.rejectValue("name", "Size.userDto.name");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "NotNull.userDto.surname");
        String surname = userDto.getSurname();
        if ((surname.length() < 2) && (surname.length() > 0) || (surname.length() > 15)) {
            errors.rejectValue("surname", "Size.userDto.surname");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotNull.userDto.email");
        String email = userDto.getEmail();
        if (email.length() > 20) {
            errors.rejectValue("email", "Size.userDto.email");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "NotNull.userDto.birthDate");
        if (!dateValidator.validate(userDto.getBirthDate())) {
            errors.rejectValue("birthDate", "Pattern.userDto.birthDate");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotNull.userDto.address");
        if (userDto.getAddress().length() > 50) {
            errors.rejectValue("address", "Size.userDto.address");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passport", "NotNull.userDto.passport");
        if (!passportValidator.validate(userDto.getPassport())){
            errors.rejectValue("passport","Size.userDto.passport");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotNull.userDto.password");
        String password = userDto.getPassword();
        if ((password.length() < 3) && (password.length() > 0) || (password.length() > 20)) {
            errors.rejectValue("password", "Size.userDto.password");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role", "NotNull.userDto.role");
    }
}
