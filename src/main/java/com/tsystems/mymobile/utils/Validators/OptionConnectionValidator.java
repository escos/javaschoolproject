package com.tsystems.mymobile.utils.Validators;

import com.tsystems.mymobile.service.api.ContractService;
import com.tsystems.mymobile.service.api.OptionService;
import com.tsystems.mymobile.service.dto.contract.ContractDto;
import com.tsystems.mymobile.service.dto.option.OptionDto;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Option connection validator.
 */
@Component
public class OptionConnectionValidator {

    @Autowired
    private ContractService contractService;

    @Autowired
    private OptionService optionService;

    /**
     * Is validate integer.
     *
     * @param optionDto  the option dto
     * @param basketList the basket list
     * @param contractId the contract id
     * @return the integer
     */
    public Integer isValidate(OptionDto optionDto, List<OptionDto> basketList, Long contractId) {
        ContractDto contractDto = contractService.getContractById(contractId);
        Set<Long> connectedOptions = contractDto.getConnectedOptions()
                .stream()
                .map(option -> option.getId())
                .collect(Collectors.toSet());
        Set<Long> basketOptions = basketList
                .stream()
                .map(option -> option.getId())
                .collect(Collectors.toSet());
        Set<Long> disOptions = optionDto.getDisOptions();
        Set<Long> dis1Options = optionDto.getDisOptions1();
        Set<Long> reqOptions = optionDto.getReqOptions();
        if (!Collections.disjoint(dis1Options, connectedOptions) ||
                !Collections.disjoint(disOptions, connectedOptions) ||
                !Collections.disjoint(disOptions, basketOptions) ||
                !Collections.disjoint(dis1Options, basketOptions)) {
            return -1;
        }
        if (!reqOptions.isEmpty()) {
            if (!connectedOptions.containsAll(reqOptions) && !basketOptions.containsAll(reqOptions)) {
                return -2;
            }
        }
        return 0;
    }

    /**
     * Sort conn list list.
     *
     * @param basketIds  the basket ids
     * @param contractId the contract id
     * @return the list
     * @throws Exception the exception
     */
    public List<Long> sortConnList(List<Long> basketIds, Long contractId) throws DataBaseException {
        ContractDto contractDto = contractService.getContractById(contractId);
        Set<Long> connOptIds = contractDto.getConnectedOptions()
                .stream().map(option -> option.getId())
                .collect(Collectors.toSet());
        List<Long> resultList = new ArrayList<>();
        List<Long> sumList = Stream.concat(connOptIds.stream(), basketIds.stream())
                .collect(Collectors.toList());
        for (Long id: basketIds) {
            Set<Long> reqOptIds = optionService.getOptionById(id).getReqOptions();
            if (basketIds.containsAll(reqOptIds)||connOptIds.containsAll(reqOptIds)
                    || sumList.containsAll(reqOptIds)){
                resultList.add(id);
            }
        }
        return resultList;
    }

    /**
     * Reset conn options.
     *
     * @param contractId the contract id
     */
    public void resetConnOptions(Long contractId){
        Set<OptionDto> connOpts = contractService
                .getContractById(contractId)
                .getConnectedOptions();
        Set<Long> connIds = connOpts.stream()
                .map(option->option.getId())
                .collect(Collectors.toSet());
        for (OptionDto optionDto: connOpts) {
            if (!connIds.containsAll(optionDto.getReqOptions())){
                optionService.disconnectOptionById(contractId,optionDto.getId());
            }
        }
    }

}
