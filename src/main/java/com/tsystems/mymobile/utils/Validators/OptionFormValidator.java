package com.tsystems.mymobile.utils.Validators;

import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class OptionFormValidator {

    private static final String TITTLE_PATTERN = "[a-zA-Z]{3,25}";
    private static final String PRICE_COST_PATTERN = "[1-9]{1}[0-9]{1,2}";

    public Boolean validate(OptionSimpleDto optionSimpleDto)  {
        if (optionSimpleDto.getTittle() == null||
                optionSimpleDto.getCost()== null||
                optionSimpleDto.getPrice() == null){
            return false;
        }
        String tittle = optionSimpleDto.getTittle();
        String price = optionSimpleDto.getCost().toString();
        String cost = optionSimpleDto.getPrice().toString();
        Pattern pattern = Pattern.compile(TITTLE_PATTERN);
        Matcher matcher = pattern.matcher(tittle);
        if(!matcher.matches()){
            return false;
        }
        Pattern pattern1 = Pattern.compile(PRICE_COST_PATTERN);
        Matcher matcher1 = pattern1.matcher(cost);
        if (!matcher1.matches()){
            return false;
        }
        Matcher matcher2 = pattern1.matcher(price);
        return matcher2.matches();
    }
}
