package com.tsystems.mymobile.utils.Validators;

import com.tsystems.mymobile.service.dto.user.UserDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * The type Login form validator.
 */
@Component
public class LoginFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDto.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors)  {
        UserDto userLoginDto = (UserDto) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotNull.userDto.email");
        String email = userLoginDto.getEmail();
        if (email.length()>20){
            errors.rejectValue("email","Size.userDto.email");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotNull.userDto.password");
        String password = userLoginDto.getPassword();
        if ((password.length() < 3)&&(password.length()>0)||(password.length()>20)){
            errors.rejectValue("password","Size.userDto.password");
        }

    }
}
