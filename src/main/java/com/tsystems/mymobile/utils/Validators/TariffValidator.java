package com.tsystems.mymobile.utils.Validators;

import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Tariff validator.
 */
@Component
public class TariffValidator  {

    private static final String TARIFF_PATTERN = "[a-zA-Z]{3,25}";
    private static final String TARIFF_PATTERN_PRICE = "[0-9]{2,4}";

    /**
     * Validate boolean.
     *
     * @param tariffDto the tariff dto
     * @return the boolean
     */
    public Boolean validate(TariffDto tariffDto) {
        if(tariffDto.getPrice()==null || tariffDto.getTittle()==null){
            return false;
        }
        String tittle = tariffDto.getTittle();
        String price = tariffDto.getPrice().toString();
        Pattern pattern = Pattern.compile(TARIFF_PATTERN);
        Matcher matcher = pattern.matcher(tittle);
        if(!matcher.matches()){
           return false;
        }

        Pattern pattern1 = Pattern.compile(TARIFF_PATTERN_PRICE);
        Matcher matcher1 = pattern1.matcher(price);

        return matcher1.matches();
    }
}