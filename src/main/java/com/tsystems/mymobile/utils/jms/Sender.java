package com.tsystems.mymobile.utils.jms;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;


/**
 * The type Sender.
 */
@Component
public class Sender {

    private static final Logger logger = Logger.getLogger(Sender.class);

    /**
     * Send.
     *
     * @param op     the op
     * @param tittle the tittle
     */
    public void send(String op, String tittle){

        QueueSender  sender = null;
        QueueSession session = null;
        QueueConnection connection = null;
        try {
            Hashtable<String, String> props = new Hashtable<>();
            props.put("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
            props.put("java.naming.provider.url", "tcp://localhost:61616");
            props.put("queue.js-queue", "myQueue");
            props.put("connectionFactoryNames", "queueCF");
            Context context = new InitialContext(props);
            QueueConnectionFactory connectionFactory = (QueueConnectionFactory) context.lookup("queueCF");
            Queue queue = (Queue) context.lookup("js-queue");
            connection = connectionFactory.createQueueConnection();
            connection.start();
            session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
            sender = session.createSender(queue);
            TextMessage message = session.createTextMessage("Changes from MobileCompany!");
            message.setStringProperty("operation", op);
            message.setStringProperty("tittle",tittle);
            sender.send(message);
            logger.info("Message sent!");
        } catch (NamingException e){
            logger.error("Error naming exception", e);
        } catch (JMSException e){
            logger.error("JMS connection error",e);
        }
        finally {
            if (sender != null) {
                try {
                    sender.close();
                    session.close();
                    connection.close();
                } catch (JMSException e) {
                    logger.error("Resources didn't close!");
                }

            }
        }
    }
}