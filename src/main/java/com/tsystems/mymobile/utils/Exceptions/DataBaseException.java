package com.tsystems.mymobile.utils.Exceptions;

public class DataBaseException extends Exception {
    public DataBaseException(String s) {
        super(s);
    }
}