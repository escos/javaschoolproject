package com.tsystems.mymobile.utils.Enums;

/**
 * The enum Role.
 */
public enum Role {
    /**
     * User role.
     */
    USER,
    /**
     * Administrator role.
     */
    ADMINISTRATOR
}
