package com.tsystems.mymobile.utils.Enums;

/**
 * The enum Block status.
 */
public enum BlockStatus {

    /**
     * Admin blocking block status.
     */
    ADMIN_BLOCKING,
    /**
     * User blocking block status.
     */
    USER_BLOCKING,
    /**
     * Unblocking block status.
     */
    UNBLOCKING
}
