package com.tsystems.mymobile.controller;

import com.tsystems.mymobile.service.api.MessageService;
import com.tsystems.mymobile.service.dto.message.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
@RequestMapping("/main")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private HttpSession session;

    @RequestMapping(value = "/admin/message/save/{number}", method = RequestMethod.POST)
    public String saveMessage(@ModelAttribute("message") MessageDto messageDto, @PathVariable String number, ModelMap map) {
        if (!messageService.saveMessage(messageDto)){
            map.addAttribute("mesSendErr"
                    ,messageSource.getMessage("Message.Save.Error", null, null));
            return "new/main";
        }
        return "new/main";
    }

    @RequestMapping(value = "/admin/messages", method = RequestMethod.GET)
    public String getMessages(@ModelAttribute("message") MessageDto messageDto, ModelMap map) {
        if (messageService.getMessages().isEmpty()) {
            map.addAttribute("messages", new ArrayList<>());
            return "new/message";
        }
        map.addAttribute("messages", messageService.getMessages());
        return "new/message";
    }

    @RequestMapping(value = "/main_messages", method = RequestMethod.GET)
    public String getMainContent(ModelMap map) {
        if (messageService.getMessages().isEmpty()) {
            map.addAttribute("messages", new ArrayList<>());
            return "new/admin/message";
        }
        map.addAttribute("messages", messageService.getMessages());
        return "new/admin/main_content";
    }

    @RequestMapping(value = "/admin/message/remove/{id}", method = RequestMethod.POST)
    public String removeMessage(@PathVariable String id, ModelMap map) {
        if (!messageService.removeMessage(Long.valueOf(id))) {
            map.addAttribute("errRemove", messageSource
                    .getMessage("Message.Remove.Error", null, null));
            return "new/message";
        }
        map.addAttribute("messages", messageService.getMessages());
        return "new/message";
    }

    @RequestMapping(value = "/admin/message/{id}", method = RequestMethod.GET)
    public String getMessageId(@PathVariable String id, ModelMap map) {
        MessageDto messageDto = messageService.getMessage(Long.valueOf(id));
        if (messageDto.getId() == null) {
            return "new/error_system";
        }
        map.addAttribute("request",1);
        map.addAttribute("message",messageDto);
        return "new/admin/create";
    }

    /**
     * Db error string.
     *
     * @param e the e
     * @return the string
     */
    @ExceptionHandler(Exception.class)
    public String dbError(Exception e){
        return "new/error_system";
    }

}
