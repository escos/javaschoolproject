package com.tsystems.mymobile.controller;


import com.tsystems.mymobile.service.api.PhoneNumberService;
import com.tsystems.mymobile.service.api.TariffService;
import com.tsystems.mymobile.service.api.UserService;
import com.tsystems.mymobile.service.dto.user.UserDto;
import com.tsystems.mymobile.utils.Enums.Role;
import com.tsystems.mymobile.utils.Validators.UserFormValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * The type User controller.
 */
@Controller
@SessionAttributes(types = UserDto.class)
@RequestMapping("/main")
public class UserController {

    private static final Logger logger = Logger.getLogger(ContractController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private PhoneNumberService phoneNumberService;

    @Autowired
    private UserFormValidator userFormValidator;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    HttpSession session;

    @InitBinder
    private void initUserBinder(WebDataBinder binder) {
        binder.setValidator(userFormValidator);
    }

    /**
     * Create user model and view.
     *
     * @return the model and view
     */
    @RequestMapping(value = "/admin/user/create", method = RequestMethod.GET)
    public ModelAndView createUser() {
        ModelAndView mav = new ModelAndView("new/admin/create");
        mav.addObject("userDto", new UserDto());
        return mav;
    }

    /**
     * Create user string.
     *
     * @param userDto the user dto
     * @param result  the result
     * @param map     the map
     * @param session the session
     * @return the string
     */
    @RequestMapping(value = "/admin/user/create", method = RequestMethod.POST)
    public String createUser(@ModelAttribute("userDto") @Validated UserDto userDto, BindingResult result,
                                                        ModelMap map, HttpSession session) throws Exception {
        if (result.hasErrors()) {
            return "new/admin/create";
        }
        if (userService.isExist(userDto.getEmail(), userDto.getPassport())) {
            map.put("status", -1);
            return "new/admin/create";
        }
        session.setAttribute("newUser", userDto);
        map.put("tariffDtos",tariffService.getActiveTariffs());
        map.put("numbers", phoneNumberService.getFreeNumbers());
        return "new/admin/create_contract";
    }

    /**
     * Admin account model and view.
     *
     * @param session the session
     * @return the model and view
     */
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView adminAccount(HttpSession session) {
        ModelAndView mav = new ModelAndView();
        UserDto userDto = (UserDto) session.getAttribute("userLoginDto");
        if (userDto.getRole() == null || !userDto.getRole().equals(Role.ADMINISTRATOR)) {
            mav.setViewName("new/access_denied");
        } else {
            mav.setViewName("new/admin/admin_account");
            mav.addObject("userLoginDto");
        }
        return mav;
    }

    /**
     * User account model and view.
     *
     * @param session the session
     * @return the model and view
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ModelAndView userAccount(HttpSession session) {
        ModelAndView mav = new ModelAndView();
        UserDto userDto = (UserDto) session.getAttribute("userLoginDto");
        if (userDto.getRole() == null || !userDto.getRole().equals(Role.USER)) {
            mav.setViewName("new/access_denied");
        } else {
            mav.setViewName("new/user/user_account");
            mav.addObject("userLoginDto");
        }
        return mav;
    }

    /**
     * Gets all users.
     *
     * @param map the map
     * @return the all users
     */
    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String getAllUsers(ModelMap map) throws Exception {
        List<UserDto> userDtos = userService.getAllUsers();
        if (userDtos.size() == 0){
            map.put("usersErr",messageSource.getMessage("Admin.users", null, null));
            return "new/admin/users";
        }
        map.put("userDtos", userDtos);
        return "new/admin/users";
    }

    /**
     * Remove user by id string.
     *
     * @param id  the id
     * @param map the map
     * @return the string
     */
    @RequestMapping(value = "/admin/user/remove/{id}", method = RequestMethod.POST)
    public String removeUserById(@PathVariable Long id, ModelMap map) {
        if (userService.removeUserById(id)) {
            map.addAttribute("success"
                    , messageSource.getMessage("Admin.user.remove.true", null, null));
        } else {
            map.addAttribute("error"
                    , messageSource.getMessage("Admin.user.remove.false", null, null));
        }
        List<UserDto> userDtos = userService.getAllUsers();
        if (userDtos.size() == 0){
            map.put("usersErr",messageSource.getMessage("Admin.users", null, null));
            return "new/admin/users";
        }
        map.put("userDtos", userDtos);
        return "new/admin/users";

    }

    /**
     * Confirmation string.
     *
     * @param status the status
     * @return the string
     */
    @RequestMapping(value = "/stop", method = RequestMethod.GET)
    public String confirmation(SessionStatus status) {
        status.setComplete();
        return "redirect:/main/auth";
    }

    @ExceptionHandler(Exception.class)
    public String dbError(Exception e){
        logger.error("Error: "+e.getMessage());
        return "new/access_denied";
    }

}