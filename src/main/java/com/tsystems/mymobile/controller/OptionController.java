package com.tsystems.mymobile.controller;

import com.tsystems.mymobile.service.api.OptionService;
import com.tsystems.mymobile.service.dto.option.OptionAdminDto;
import com.tsystems.mymobile.service.dto.option.OptionDto;
import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import com.tsystems.mymobile.service.dto.user.UserDto;
import com.tsystems.mymobile.utils.Enums.Role;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import com.tsystems.mymobile.utils.Validators.OptionConnectionValidator;
import com.tsystems.mymobile.utils.Validators.OptionFormValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Option controller.
 */
@Controller
@RequestMapping(value = "/main")
public class OptionController {

    private final static Logger logger = Logger.getLogger(ContractController.class);

    @Autowired
    private OptionService optionService;
    @Autowired
    private OptionConnectionValidator connValidator;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private OptionFormValidator validator;
    @Autowired
    private HttpSession session;

    /**
     * Create option for tariff string.
     *
     * @param optionDto the option dto
     * @param map       the map
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/admin/option/create", method = RequestMethod.POST)
    public String createOptionForTariff(@ModelAttribute("optionDto") OptionSimpleDto optionDto, ModelMap map)
            throws DataBaseException {
        Long tariffId = (Long) session.getAttribute("updateId");
        if (!validator.validate(optionDto)){
            return "/mymobile/main/admin/tariff/" + tariffId;
        }
        if (optionService.isExist(optionDto.getTittle())) {
            return "/mymobile/main/admin/tariff/" + tariffId;
        }
        OptionSimpleDto optionSimpleDto = optionService.createOptionForTariff(tariffId, optionDto);
        if (optionSimpleDto.getId() == null) {
            map.addAttribute("optionErr", messageSource.getMessage("Option.create", null, null));
        }
        return "/mymobile/main/admin/tariff/" + tariffId;
    }

    /**
     * Update option string.
     *
     * @param optionSimpleDto the option simple dto
     * @param map             the map
     * @return the string
     * @throws DataBaseException the data base exception
     */
    @RequestMapping(value = "/admin/option/update", method = RequestMethod.POST)
    public String updateOption(@ModelAttribute("option") OptionSimpleDto optionSimpleDto, ModelMap map) throws DataBaseException {
        if (!validator.validate(optionSimpleDto)){
            map.addAttribute("optUpErr"
                    ,messageSource.getMessage("Option.update.error",null,null));
            return "new/admin/option_update";
        }
        OptionSimpleDto newOptionSimpleDto = optionService.updateOption(optionSimpleDto);
        if (newOptionSimpleDto.getId() == null) {
            map.addAttribute("option", optionSimpleDto);
            return "new/admin/option_update";
        }
        map.addAttribute("option", newOptionSimpleDto);
        return "new/admin/option_update";
    }

    /**
     * Remove req option by id string.
     *
     * @param reqOptionTittle the req option tittle
     * @param optionId        the option id
     * @param map             the map
     * @return the string
     */
    @RequestMapping(value = "/admin/option/remove_req/{reqOptionTittle}", method = RequestMethod.POST)
    public String removeReqOptionById(@PathVariable String reqOptionTittle, @RequestParam String optionId, ModelMap map) {
        optionService.removeReqOptionById(reqOptionTittle, Long.valueOf(optionId));
        map.addAttribute("option", optionService.getAdminOptionById(Long.valueOf(optionId)));
        return "new/admin/optionDto";
    }

    /**
     * Remove dis option by id string.
     *
     * @param disOptionTittle the dis option tittle
     * @param optionId        the option id
     * @param map             the map
     * @return the string
     */
    @RequestMapping(value = "/admin/option/remove_dis/{disOptionTittle}", method = RequestMethod.POST)
    public String removeDisOptionById(@PathVariable String disOptionTittle,
                                      @RequestParam String optionId, ModelMap map) {
        optionService.removeDisOptionById(disOptionTittle, Long.valueOf(optionId));
        map.addAttribute("option", optionService.getAdminOptionById(Long.valueOf(optionId)));
        return "new/admin/optionDto";
    }

    /**
     * Add req option by id string.
     *
     * @param reqOptionTittle the req option tittle
     * @param optionId        the option id
     * @param map             the map
     * @return the string
     */
    @RequestMapping(value = "/admin/option/add_req/{reqOptionTittle}", method = RequestMethod.POST)
    public String addReqOptionById(@PathVariable String reqOptionTittle,
                                   @RequestParam String optionId, Model map) {
        optionService.addReqOptionById(reqOptionTittle, Long.valueOf(optionId));
        map.addAttribute("option", optionService.getAdminOptionById(Long.valueOf(optionId)));
        return "new/admin/optionDto";
    }

    /**
     * Add dis option by id string.
     *
     * @param disOptionTittle the dis option tittle
     * @param optionId        the option id
     * @param map             the map
     * @return the string
     */
    @RequestMapping(value = "/admin/option/add_dis/{disOptionTittle}", method = RequestMethod.POST)
    public String addDisOptionById(@PathVariable String disOptionTittle,
                                   @RequestParam String optionId, ModelMap map) {
        optionService.addDisOptionById(disOptionTittle, Long.valueOf(optionId));
        map.addAttribute("incOptSuccess",
                messageSource.getMessage("Add.inc.option.success", null, null));
        map.addAttribute("option", optionService.getAdminOptionById(Long.valueOf(optionId)));
        return "new/admin/optionDto";
    }

    /**
     * Remove option by id string.
     *
     * @param optionId the option id
     * @param map      the map
     * @param session  the session
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/admin/option/remove/{optionId}", method = RequestMethod.POST)
    public String removeOptionById(@PathVariable Long optionId, ModelMap map, HttpSession session) {
        Long tariffId = (Long) session.getAttribute("updateId");
        if (!optionService.removeOptionById(optionId)) {
            map.addAttribute("removeOption",
                    messageSource.getMessage("Option.remove", null, null));
            return "/mymobile/main/admin/tariff/" + tariffId;
        }
        map.addAttribute("removeOption",
                messageSource.getMessage("Option.remove.success", null, null));

        return "/mymobile/main/admin/tariff/" + tariffId;
    }

    /**
     * User basket string.
     *
     * @param session the session
     * @param map     the map
     * @return the string
     */
    @RequestMapping(value = "/user/basket", method = RequestMethod.GET)
    public String userBasket(HttpSession session, ModelMap map) {
        List<OptionDto> optionDtos = (List<OptionDto>) session.getAttribute("basketList");
        map.addAttribute("basketOptions", optionDtos);
        return "new/basket";
    }

    /**
     * Put option into basket list.
     *
     * @param optionId the option id
     * @param id       the id
     * @param session  the session
     * @param map      the map
     * @return the list
     */
    @ResponseBody
    @RequestMapping(value = "/user/basket/add", method = RequestMethod.POST)
    public List<String> putOptionIntoBasket(@RequestParam String optionId, @RequestParam String id,
                                            HttpSession session, ModelMap map) {
        Long contractDtoId = Long.valueOf(id);
        session.setAttribute("contractBasket", contractDtoId);
        UserDto userDto = (UserDto) session.getAttribute("userLoginDto");
        List<OptionDto> basketOpts = (List<OptionDto>) session.getAttribute("basketList");
        OptionDto optionDto = optionService.getOptionById(Long.valueOf(optionId));
        if (optionDto.getId() == null) {
            map.addAttribute("optionErr"
                    , messageSource.getMessage("Option.NotFind", null, null));
        }
        List<String> params = new ArrayList<>();
        Integer status = connValidator.isValidate(optionDto, basketOpts, contractDtoId);
        if (!status.equals(0)) {
            params.add("0");
            if (status.equals(-1)) {
                if (userDto.getRole().equals(Role.ADMINISTRATOR)) {
                    params.add("/mymobile/main/admin/user/contract/" + id);
                    session.setAttribute("errStatus","-1");
                    return params;
                }
                session.setAttribute("errStatus","-1");
                params.add("/mymobile/main/user/contract/" + id);
                return params;
            }
            if (userDto.getRole().equals(Role.ADMINISTRATOR)) {
                params.add("/mymobile/main/admin/user/contract/" + id);
                session.setAttribute("errStatus","-2");
                return params;
            }
            params.add("/mymobile/main/user/contract/" + id);
            session.setAttribute("errStatus","-2");
            return params;
        }
        if (!basketOpts.contains(optionDto)) {
            basketOpts.add(optionDto);
            params.add("1");
        } else {
            map.addAttribute("status", "0");
        }

        session.setAttribute("basketList", basketOpts);
        if (userDto.getRole().equals(Role.ADMINISTRATOR)) {
            params.add("/mymobile/main/admin/user/contract/" + id);
            return params;
        }
        params.add("/mymobile/main/user/contract/" + id);
        return params;
    }

    /**
     * Remove option from basket string.
     *
     * @param optionId the option id
     * @param session  the session
     * @param map      the map
     * @return the string
     */
    @RequestMapping(value = "/user/basket/option/remove/{optionId}", method = RequestMethod.POST)
    public String removeOptionFromBasket(@PathVariable Long optionId, HttpSession session, ModelMap map) {
        List<OptionDto> optionDtos = (List<OptionDto>) session.getAttribute("basketList");
        optionDtos.removeIf(option -> option.getId().equals(optionId));
        map.addAttribute("basketOptions", optionDtos);
        return "new/basket";
    }

    /**
     * Connect options string.
     *
     * @param session the session
     * @param map     the map
     * @return the string
     * @throws DataBaseException the data base exception
     */
    @ResponseBody
    @RequestMapping(value = "/user/options/connect", method = RequestMethod.POST)
    public String connectOptions(HttpSession session, ModelMap map) throws DataBaseException {
        UserDto userDto = (UserDto) session.getAttribute("userLoginDto");
        List<OptionDto> optionDtos = (List<OptionDto>) session.getAttribute("basketOptions");
        List<Long> optionIds = optionDtos.stream()
                .map(OptionDto::getId)
                .collect(Collectors.toList());
        Long contractId = (Long) session.getAttribute("contractBasket");

        if (!optionService.connectOptionsByIds(contractId, connValidator.sortConnList(optionIds, contractId))) {
            map.addAttribute("connectErr"
                    , messageSource.getMessage("Options.connect.error", null, null));
        } else {
            map.addAttribute("connectSucc"
                    , messageSource.getMessage("Options.connect.success", null, null));
            optionDtos.clear();
            session.setAttribute("basketList", optionDtos);
        }
        if (userDto.getRole().equals(Role.ADMINISTRATOR)) {
            return "/mymobile/main/admin/user/contract/" + contractId;
        }
        return "/mymobile/main/user/contract/" + contractId;
    }

    /**
     * Disconnect options string.
     *
     * @param optionDtoId the option dto id
     * @param session     the session
     * @param map         the map
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/user/option/disconnect", method = RequestMethod.POST)
    public String disconnectOptions(@RequestParam String optionDtoId, HttpSession session, ModelMap map) {
        UserDto userDto = (UserDto) session.getAttribute("userLoginDto");
        Long contractId = (Long) session.getAttribute("contractBasket");
        if (!optionService.disconnectOptionById(contractId, Long.valueOf(optionDtoId))) {
            map.addAttribute("disErr"
                    , messageSource.getMessage("Options.disconnect.error", null, null));
            return "/mymobile/main/user/contract" + contractId;
        }
        connValidator.resetConnOptions(contractId);
        map.addAttribute("disSucc"
                , messageSource.getMessage("Options.disconnect.success", null, null));
        if (userDto.getRole().equals(Role.ADMINISTRATOR)) {
            return "/mymobile/main/admin/user/contract/" + contractId;
        }
        return "/mymobile/main/user/contract/" + contractId;
    }

    /**
     * Show option string.
     *
     * @param optionDtoId the option dto id
     * @param map         the map
     * @return the string
     */
    @RequestMapping(value = "/user/option/show", method = RequestMethod.POST)
    public String showOption(@RequestParam String optionDtoId, ModelMap map) {
        OptionAdminDto option = optionService.getAdminOptionById(Long.valueOf(optionDtoId));
        map.addAttribute("option", option);
        return "new/admin/optionDto";
    }

    /**
     * Db error string.
     *
     * @param e the e
     * @return the string
     */
    @ExceptionHandler(Exception.class)
    public String dbError(Exception e) {
        logger.error("DataBase error: " + e.getMessage());
        return "/mymobile/main/oops";
    }

}