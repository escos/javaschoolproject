package com.tsystems.mymobile.controller;

import com.google.gson.Gson;
import com.tsystems.mymobile.service.api.PhoneNumberService;
import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;
import com.tsystems.mymobile.service.dto.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Phone number controller.
 */
@Controller
@SessionAttributes(types = UserDto.class)
@RequestMapping("/main")
public class PhoneNumberController {

    @Autowired
    private PhoneNumberService phoneNumberService;
    @Autowired
    private MessageSource messageSource;

//    /**
//     * Gets free numbers.
//     *
//     * @param map the map
//     * @return the free numbers
//     */
//    @RequestMapping(value = "/admin/numbers", method = RequestMethod.GET)
//    public String getFreeNumbers(ModelMap map) {
//        List<PhoneNumberDto> phoneNumberDtos = phoneNumberService.getFreeNumbers();
//        if (phoneNumberDtos.size() == 0){
//            map.put("userContractsErr", messageSource.getMessage("Numbers.Free",null,null));
//            return "adminAccount/getNumbers";
//        }
//        map.put("freeNumbers",phoneNumberDtos);
//        return "adminAccount/getNumbers";
//    }

    /**
     * Gets numbers.
     *
     * @param map the map
     * @return the numbers
     */
    @ResponseBody
    @RequestMapping(value = "/numbers", method = RequestMethod.POST)
    public String getNumbers(ModelMap map) {
        List<PhoneNumberDto> phoneNumberDtos = phoneNumberService.getFreeNumbers();
        if (phoneNumberDtos.size() == 0){
            return "new/oops";
        }
        List<String> numbers = phoneNumberDtos.stream().map(number->number.getId().toString())
                .collect(Collectors.toList());
        Gson gson = new Gson();
        return gson.toJson(numbers);
    }

    /**
     * Gets random number.
     *
     * @param map the map
     * @return the random number
     */
    @ResponseBody
    @RequestMapping(value = "/number", method = RequestMethod.POST)
    public String getRandomNumber(ModelMap map) {
        PhoneNumberDto rndmNumber = phoneNumberService.getFreeNumber();
        if (rndmNumber.getId() == null){
            return "Error! Try again later.";
        }
        map.put("rndmNumber",rndmNumber.getId());
        return rndmNumber.getId().toString();
    }

}
