package com.tsystems.mymobile.controller;

import com.tsystems.mymobile.service.api.ContractService;
import com.tsystems.mymobile.service.api.PhoneNumberService;
import com.tsystems.mymobile.service.api.TariffService;
import com.tsystems.mymobile.service.api.UserService;
import com.tsystems.mymobile.service.dto.contract.ContractSimpleDto;
import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;
import com.tsystems.mymobile.service.dto.option.OptionDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.service.dto.user.UserDto;
import com.tsystems.mymobile.utils.Enums.Role;
import com.tsystems.mymobile.utils.Validators.LoginFormValidator;
import com.tsystems.mymobile.utils.jms.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Authorize user controller.
 */
@Controller
@SessionAttributes(types = UserDto.class)
@RequestMapping("/main")
public class AuthorizeUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private PhoneNumberService phoneNumberService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LoginFormValidator loginFormValidator;

    @Autowired
    private Sender sender;

    @InitBinder
    private void initUserLoginBinder(WebDataBinder binder) {
        binder.setValidator(loginFormValidator);
    }

    /**
     * Shows main page of the app.
     *
     * @return the model and view
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView main() {
        ModelAndView mav = new ModelAndView("new/main");
        PhoneNumberDto numberDto = phoneNumberService.getFreeNumber();
        if(numberDto.getId() == null){
            mav.addObject("Problems",messageSource.getMessage("Problems",null,null));
        }
        List<TariffDto> tariffDtos = tariffService.getActiveTariffs();
        if(tariffDtos.isEmpty()){
            mav.addObject("ProblemsTariff",messageSource.getMessage("Problems",null,null));
        }
        mav.addObject("rndmNumber", numberDto.getId());
        mav.addObject("tariffDtos", tariffDtos.stream().limit(4).collect(Collectors.toList()));
        sender.send("all","tariffs");
        return mav;
    }

    /**
     * Show login form page for authorization.
     *
     * @param session the session
     * @return the model and view
     */
    @RequestMapping(value = "/auth",method = RequestMethod.GET)
    public ModelAndView doAuthorize(HttpSession session) {
        ModelAndView mav = new ModelAndView("new/auth");
        session.setAttribute("userLoginDto", new UserDto());
        mav.addObject("userLoginDto", new UserDto());
        return mav;
    }

    /**
     * Do authorize: check if exist user with email and password.
     * Also check input data for correctness.
     * If user exist and role is admin: redirect to admin account page,
     * otherwise redirect to user account page
     *
     *
     * @param userDto the user dto
     * @param result  input date(form)
     * @param map     the map
     * @param session the session
     * @return the string(redirect to page)
     */
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public String doAuthorize(@ModelAttribute("userLoginDto") @Validated UserDto userDto, BindingResult result,
                              ModelMap map, HttpSession session) {
        if (result.hasErrors()) {
            return "new/auth";
        }
        UserDto userLoginDto = userService.authorise(userDto);
        if (userLoginDto.getName() == null) {
            userLoginDto.setId(-1L);
            map.put("userLoginDto", userLoginDto);
            return "new/auth";
        }

        map.put("userLoginDto", userLoginDto);
        session.setAttribute("userLoginDto", userLoginDto);
        List<OptionDto> optionDtos = new ArrayList<>();
        session.setAttribute("basketList", optionDtos);
        if (userLoginDto.getRole().equals(Role.USER)) {
            List<ContractSimpleDto> contractSimpleDtos = contractService.getContractByUserId(userLoginDto.getId());
            session.setAttribute("contractSimpleDtos", contractSimpleDtos);
            return "redirect:/main/user";
        }
        List<PhoneNumberDto> numbers = phoneNumberService.getUsedNumbers();
        if (numbers.size() == 0){
            map.put("numbersErr", messageSource.getMessage("Numbers.Used",null,null));
            return "redirect:/main/admin";
        }
        map.put("usedNumbers",numbers);
        return "redirect:/main/admin";
    }

    /**
     * Redirect t error page.
     *
     * @return the model and view
     */
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView error() {
        return new ModelAndView("new/error_system");
    }

    /**
     * Redirect t error page.
     *
     * @return the model and view
     */
    @RequestMapping(value = "/oops", method = RequestMethod.GET)
    public ModelAndView oops() {
        return new ModelAndView("new/oops");
    }

    @RequestMapping(value = "/bad_data", method = RequestMethod.GET)
    public ModelAndView badData() {
        return new ModelAndView("new/bad_data");
    }

}
