package com.tsystems.mymobile.controller;

import com.tsystems.mymobile.service.api.TariffService;
import com.tsystems.mymobile.service.dto.tariff.TariffSimpleDto;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Restful controller.
 */
@RestController
@RequestMapping("/main")
public class RestfulController {

    private static final Logger logger = Logger.getLogger(RestfulController.class);

    @Autowired
    private TariffService tariffService;

    /**
     * Get updated tariff by id list.
     *
     * @return the list
     */
    @RequestMapping(value = "/updated_tariffs", method = RequestMethod.GET, produces = "application/json")
    public List<TariffSimpleDto> getUpdatedTariffById() throws DataBaseException {
        if (tariffService.getSimpleTariffs().isEmpty()){
            logger.error("List is empty.Transmitting failed.");
            throw new DataBaseException("Get tariffs error!");
        }
        logger.info("Date receive to banner");
        return tariffService.getSimpleTariffs();
    }

    /**
     * Db error string.
     *
     * @param e the e
     * @return the string
     */
    @ExceptionHandler(Exception.class)
    public String dbError(Exception e){
        logger.error("DataBase error: "+e.getMessage());
        return "new/oops";
    }
}
