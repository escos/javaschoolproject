package com.tsystems.mymobile.controller;

import com.tsystems.mymobile.service.api.*;
import com.tsystems.mymobile.service.dto.contract.ContractDto;
import com.tsystems.mymobile.service.dto.contract.ContractSimpleDto;
import com.tsystems.mymobile.service.dto.number.PhoneNumberDto;
import com.tsystems.mymobile.service.dto.option.OptionDto;
import com.tsystems.mymobile.service.dto.option.OptionTittleDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.service.dto.user.UserDto;
import com.tsystems.mymobile.utils.Email.SendHTMLEmail;
import com.tsystems.mymobile.utils.Enums.BlockStatus;
import com.tsystems.mymobile.utils.Enums.Role;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import com.tsystems.mymobile.utils.Validators.NumberValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Contract controller.
 */
@Controller
@RequestMapping("/main")
public class ContractController {

    private final static Logger logger = Logger.getLogger(ContractController.class);

    @Autowired
    private ContractService contractService;
    @Autowired
    private TariffService tariffService;
    @Autowired
    private PhoneNumberService phoneNumberService;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private OptionService optionService;

    /**
     * Gets all contracts(not deleted)
     * If number of the contracts is 0 then
     * output error message(db error).
     *
     * @param map     the map
     * @param session the session
     * @return the all contracts
     */
    @RequestMapping(value = "/admin/contracts", method = RequestMethod.POST)
    public String getAllContracts(ModelMap map, HttpSession session) {
        List<ContractSimpleDto> contractSimpleDtos = contractService.getAllContracts();
        if (contractSimpleDtos.size() == 0) {
            map.put("contractsErr", messageSource.getMessage("Admin.contracts", null, null));
            return "new/admin/contracts";
        }
        map.put("contractSimpleDtos", contractSimpleDtos);
        new SendHTMLEmail().sendEmail();
        session.setAttribute("thisUser", null);
        return "new/admin/contracts";
    }

    /**
     * Gets all existing contracts by user id.
     *
     *
     * @param userId  the user id
     * @param session the session
     * @param map     the map
     * @return the contracts by user id
     * @throws Exception the exception
     */
    @RequestMapping(value = "/admin/contracts/{userId}", method = RequestMethod.POST)
    public String getContractsByUserId(@PathVariable Long userId, HttpSession session, ModelMap map) {
        List<ContractSimpleDto> contractSimpleDtos = contractService.getContractByUserId(userId);
        UserDto userDto = userService.getUserById(userId);
        if (contractSimpleDtos.isEmpty()) {
            map.put("contractsErr", messageSource.getMessage("Admin.contracts", null, null));
            return "new/admin/contracts";
        }
        map.put("contractSimpleDtos", contractSimpleDtos);
        session.setAttribute("thisUser", userDto);
        return "new/admin/contracts";
    }

    /**
     * Search contract by phoneNumber
     *
     * @param numberId the number id
     * @param map      the map
     * @return the contract by number
     */
    @RequestMapping(value = "/admin/search", method = RequestMethod.POST)
    public String getContractByNumber(@RequestParam String numberId, ModelMap map) {
        if (!NumberValidator.validate(numberId)) {
            map.addAttribute("parseErr"
                    , messageSource.getMessage("Contract.search.Number", null, null));
            return "new/admin/main_content";
        }
        Long id = Long.valueOf(numberId);
        ContractSimpleDto contractDto = contractService.getContractByNumber(id);
        if (contractDto.getId() == null) {
            map.addAttribute("existErr"
                    , messageSource.getMessage("Contract.search.NotExist", null, null));
            return "new/admin/main_content";
        }
        map.put("contractSimpleDtos", Collections.singletonList(contractDto));
        return "new/admin/contracts";
    }

    /**
     * Gets all contracts for user.
     *
     * @param map     the map
     * @param session the session
     * @return the contracts for user
     */
    @RequestMapping(value = "/user/contracts", method = RequestMethod.GET)
    public String getContractsForUser(ModelMap map, HttpSession session)  throws DataBaseException {
        UserDto user = (UserDto) session.getAttribute("userLoginDto");
        List<ContractSimpleDto> contractSimpleDtos = contractService.getContractByUserId(user.getId());
        if (contractSimpleDtos.isEmpty()){
            logger.error("Get contracts error");
            throw new DataBaseException("Data base error");
        }
        map.put("contractSimpleDtos", contractSimpleDtos);
        return "new/user/user_account";
    }

    /**
     * Gets tariff by contract id.
     *
     * @param id      the id
     * @param session the session
     * @param map     the map
     * @return the tariff by contract id
     * @throws Exception the exception
     */
    @RequestMapping(value = "/user/contract/{id}", method = RequestMethod.POST)
    public String getTariffByContractId(@PathVariable Long id, HttpSession session, ModelMap map) throws Exception {
        List<OptionDto> basket = (List<OptionDto>) session.getAttribute("basketList");
        ContractDto contractDto = contractService.getContractById(id);
        Long contractId = (Long) session.getAttribute("contractId");
        if (!id.equals(contractId)) {
            basket.clear();
        }

        if (session.getAttribute("errStatus").equals("1")){
            map.addAttribute("tarSuccess",  messageSource
                    .getMessage("User.contract.update.success", null, null));
        }
        if (session.getAttribute("errStatus").equals("-1")){
            map.addAttribute("disOptsError", messageSource
                    .getMessage("Option.dis.error", null, null));
        }
        if (session.getAttribute("errStatus").equals("-2")){
            map.addAttribute("reqOptsError", messageSource
                    .getMessage("Option.req.error", null, null));
        }
        session.setAttribute("errStatus","0");
        if (contractDto.getId() == null) {
            map.put("contractErr", messageSource.getMessage("User.contract", null, null));
            return "new/user/user_tariff";
        }
        List<TariffDto> freeTariffDtos = tariffService
                .getEnableTariffs(contractDto.getTariffDto().getTittle());
        if (freeTariffDtos.size() == 0) {
            map.put("tariffErr", messageSource.getMessage("User.tariff", null, null));
            return "new/user/user_tariff";
        }
        List<OptionDto> tariffOptions = new ArrayList<>(contractDto.getTariffDto().getOptionDtos());
        List<OptionDto> freeOptions = tariffOptions
                .stream()
                .filter(option -> !contractDto.getConnectedOptions().contains(option))
                .collect(Collectors.toList());
        freeOptions.removeAll(basket);
        Set<OptionTittleDto> freeOptionTittleDtos = freeOptions
                .stream()
                .map(option->optionService.getOptionByTittle(option.getTittle()))
                .collect(Collectors.toSet());
        session.setAttribute("contractBasket", id);
        map.put("blockStatus",contractDto.getBlockStatus());
        map.put("contractId", id);
        map.put("connectedOptions", contractDto.getConnectedOptions());
        map.put("tariff", contractDto.getTariffDto());
        map.put("freeTariffs", freeTariffDtos);
        map.put("freeOptions", freeOptions);
        map.put("freeTittleOptions",freeOptionTittleDtos);
        map.put("number", contractDto.getPhoneNumberDto().getId());
        return "new/user/user_tariff";
    }

    /**
     * Contract block by id string.
     *
     * @param id      the id
     * @param map     the map
     * @param session the session
     * @return the string
     */
    @RequestMapping(value = "/user/contract/block/{id}", method = RequestMethod.POST)
    public String contractBlockById(@PathVariable Long id, ModelMap map, HttpSession session) {
        Boolean status = contractService.contractBlockById(id);
        UserDto userDto = (UserDto) session.getAttribute("userLoginDto");
        List<ContractSimpleDto> contractSimpleDtos = contractService.getContractByUserId(userDto.getId());
        if (contractSimpleDtos.size() == 0) {
            map.put("userContractsErr", messageSource.getMessage("User.contracts", null, null));
            return "new/user/user_body";
        }
        if (status) {
            map.addAttribute("userBlock",
                    messageSource.getMessage("User.block", null, null));
        }
        map.addAttribute("contractSimpleDtos", contractSimpleDtos);
        return "new/user/user_body";
    }

    /**
     * Create contract string.
     *
     * @param map the map
     * @return the string
     */
    @RequestMapping(value = "/admin/contract/create", method = RequestMethod.GET)
    public String createContract(ModelMap map) {
        List<TariffDto> tariffDtos = tariffService.getActiveTariffs();
        List<PhoneNumberDto> freeNumbers = phoneNumberService.getFreeNumbers();
        map.put("tariffDtos", tariffDtos);
        map.put("numbers", freeNumbers);
        return "new/admin/create_contract";
    }

    /**
     * Create contract string.
     *
     * @param map         the map
     * @param session     the session
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = "/admin/contract/create", method = RequestMethod.POST)
    public String createContract(@RequestParam String tariffId, @RequestParam String number, ModelMap map,
                                 HttpSession session){
        ContractDto contractDto = new ContractDto();
        UserDto userDto = (UserDto) session.getAttribute("newUser");
        UserDto sessionUser = (UserDto) session.getAttribute("thisUser");
        if (userDto == null) {
            userDto = sessionUser;
        } else {
            Long userId = userService.createUser(userDto);
            if (userId.equals(-1L)) {
                map.addAttribute("statusCreate", -2);
                return "new/admin/create";
            }
            userDto.setId(userId);
        }
        Long numberId = Long.valueOf(number);
        contractDto.setPhoneNumberDto(phoneNumberService.getNumberById(numberId));
        contractDto.setUserDto(userDto);
        Long tariffDtoId = Long.valueOf(tariffId);
        contractDto.setTariffDto(tariffService.getTariffById(tariffDtoId));
        contractDto.setBlockStatus(BlockStatus.UNBLOCKING);
        contractDto.setConnectedOptions(new HashSet<>());

        if (contractService.createContract(contractDto).equals(-1L)) {
            map.addAttribute("statusCreate", -1);
            return "new/admin/create";
        }
        map.put("statusCreate", 1);
        map.put("number", contractDto.getPhoneNumberDto().getId());
        List<ContractSimpleDto> contracts = contractService.getContractByUserId(userDto.getId());
        if(contracts.isEmpty()){
            map.put("contractsErr", messageSource.getMessage("Admin.contracts", null, null));
            return "new/admin/contracts";
        }
        map.put("contractSimpleDtos", contracts);
        session.setAttribute("thisUser", userDto);
        return "new/admin/contracts";
    }

    /**
     * Contract admin block by id string.
     *
     * @param id      the id
     * @param map     the map
     * @return the string
     */
    @RequestMapping(value = "/admin/contract/block/{id}", method = RequestMethod.POST)
    public String contractAdminBlockById(@PathVariable Long id, ModelMap map) {
        if (contractService.contractAdminBlockById(id)) {
            map.addAttribute("userBlock",
                    messageSource.getMessage("User.block", null, null));
        } else {
            map.addAttribute("userBlock",
                    messageSource.getMessage("User.block.fail", null, null));
        }
        List<ContractSimpleDto> contractSimpleDtos = contractService.getAllContracts();
        if (contractSimpleDtos.size() == 0) {
            map.addAttribute("contractsErr",
                    messageSource.getMessage("User.contracts", null, null));

        } else {
            map.addAttribute("contractSimpleDtos", contractSimpleDtos);
        }
        return "new/admin/contracts";


    }

    /**
     * Update contract by tariff string.
     *
     * @param tariffId the tariff id
     * @param session  the session
     * @param map      the map
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/user/contract/tariff/update", method = RequestMethod.POST)
    public String updateContractByTariff(@RequestParam String tariffId, HttpSession session, ModelMap map)
                                                                                    throws DataBaseException {
        UserDto userDto = (UserDto) session.getAttribute("userLoginDto");
        Long contractId = Long.valueOf(session.getAttribute("contractId").toString());
        Long tariff = Long.valueOf(tariffId);
        if (!contractService.updateContractByTariff(contractId, tariff)) {
            throw new DataBaseException("Update tariff error");
        }
        List<OptionDto> optionDtos = (List<OptionDto>) session.getAttribute("basketList");
        optionDtos.clear();
        session.setAttribute("errStatus", "1");
        if (userDto.getRole().equals(Role.USER)) {
            return "/mymobile/main/user/contract/"+contractId;
        }
        return "/mymobile/main/admin/user/contract/" + contractId;
    }

    /**
     * Remove contract by id string.
     *
     * @param contractId the contract id
     * @param map        the map
     * @return the string
     */
    @RequestMapping(value = "/admin/contract/remove/{contractId}", method = RequestMethod.POST)
    public String removeContractById(@PathVariable Long contractId, ModelMap map) {
        if (!contractService.removeContractById(contractId)) {
            map.addAttribute("error", messageSource
                    .getMessage("Admin.contract.delete.fail", null, null));
        } else {
            map.addAttribute("success", messageSource
                    .getMessage("Admin.contract.remove.true", null, null));
        }
        List<ContractSimpleDto> contractSimpleDtos = contractService.getAllContracts();
        if (contractSimpleDtos.size() == 0) {
            map.put("contractsErr", messageSource.getMessage("Admin.contracts", null, null));
            return "new/admin/contracts";
        }
        map.put("contractSimpleDtos", contractSimpleDtos);
        return "new/admin/contracts";


    }

    /**
     * Gets user contract by id.
     *
     * @param id      the id
     * @param session the session
     * @param map     the map
     * @return the user contract by id
     * @throws Exception the exception
     */
    @RequestMapping(value = "/admin/user/contract/{id}", method = RequestMethod.POST)
    public String getUserContractById(@PathVariable Long id, HttpSession session, ModelMap map) throws Exception{
        List<OptionDto> basket = (List<OptionDto>) session.getAttribute("basketList");
        Long contractId = (Long) session.getAttribute("contractId");
        if (!id.equals(contractId)) {
            basket.clear();
        }
        ContractDto contractDto = contractService.getContractById(id);
        if (session.getAttribute("errStatus").equals("1")){
            map.addAttribute("tarSuccess",  messageSource
                    .getMessage("User.contract.update.success", null, null));
        }
        if (session.getAttribute("errStatus").equals("-1")){
            map.addAttribute("disOptsError", messageSource
                    .getMessage("Option.dis.error", null, null));
        }
        if (session.getAttribute("errStatus").equals("-2")){
            map.addAttribute("reqOptsError", messageSource
                    .getMessage("Option.req.error", null, null));
        }
        session.setAttribute("errStatus","0");
        if (contractDto.getId() == null) {
            map.put("contractErr", messageSource.getMessage("User.contract", null, null));
            return "new/admin/user_contract";
        }
        List<TariffDto> freeTariffDtos = tariffService
                .getEnableTariffs(contractDto.getTariffDto().getTittle());
        if (freeTariffDtos.size() == 0) {
            map.put("tariffErr", messageSource.getMessage("User.tariff", null, null));
            return "new/admin/user_contract";
        }
        List<OptionDto> tariffOptions = new ArrayList<>(contractDto.getTariffDto().getOptionDtos());
        List<OptionDto> freeOptions = tariffOptions
                .stream()
                .filter(option -> !contractDto.getConnectedOptions().contains(option))
                .collect(Collectors.toList());
        freeOptions.removeAll(basket);
        Set<OptionTittleDto> freeOptionTittleDtos = freeOptions
                .stream()
                .map(option->optionService.getOptionByTittle(option.getTittle()))
                .collect(Collectors.toSet());
        session.setAttribute("contractBasket", id);
        map.put("status",contractDto.getBlockStatus());
        map.put("basket_status", session.getAttribute("basket_status"));
        map.put("number", contractDto.getPhoneNumberDto().getId());
        map.put("userShow",contractDto.getUserDto());
        map.put("contractId", id);
        map.put("connectedOptions", contractDto.getConnectedOptions());
        map.put("tariff", contractDto.getTariffDto());
        map.put("freeTariffs", freeTariffDtos);
        map.put("freeTittleOptions",freeOptionTittleDtos);
        map.put("freeOptions", freeOptions);
        map.put("number", contractDto.getPhoneNumberDto().getId());
        return "new/admin/user_contract";
    }

    /**
     * Db error string.
     *
     * @param e the e
     * @return the string
     */
    @ExceptionHandler(Exception.class)
    public String dbError(Exception e){
        logger.error("DataBase error: "+e.getMessage());
        return "new/error_system";
    }

}
