package com.tsystems.mymobile.controller;

import com.tsystems.mymobile.service.api.OptionService;
import com.tsystems.mymobile.service.api.TariffService;
import com.tsystems.mymobile.service.dto.option.OptionSimpleDto;
import com.tsystems.mymobile.service.dto.tariff.TariffAdminDto;
import com.tsystems.mymobile.service.dto.tariff.TariffDto;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import com.tsystems.mymobile.utils.Validators.TariffValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type Tariff controller.
 */
@Controller
@RequestMapping(value = "/main")
public class TariffController {

    private static final Logger logger = Logger.getLogger(TariffController.class);

    @Autowired
    private TariffService tariffService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private TariffValidator validator;

    @Autowired
    private OptionService optionService;

    /**
     * All tariffs string.
     *
     * @param map the map
     * @return the string
     */
    @RequestMapping(value = "/tariffs", method = RequestMethod.POST)
    public String allTariffs(ModelMap map){
        List<TariffDto> tariffDtos = tariffService.getActiveTariffs();
        if (tariffDtos.size() != 0){
            map.put("tariffDtos", tariffDtos);
        } else {
            map.put("tariffs", messageSource.getMessage("Tariffs",null,null));
        }
        return "new/tariff_main";
    }

    /**
     * Gets active tariffs.
     *
     * @param map the map
     * @return the active tariffs
     */
    @RequestMapping(value = "/admin/tariffs", method = RequestMethod.POST)
    public String getActiveTariffs(ModelMap map){
        List<TariffDto> tariffDtos = tariffService.getActiveTariffs();
        if (tariffDtos.size() != 0){
            map.addAttribute("tariffDtos", tariffDtos);
        } else {
            map.addAttribute("tariffs"
                    , messageSource.getMessage("Tariffs",null,null));
        }
        return "new/tariffs";
    }

    /**
     * Gets tariff archive.
     *
     * @param map the map
     * @return the tariff archive
     */
    @RequestMapping(value = "/admin/tariff_archive", method = RequestMethod.POST)
    public String getTariffArchive(ModelMap map){
        List<TariffDto> tariffDtos = tariffService.getTariffArchive();
        if (tariffDtos.size() != 0){
            map.addAttribute("tariffDtos", tariffDtos);
        } else {
            map.addAttribute("tariffsArchive"
                    , messageSource.getMessage("Tariffs.archive",null,null));
        }
        return "new/tariffs";
    }

    /**
     * Gets enable tariffs.
     *
     * @param tariffTittle the tariff tittle
     * @param map          the map
     * @return the enable tariffs
     */
    @RequestMapping(value = "/user/free_tariffs/{tariffTittle}", method = RequestMethod.GET)
    public String getEnableTariffs(@PathVariable String tariffTittle, ModelMap map){
        List<TariffDto> tariffDtos = tariffService.getEnableTariffs(tariffTittle);
        if (tariffDtos.size() != 0){
            map.put("freeTariffs", tariffDtos);
        } else {
            map.put("tariffsErr", messageSource.getMessage("Tariffs.Enable",null,null));
        }
        return "new/user/enable_tariffs";
    }

    /**
     * Gets tariff by id.
     *
     * @param tariffId the tariff id
     * @param map      the map
     * @return the tariff by id
     */
    @RequestMapping(value = "/admin/tariff/{tariffId}", method = RequestMethod.POST)
    public String getTariffById(@PathVariable String tariffId, ModelMap map) {
        TariffAdminDto tariffAdminDto = tariffService.getTariffAdminById(Long.valueOf(tariffId));
        if (tariffAdminDto.getId() == null){
            map.addAttribute("errGetTariff"
                    ,messageSource.getMessage("Tariff.GetError",null,null));
            return "new/admin/admin_tariff";
        }

        map.put("tariffDto", tariffAdminDto);
        return "new/admin/admin_tariff";
    }

    /**
     * Gets tariff by id.
     *
     * @param tariffId the tariff id
     * @param map      the map
     * @return the tariff by id
     */
    @RequestMapping(value = "/admin/tariff/load/{tariffId}", method = RequestMethod.POST)
    public String tariffLoadForCreateContract(@PathVariable String tariffId, ModelMap map){
        TariffAdminDto tariffAdminDto = tariffService.getTariffAdminById(Long.valueOf(tariffId));
        if (tariffAdminDto.getId() == null){
            map.addAttribute("errGetTariff"
                    ,messageSource.getMessage("Tariff.GetError",null,null));
            return "new/admin/tariff_for_create_contract";
        }

        map.put("tariffDto", tariffAdminDto);
        return "new/admin/tariff_for_create_contract";
    }

    /**
     * Create tariff string.
     *
     * @return the string
     */
    @RequestMapping(value = "/admin/tariff/create", method = RequestMethod.GET)
    public String createTariff() {
        return "new/admin/create_tariff";
    }

    /**
     * Create tariff string.
     *
     * @param tariffTittle the tariff tittle
     * @param tariffPrice  the tariff price
     * @param optionTittle the option tittle
     * @param optionPrice  the option price
     * @param optionCost   the option cost
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/admin/tariff/create", method = RequestMethod.POST)
    public String createTariff(@RequestParam String tariffTittle, @RequestParam String tariffPrice,
                               @RequestParam String optionTittle, @RequestParam String optionPrice,
                               @RequestParam String optionCost, ModelMap map) throws DataBaseException {

        if (tariffService.isExist(tariffTittle)){
            return "/mymobile/main/admin/tariff/create";
        }
        TariffDto tariffDto = new TariffDto();
        tariffDto.setPrice(Integer.valueOf(tariffPrice));
        tariffDto.setTittle(tariffTittle);
        if (optionService.isExist(optionTittle)) {
            return "/mymobile/main/admin/tariff/create";
        }
        OptionSimpleDto optionSimpleDto = new OptionSimpleDto(null, optionTittle, Integer.valueOf(optionPrice),
                Integer.valueOf(optionCost));
        Long tariffId = tariffService.createTariff(tariffDto, optionSimpleDto);
        if (tariffId.equals(-1L)){
            throw new DataBaseException("Error");
        }
        return "/mymobile/main/admin/tariff/"+tariffId;
    }

    /**
     * Update tariff string.
     *
     * @param tariffDto the tariff dto
     * @param map       the map
     * @return the string
     */
    @RequestMapping(value = "/admin/tariff/update", method = RequestMethod.POST)
    public String updateTariff(@ModelAttribute("tariffDto") TariffDto tariffDto, ModelMap map)throws DataBaseException {
        if(!validator.validate(tariffDto)){
            map.addAttribute("tariffErr"
                    ,messageSource.getMessage("Tariff.Create.Error",null,null));
            return "new/tariffs";
        };

        Long tariffId = tariffDto.getId();
        String tittle = tariffDto.getTittle();
        Integer price = tariffDto.getPrice();
        TariffDto oldtariff = tariffService.getTariffById(tariffId);
        if (oldtariff.getTittle() == null){
            throw  new DataBaseException("Get tariff error");
        }
        TariffDto tariffDto1 = tariffService.getTariffById(tariffId);
        if (tariffDto1.getId() == null){throw new DataBaseException("Get tariff error");}
        if (tariffDto1.getTittle().equals(tittle) && tariffDto1.getPrice().equals(price)){
            map.addAttribute("updateErr"
                    ,messageSource.getMessage("Update.Tar.Err", null, null));
            map.addAttribute("tariffDto",oldtariff);
            return "new/admin/tariff_update";
        }

        TariffDto newTariffDto = tariffService.updateTariff(tariffDto);
        if (newTariffDto.getId() == null){
            map.addAttribute("updateErr"
                    ,messageSource.getMessage("Tariff.Error", null, null));
            map.addAttribute("tariffDto",oldtariff);
            return "new/admin/tariff_update";
        }
        map.addAttribute("tariffDto",tariffService.getTariffAdminById(newTariffDto.getId()));
        return "new/admin/tariff_update";
    }

    /**
     * Remove option by id string.
     *
     * @param id  the id
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/admin/tariff/remove/{id}", method = RequestMethod.POST)
    public String removeOptionById(@PathVariable Long id) {
        if (!tariffService.removeTariffById(id)) {
            return "/mymobile/main/oops";
        }
        return "/mymobile/main/admin/tariffs";
    }

    @ExceptionHandler(Exception.class)
    public String dbError(Exception ex){
        logger.error("DataBase error: " + ex.getMessage());
        return "new/bad_data";
    }

}
