package com.tsystems.mymobile.dao.api;

import com.tsystems.mymobile.model.Message;

import java.util.List;

/**
 * The interface Message dao.
 */
public interface MessageDAO {

    /**
     * Save message boolean.
     *
     * @param message the message
     * @return the boolean
     */
    Boolean saveMessage(Message message);

    /**
     * Gets messages.
     *
     * @return the messages
     */
    List<Message> getMessages();

    /**
     * Remove message boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeMessage(Long id);

    /**
     * Gets message.
     *
     * @param id the id
     * @return the message
     */
    Message getMessage(Long id);
}
