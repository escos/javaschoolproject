package com.tsystems.mymobile.dao.api;

import com.tsystems.mymobile.model.Option;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;

import java.util.List;

/**
 * The interface Option dao.
 */
public interface OptionDAO {

    /**
     * Create option for tariff option.
     *
     * @param tariffId the tariff id
     * @param option   the option
     * @return the option
     */
    Option createOptionForTariff(Long tariffId, Option option);

    /**
     * Update option option.
     *
     * @param option the option
     * @return the option
     */
    Option updateOption(Option option) throws DataBaseException;

    /**
     * Remove option by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeOptionById(Long id) ;

    /**
     * Remove req option by id boolean.
     *
     * @param reqOptionTittle the req option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean removeReqOptionById(String reqOptionTittle, Long optionId) ;

    /**
     * Remove dis option by id boolean.
     *
     * @param disOptionTittle the dis option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean removeDisOptionById(String disOptionTittle, Long optionId) ;

    /**
     * Add req option by id boolean.
     *
     * @param reqOptionTittle the req option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean addReqOptionById(String reqOptionTittle, Long optionId) ;

    /**
     * Add dis option by id boolean.
     *
     * @param disOptionTittle the dis option tittle
     * @param optionId        the option id
     * @return the boolean
     */
    Boolean addDisOptionById(String disOptionTittle, Long optionId);

    /**
     * Gets option by id.
     *
     * @param id the id
     * @return the option by id
     */
    Option getOptionById(Long id);

    /**
     * Connect options by ids boolean.
     *
     * @param contractId the contract id
     * @param optionIds  the option ids
     * @return the boolean
     */
    Boolean connectOptionsByIds(Long contractId, List<Long> optionIds);

    /**
     * Disconnect option by id boolean.
     *
     * @param contractId the contract id
     * @param id         the id
     * @return the boolean
     */
    Boolean disconnectOptionById(Long contractId, Long id);

    /**
     * Gets option by tittle.
     *
     * @param tittle the tittle
     * @return the option by tittle
     */
    Option getOptionByTittle(String tittle);
}
