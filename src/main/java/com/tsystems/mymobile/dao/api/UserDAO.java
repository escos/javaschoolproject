package com.tsystems.mymobile.dao.api;

import com.tsystems.mymobile.model.User;

import java.util.List;

/**
 * The interface User dao.
 */
public interface UserDAO {

    /**
     * Authorise user.
     *
     * @param email    the email
     * @param password the password
     * @return the user
     */
    User authorise(String email, String password);

    /**
     * Gets all users.
     *
     * @return the all users
     */
    List<User> getAllUsers();

    /**
     * Create user long.
     *
     * @param user the user
     * @return the long
     */
    Long createUser(User user);

    /**
     * Remove user by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeUserById(Long id) ;

    /**
     * Gets user by id.
     *
     * @param id the id
     * @return the user by id
     */
    User getUserById(Long id);

    /**
     * Gets user by id.
     *
     * @param email the email
     * @return the user by id
     */
    User getUserByEmail(String email);

    /**
     * Is exist user boolean.
     *
     * @param email    the email
     * @param passport the passport
     * @return the boolean
     */
    Boolean isExistUser(String email,String passport);

}
