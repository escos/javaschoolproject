package com.tsystems.mymobile.dao.api;

import com.tsystems.mymobile.model.Option;
import com.tsystems.mymobile.model.Tariff;

import java.util.List;

/**
 * The interface Tariff dao.
 */
public interface TariffDAO {

    /**
     * Gets tariff by contract id.
     *
     * @param contractId the contract id
     * @return the tariff by contract id
     */
    Tariff getTariffByContractId(Long contractId);

    /**
     * Gets active tariffs.
     *
     * @return the active tariffs
     */
    List<Tariff> getActiveTariffs();

    /**
     * Gets tariff archive.
     *
     * @return the tariff archive
     */
    List<Tariff> getTariffArchive();

    /**
     * Gets tariff by id.
     *
     * @param id the id
     * @return the tariff by id
     */
    Tariff getTariffById(Long id);

    /**
     * Gets tariff by tittle.
     *
     * @param tittle the tittle
     * @return the tariff by tittle
     */
    Tariff getTariffByTittle(String tittle);

    /**
     * Create tariff long.
     *
     * @param tariff the tariff
     * @param option the option
     * @return the long
     */
    Long createTariff(Tariff tariff, Option option);

    /**
     * Remove tariff by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeTariffById(Long id);

    /**
     * Update tariff tariff.
     *
     * @param tariff the tariff
     * @return the tariff
     */
    Tariff updateTariff(Tariff tariff);

    /**
     * Is exist boolean.
     *
     * @param tittle the tittle
     * @return the boolean
     */
    Boolean isExist(String tittle) ;
}
