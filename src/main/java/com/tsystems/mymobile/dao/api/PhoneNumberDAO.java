package com.tsystems.mymobile.dao.api;

import com.tsystems.mymobile.model.PhoneNumber;

import java.util.List;

/**
 * The interface Phone number dao.
 */
public interface PhoneNumberDAO {

    /**
     * Gets free numbers.
     *
     * @return the free numbers
     */
    List<PhoneNumber> getFreeNumbers();

    /**
     * Gets number by id.
     *
     * @param id the id
     * @return the number by id
     */
    PhoneNumber getNumberById(Long id);

    /**
     * Gets used numbers.
     *
     * @return the used numbers
     */
    List<PhoneNumber> getUsedNumbers();
}
