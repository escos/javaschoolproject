package com.tsystems.mymobile.dao.api;

import com.tsystems.mymobile.model.Contract;

import java.util.List;

/**
 * The interface Contract dao.
 */
public interface ContractDAO {

    /**
     * Create contract long.
     *
     * @param contract the contract
     * @return the long
     */
    Long createContract(Contract contract);

    /**
     * Gets all contracts.
     *
     * @return the all contracts
     */
    List<Contract> getAllContracts();

    /**
     * Gets contract by user id.
     *
     * @param userId the user id
     * @return the contract by user id
     */
    List<Contract> getContractByUserId(Long userId);

    /**
     * Gets contract by number.
     *
     * @param numberId the number id
     * @return the contract by number
     */
    Contract getContractByNumber(Long numberId);

    /**
     * Gets contract by id.
     *
     * @param id the id
     * @return the contract by id
     */
    Contract getContractById(Long id);

    /**
     * Contract block by id integer.
     *
     * @param id the id
     * @return the integer
     */
    Integer contractBlockById(Long id);

    /**
     * Contract admin block by id integer.
     *
     * @param id the id
     * @return the integer
     */
    Integer contractAdminBlockById(Long id);

    /**
     * Remove contract by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    Boolean removeContractById(Long id);

    /**
     * Gets contracts by user email.
     *
     * @param userEmail the user email
     * @return the contracts by user email
     */
    List<Contract> getContractsByUserEmail(String userEmail);

    /**
     * Update contract by tariff boolean.
     *
     * @param contractId the contract id
     * @param tariffId   the tariff id
     * @return the boolean
     */
    Boolean updateContractByTariff(Long contractId, Long tariffId);

}
