package com.tsystems.mymobile.dao.impl;

import com.tsystems.mymobile.dao.api.OptionDAO;
import com.tsystems.mymobile.model.Contract;
import com.tsystems.mymobile.model.Option;
import com.tsystems.mymobile.model.Tariff;
import com.tsystems.mymobile.utils.Exceptions.DataBaseException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The type Option dao.
 */
@Repository
public class OptionDaoImpl implements OptionDAO {

    private static Logger logger = Logger.getLogger(OptionDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Option createOptionForTariff(Long id, Option option){
        try {
            Tariff tariff = entityManager.find(Tariff.class, id);
            option.setTariff(tariff);
            option.setIsDeleted(true);
            tariff.getOptions().add(option);
            entityManager.persist(option);
            return option;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new Option();
        }
    }

    @Override
    public Option updateOption(Option option) throws DataBaseException {
        try {
            Option optEntity = entityManager.find(Option.class, option.getId());
            optEntity.setPrice(option.getPrice());
            optEntity.setTittle(option.getTittle());
            optEntity.setCost(option.getCost());
            entityManager.merge(optEntity);
            return optEntity;
        } catch (NoResultException e) {
            logger.error(e.getMessage());
            return new Option();
        } catch (Exception e){
            throw new DataBaseException(e.getMessage());
        }
    }

    @Override
    public Boolean removeOptionById(Long id) {
        try {
            Option option = entityManager.find(Option.class, id);
            option.setDisOptions(new HashSet<>());
            option.setDisOptions1(new HashSet<>());
            option.setReqOptions(new HashSet<>());
            option.setReqOptions1(new HashSet<>());
            option.setIsDeleted(false);
            if (option.getTariff().getOptions()
                    .stream().filter(Option::getIsDeleted)
                    .collect(Collectors.toSet()).isEmpty()) {
                option.getTariff().setIsDeleted(false);
            }
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean removeReqOptionById(String reqOptionTittle, Long optionId){
        try {
            Option option = entityManager.find(Option.class, optionId);
            return option.getReqOptions().removeIf(option1 -> option1.getTittle().equals(reqOptionTittle));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean removeDisOptionById(String disOptionTittle, Long optionId) {
        try {
            Option option = entityManager.find(Option.class, optionId);
            option.getDisOptions().removeIf(option1 -> option1.getTittle().equals(disOptionTittle));
            Option option1 = entityManager
                    .createQuery("select option from Option option where option.tittle = :tittle ", Option.class)
                    .setParameter("tittle", disOptionTittle).getSingleResult();
            option1.getDisOptions().removeIf(option2 -> option2.getId().equals(optionId));

            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean addReqOptionById(String reqOptionTittle, Long optionId) {
        try {
            Option option = entityManager.find(Option.class, optionId);
            Option reqOption = entityManager
                    .createQuery("select option from Option option where option.tittle = :tittle ", Option.class)
                    .setParameter("tittle", reqOptionTittle).getSingleResult();
            option.getReqOptions().add(reqOption);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean addDisOptionById(String disOptionTittle, Long optionId) {
        try {
            Option option = entityManager.find(Option.class, optionId);
            Option disOption = entityManager
                    .createQuery("select option from Option option where option.tittle = :tittle ", Option.class)
                    .setParameter("tittle", disOptionTittle).getSingleResult();
            option.getDisOptions().add(disOption);
            disOption.getDisOptions().add(option);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Option getOptionById(Long id) {
        try {
            return entityManager.find(Option.class, id);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new Option();
        }
    }

    @Override
    public Boolean connectOptionsByIds(Long contractId, List<Long> ids) {
        try {
            Contract contract = entityManager.find(Contract.class, contractId);
            for (Long id : ids) {
                Option option = entityManager.find(Option.class, id);
                contract.getConnectedOptions().add(option);
            }
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean disconnectOptionById(Long contractId, Long id) {
        try {
            Contract contract = entityManager.find(Contract.class, contractId);
            Set<Option> connectedOptions = contract.getConnectedOptions();
            connectedOptions.removeIf(opt -> opt.getId().equals(id));
            contract.setConnectedOptions(connectedOptions);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Option getOptionByTittle(String tittle){
        try {
            return entityManager
                    .createQuery("select option from Option option where option.tittle = :tittle ", Option.class)
                    .setParameter("tittle", tittle).getSingleResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new Option();
        }
    }
}

