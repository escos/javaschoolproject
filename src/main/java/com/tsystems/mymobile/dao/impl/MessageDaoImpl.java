package com.tsystems.mymobile.dao.impl;

import com.tsystems.mymobile.dao.api.MessageDAO;
import com.tsystems.mymobile.model.Message;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;


@Repository
public class MessageDaoImpl implements MessageDAO {

    private static Logger logger = Logger.getLogger(MessageDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Boolean saveMessage(Message message){
        try {
            message.setIsDeleted(true);
            entityManager.persist(message);
            return true;
        } catch (Exception e) {
            logger.error("Save message error" + e.getMessage());
            return false;
        }
    }

    @Override
    public List<Message> getMessages() {
        try {
            return entityManager
                    .createQuery("select message from Message message where message.isDeleted = true ", Message.class)
                    .getResultList();
        } catch (NoResultException e) {
            logger.error("No messages" + e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Boolean removeMessage(Long id) {
        try {
            Message message = entityManager.find(Message.class, id);
            message.setIsDeleted(false);
            entityManager.merge(message);
            return true;
        } catch (Exception e) {
            logger.error("Removing error: " + e.getMessage());
            return false;
        }
    }

    @Override
    public Message getMessage(Long id) {
        try {
            return entityManager.find(Message.class, id);
        } catch (Exception e) {
            logger.error("Getting message error: " + e.getMessage());
            return new Message();
        }
    }
}
