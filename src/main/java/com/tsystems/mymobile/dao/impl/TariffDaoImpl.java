package com.tsystems.mymobile.dao.impl;

import com.tsystems.mymobile.dao.api.TariffDAO;
import com.tsystems.mymobile.model.Contract;
import com.tsystems.mymobile.model.Option;
import com.tsystems.mymobile.model.Tariff;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TariffDaoImpl implements TariffDAO {

    private static Logger logger = Logger.getLogger(TariffDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Tariff getTariffByContractId(Long contractId) {
        try {
            Contract contract = entityManager.find(Contract.class, contractId);
            return contract.getTariff();
        } catch (Exception e) {
            logger.error("No tariff error: "+ e.getMessage());
            return new Tariff();
        }
    }

    @Override
    public List<Tariff> getActiveTariffs() {
        try {
            return entityManager
                    .createQuery("select tariff from Tariff tariff where tariff.isDeleted = true ", Tariff.class)
                    .getResultList();
        } catch (Exception e) {
            logger.error("Get tariffs error: "+ e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Tariff> getTariffArchive() {
        try {
            return entityManager
                    .createQuery("select tariff from Tariff tariff where tariff.isDeleted = false ", Tariff.class)
                    .getResultList();
        } catch (Exception e) {
            logger.error("Get tariffs error: "+ e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Tariff getTariffById(Long id) {
        try {
            return entityManager.find(Tariff.class, id);
        } catch (Exception e) {
            logger.error("No tariff error: "+ e.getMessage());
            return new Tariff();
        }
    }

    @Override
    public Tariff getTariffByTittle(String tittle){
        try {
            return entityManager
                    .createQuery("select tariff from Tariff tariff where tariff.tittle = :tittle ", Tariff.class)
                    .setParameter("tittle", tittle)
                    .getSingleResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new Tariff();
        }
    }

    @Override
    public Long createTariff(Tariff tariff, Option option) {
        try {
            String tittle = tariff.getTittle();
            tariff.setIsDeleted(true);
            entityManager.persist(tariff);
            Tariff newTariff = entityManager
                    .createQuery("select tariff from Tariff tariff where tariff.tittle = :tittle ", Tariff.class)
                    .setParameter("tittle",tittle)
                    .getSingleResult();
            option.setTariff(newTariff);
            option.setIsDeleted(true);
            entityManager.merge(option);
            return newTariff.getId();
        } catch (Exception e){
            logger.error(e.getMessage());
            return -1L;
        }
    }

    @Override
    public Boolean removeTariffById(Long id) {
        try {
            entityManager.createQuery("update Tariff tariff set tariff.isDeleted = false" + " where tariff.id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Tariff updateTariff(Tariff tariff) {
        try {
            Tariff tarEntity = entityManager.find(Tariff.class, tariff.getId());
            tarEntity.setPrice(tariff.getPrice());
            tarEntity.setTittle(tariff.getTittle());
            entityManager.merge(tarEntity);
            return tarEntity;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new Tariff();
        }
    }

    @Override
    public Boolean isExist(String tittle) {
        try {
            Tariff tariff = entityManager
                    .createQuery("select tariff from Tariff tariff where tariff.tittle = :tittle ", Tariff.class)
                    .setParameter("tittle",tittle)
                    .getSingleResult();
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }
}
