package com.tsystems.mymobile.dao.impl;

import com.tsystems.mymobile.dao.api.UserDAO;
import com.tsystems.mymobile.model.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * The type User dao.
 */
@Repository
public class UserDaoImpl implements UserDAO {

    private static Logger logger = Logger.getLogger(UserDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User authorise(String email, String password) {
        try {
            return entityManager
                    .createQuery("select user from User user where user.email = :userEmail and " +
                            "user.password = :user_password and user.isDeleted = true ", User.class)
                    .setParameter("userEmail", email).setParameter("user_password", password)
                    .getSingleResult();

        } catch (Exception e) {
            logger.error("Authorize exception: " + e.getMessage());
            return new User();
        }
    }

    @Override
    public User getUserById(Long id) {
        try {
            return entityManager.find(User.class, id);
        } catch (Exception e) {
            logger.error("Did not find user: " + e.getMessage());
            return new User();
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try {
            return entityManager
                    .createQuery("select user from User user " +
                            "where (user.email = :email ) " +
                            "and user.isDeleted = true ", User.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (Exception e) {
            return new User();
        }
    }

    @Override
    public Boolean isExistUser(String email, String passport) {
        try {
            entityManager
                    .createQuery("select user from User user " +
                            "where (user.email = :email or user.passport = :passport) " +
                            "and user.isDeleted = true ", User.class)
                    .setParameter("email", email).setParameter("passport", passport)
                    .getSingleResult();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<User> getAllUsers() {
        try {
            return entityManager
                    .createQuery("select user from User user where user.role = 'USER' and user.isDeleted != false", User.class)
                    .getResultList();
        } catch (Exception e) {
            logger.error("Get all users error: " + e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Long createUser(User user) {
        try {
            user.setIsDeleted(true);
            entityManager.persist(user);
            String email = user.getEmail();
            User newUser = entityManager
                    .createQuery("select user from User user where user.email = :email ", User.class)
                    .setParameter("email", email).getSingleResult();
            return newUser.getId();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return -1L;
        }
    }

    @Override
    public Boolean removeUserById(Long id) {
        try {
            entityManager
                    .createQuery("update User user set user.isDeleted = false" +
                            " where user.id=:id")
                    .setParameter("id", id)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error("Remove user error: " + e.getMessage());
            return false;
        }
    }

}

