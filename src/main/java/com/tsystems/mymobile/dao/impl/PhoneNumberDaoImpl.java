package com.tsystems.mymobile.dao.impl;

import com.tsystems.mymobile.dao.api.PhoneNumberDAO;
import com.tsystems.mymobile.model.Contract;
import com.tsystems.mymobile.model.Message;
import com.tsystems.mymobile.model.PhoneNumber;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Phone number dao.
 */
@Repository
public class PhoneNumberDaoImpl implements PhoneNumberDAO {

    private static final Logger logger = Logger.getLogger(PhoneNumberDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PhoneNumber> getFreeNumbers() {
        try {

            List<Contract> contracts = entityManager
                    .createQuery("select contract from Contract contract", Contract.class)
                    .getResultList();
            List<Message> messages = entityManager
                    .createQuery("select message from Message message", Message.class)
                    .getResultList();
            List<Long> messageNumbers = messages.stream()
                    .map(Message::getNumber)
                    .collect(Collectors.toList());
            List<PhoneNumber> usedNumbers = contracts.stream()
                    .map(Contract::getPhoneNumber).collect(Collectors.toList());
            List<PhoneNumber> numbers = entityManager
                    .createQuery("select phoneNumber from PhoneNumber phoneNumber "
                            , PhoneNumber.class).getResultList();
            return numbers.stream()
                    .filter(number -> !messageNumbers.contains(number.getId()))
                    .filter(number -> !usedNumbers.contains(number)).limit(20)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public PhoneNumber getNumberById(Long id){
        try {
            return entityManager.find(PhoneNumber.class, id);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new PhoneNumber();
        }
    }

    @Override
    public List<PhoneNumber> getUsedNumbers() {
        try {
            List<Contract> contracts = entityManager
                    .createQuery("select contract from Contract contract", Contract.class)
                    .getResultList();
            return contracts.stream().map(Contract::getPhoneNumber).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }
}
