package com.tsystems.mymobile.dao.impl;

import com.tsystems.mymobile.dao.api.ContractDAO;
import com.tsystems.mymobile.model.*;
import com.tsystems.mymobile.utils.Enums.BlockStatus;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The type Contract dao.
 */
@Repository
public class ContractDaoImpl implements ContractDAO {

    private static final Logger logger = Logger.getLogger(ContractDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long createContract(Contract contract) {
        try {
            entityManager.persist(contract);
            return contract.getId();
        } catch (Exception e) {
            logger.error("Create contract error: " + e.getMessage());
            return -1L;
        }
    }

    @Override
    public List<Contract> getAllContracts() {
        try {
            return entityManager
                    .createQuery("select contract from Contract contract " +
                            "where contract.user.role = 'USER'", Contract.class)
                    .getResultList();
        } catch (Exception e) {
            logger.error("Get all contracts error: " + e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contract> getContractByUserId(Long userId) {
        try {
            User user = entityManager.find(User.class, userId);

            List<Contract> contracts = entityManager
                    .createQuery("select contract from Contract contract", Contract.class)
                    .getResultList();
            List<Message> messages = entityManager
                    .createQuery("select message from Message message", Message.class)
                    .getResultList();
            List<Long> messageNumbers = messages.stream()
                    .map(Message::getNumber)
                    .collect(Collectors.toList());
            List<PhoneNumber> usedNumbers = contracts.stream()
                    .map(Contract::getPhoneNumber).collect(Collectors.toList());
            List<PhoneNumber> numbers = entityManager
                    .createQuery("select phoneNumber from PhoneNumber phoneNumber "
                            , PhoneNumber.class).getResultList();
            List<PhoneNumber> result = numbers.stream()
                    .filter(number -> !messageNumbers.contains(number.getId()))
                    .filter(number -> !usedNumbers.contains(number))
                    .collect(Collectors.toList());
            if (result.size() <= 5) {
                for (int i = 0; i < 5; i++) {
                    PhoneNumber number = new PhoneNumber();
                    number.setIsDeleted(true);
                    entityManager.persist(number);
                }
            }
            return entityManager.createQuery("select contract from Contract contract " +
                    "where contract.user = :user", Contract.class)
                    .setParameter("user", user)
                    .getResultList();
        } catch (Exception e) {
            logger.error("Get contracts error: " + e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Contract getContractByNumber(Long numberId) {
        try {
            PhoneNumber number = entityManager.find(PhoneNumber.class, numberId);
            return entityManager
                    .createQuery("select contract from Contract contract " +
                            "where contract.phoneNumber = :number", Contract.class)
                    .setParameter("number", number)
                    .getSingleResult();
        } catch (Exception e) {
            logger.error("Get contract error: " + e.getMessage());
            return new Contract();
        }
    }

    @Override
    public Contract getContractById(Long id) {
        try {
            return entityManager.find(Contract.class, id);
        } catch (Exception e) {
            logger.error("Get contract error: " + e.getMessage());
            return new Contract();
        }
    }

    @Override
    public Integer contractBlockById(Long id) {
        int status;
        try {
            Contract contract = entityManager.find(Contract.class, id);
            if (contract.getBlockStatus().equals(BlockStatus.UNBLOCKING)) {
                status = entityManager
                        .createQuery("update Contract contract set contract.blockStatus = 'USER_BLOCKING'" +
                                " where contract.id=:id")
                        .setParameter("id", id)
                        .executeUpdate();
            } else {
                status = entityManager
                        .createQuery("update Contract contract set contract.blockStatus = 'UNBLOCKING'" +
                                " where contract.id=:id")
                        .setParameter("id", id)
                        .executeUpdate();
            }
            return status;
        } catch (Exception e) {
            logger.error("Block error: " + e.getMessage());
            return 0;
        }
    }

    @Override
    public Integer contractAdminBlockById(Long id) {
        int status = 0;
        try {
            Contract contract = entityManager.find(Contract.class, id);
            if (contract.getBlockStatus().equals(BlockStatus.UNBLOCKING) ||
                    contract.getBlockStatus().equals(BlockStatus.USER_BLOCKING)) {
                status = entityManager
                        .createQuery("update Contract contract set contract.blockStatus = 'ADMIN_BLOCKING'" +
                                " where contract.id=:id")
                        .setParameter("id", id)
                        .executeUpdate();
            } else {
                status = entityManager
                        .createQuery("update Contract contract set contract.blockStatus = 'UNBLOCKING'" +
                                " where contract.id=:id")
                        .setParameter("id", id)
                        .executeUpdate();
            }
            return status;
        } catch (Exception e) {
            logger.error("Admin block error: " + e.getMessage());
            return 0;
        }
    }

    @Override
    public Boolean removeContractById(Long id) {
        try {
            Contract contract = entityManager.find(Contract.class, id);
            entityManager.remove(contract);
            return true;
        } catch (Exception e) {
            logger.error("Removing error: " + e.getMessage());
            return false;
        }
    }

    @Override
    public List<Contract> getContractsByUserEmail(String userEmail) {
        try {
            User user = entityManager.createQuery("select user from User user where user.email = :userEmail"
                    , User.class)
                    .setParameter("userEmail", userEmail)
                    .getSingleResult();
            return entityManager.createQuery("select contract from Contract contract " +
                    "where contract.user = :user", Contract.class)
                    .setParameter("user", user)
                    .getResultList();
        } catch (Exception e) {
            logger.error("Get contracts error: " + e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Boolean updateContractByTariff(Long contractId, Long tariffId) {
        try {
            Tariff tariff = entityManager.find(Tariff.class, tariffId);
            Set<Option> clearSet = new HashSet<>();
            Contract contract = entityManager.createQuery("select contract from Contract contract " +
                    "where contract.id = :contractId", Contract.class)
                    .setParameter("contractId", contractId).getSingleResult();
            contract.setConnectedOptions(clearSet);
            contract.setTariff(tariff);
            return true;
        } catch (Exception e) {
            logger.error("Update error: " + e.getMessage());
            return false;
        }
    }

}
