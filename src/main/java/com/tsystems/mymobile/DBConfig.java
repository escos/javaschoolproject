package com.tsystems.mymobile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * The type Db config.
 */
@Configuration
@EnableTransactionManagement
public class DBConfig {

    private static final String PERSISTENCE_UNIT_NAME = "mymobile";

    /**
     * Gets entity manager factory.
     *
     * @return the entity manager factory
     */
    @Bean
    public EntityManagerFactory getEntityManagerFactory() {
        return Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    /**
     * Platform transaction manager platform transaction manager.
     *
     * @return the platform transaction manager
     */
    @Bean
    public PlatformTransactionManager platformTransactionManager(){
        return new JtaTransactionManager();
    }


}
