package com.tsystems.mymobile.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "message")
public class Message{

    @Id
    @GeneratedValue
    @Column(name = "message_id", unique = true, nullable = false)
    @Getter @Setter private Long id;

    @Column(name = "user_email", nullable = false)
    @Getter @Setter private String email;

    @Column(name = "tariff_tittle", nullable = false)
    @Getter @Setter private String tittle;

    @Column(name = "number", nullable = false)
    @Getter @Setter private Long number;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false, columnDefinition = "DATETIME")
    @Getter @Setter private Date date ;

    @Column(name = "isDeleted", nullable = false, columnDefinition="tinyint(1) default 0")
    @Getter @Setter private Boolean isDeleted;



}
