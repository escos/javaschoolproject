package com.tsystems.mymobile.model;

import com.tsystems.mymobile.utils.Enums.BlockStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "contract")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contract_id", unique = true, nullable = false)
    @Getter @Setter private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "number_id")
    @Getter @Setter private PhoneNumber phoneNumber;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "tariff_id")
    @Getter @Setter private Tariff tariff;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @Getter @Setter private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "contract_block", nullable = false)
    @Getter @Setter private BlockStatus blockStatus;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "contract_options", joinColumns = {@JoinColumn(name = "contract_id")},
    inverseJoinColumns = {@JoinColumn(name = "option_id")})
    @Getter @Setter private Set<Option> connectedOptions = new HashSet<>();
}