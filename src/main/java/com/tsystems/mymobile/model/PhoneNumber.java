package com.tsystems.mymobile.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "number")
public class PhoneNumber {

    @Id
    @GeneratedValue
    @Column(name = "number_id", unique = true)
    @Getter @Setter private Long id;
    @Column(name = "number_deleted", unique = true, columnDefinition="tinyint(1) default 0")
    @Getter @Setter private Boolean isDeleted;

}
