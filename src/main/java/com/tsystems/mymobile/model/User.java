package com.tsystems.mymobile.model;

import com.tsystems.mymobile.utils.Enums.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="user_id", unique = true, nullable = false)
    @Getter @Setter private Long id;

    @Column(name="user_name", nullable = false, length = 20)
    @Getter @Setter private String name;

    @Column(name="user_surname", nullable = false, length = 20)
    @Getter @Setter private String surname;

    @Temporal(TemporalType.DATE)
    @Column(name="user_birthdate", nullable = false)
    @Getter @Setter private Date birthDate;

    @Column(name="user_passport", unique = true, nullable = false, length = 11)
    @Getter @Setter private String passport;

    @Column(name="user_address", nullable = false, length = 50)
    @Getter @Setter private String address;

    @Column(name="user_email", unique = true, nullable = false, length = 30)
    @Getter @Setter private String email;

    @Column(name="user_password", nullable = false)
    @Getter @Setter private String password;

    @Column(name="user_deleted", nullable = false, columnDefinition="tinyint(1) default 0")
    @Getter @Setter private Boolean isDeleted;

    @Enumerated(EnumType.STRING)
    @Column(name="user_role", nullable = false)
    @Getter @Setter private Role role;
}

