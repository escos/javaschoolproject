package com.tsystems.mymobile.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "options")
public class Option {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "option_id", unique = true, nullable = false)
    @Getter @Setter private Long id;


    @Column(name = "option_tittle", unique = true, length = 30)
    @Getter @Setter private String tittle;

    @Column(name = "option_price", nullable = false)
    @Getter @Setter private Integer price;

    @Column(name = "option_cost", nullable = false)
    @Getter @Setter private Integer cost;

    @Column(name = "option_deleted", nullable = false, columnDefinition="tinyint(1) default 0")
    @Getter @Setter private Boolean isDeleted;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "tariff_id")
    @Getter @Setter private Tariff tariff;

    @ManyToMany(fetch=FetchType.EAGER, mappedBy="connectedOptions")
    @Getter @Setter public Set<Contract> contracts;

    @ManyToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name="disabled_options",
            joinColumns={@JoinColumn(name="option_id")},
            inverseJoinColumns={@JoinColumn(name="option_id1")})
    @Getter @Setter private Set<Option> disOptions = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "disOptions")
    @Getter @Setter private Set<Option> disOptions1 = new HashSet<>();

    @ManyToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name="required_options",
            joinColumns={@JoinColumn(name="option_id")},
            inverseJoinColumns={@JoinColumn(name="option_id1")})
    @Getter @Setter private Set<Option> reqOptions = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "reqOptions")
    @Getter @Setter private Set<Option> reqOptions1 = new HashSet<>();

}
