package com.tsystems.mymobile.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tariff")
public class Tariff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tariff_id",unique = true, nullable = false)
    @Getter @Setter private Long id;

    @Column(name = "tariff_tittle", unique = true, nullable = false, length = 30)
    @Getter @Setter private String tittle;

    @Column(name = "tariff_price", nullable = false)
    @Getter @Setter private Integer price;

    @Column(name = "tariff_deleted", nullable = false, columnDefinition="tinyint(1) default 0")
    @Getter @Setter private Boolean isDeleted;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "tariff")
    @Getter @Setter private Set<Option> options = new HashSet<>();

}