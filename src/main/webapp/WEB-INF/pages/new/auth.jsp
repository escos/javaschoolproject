<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/font-awesome-4.2.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/fonts.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/main.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/login_form.css" />
    <title>authorization</title>
</head>
<body>
<c:set var="showTariffs" value="0"/>

<%@include file="header_img.jsp"%>
<section class="main_content">
    <div class="container">
        <div class="col-md-12">
            <div class="container main_container" >
                <div class="row">
                    <div class="form_container">
                        <section id="cont">
                          <form:form method="POST" modelAttribute="userLoginDto" action="/mymobile/main/auth" class="form-horizontal">
                                <h1>Login Form</h1>
                                <c:if test="${userLoginDto.id == -1}">
                                    <div class="alert alert-danger">
                                        <spring:message code="User.authorize"/>
                                    </div>
                                </c:if>
                                <div>
                                    <input type="email" class="form-control" placeholder="Enter email"
                                                required="" name="email"/>
                                    <div class="has-error">
                                        <form:errors path="email" class="control-label"/>
                                    </div>
                                </div>
                                <div>
                                    <input type="password" class="form-control"
                                                required="" placeholder="Enter password" name="password"/>
                                    <div class="has-error">
                                        <form:errors path="password" class="control-label"/>
                                    </div>
                                </div>
                                <div>
                                    <input class="but_login" type="submit" value="Log in" />
                                    <%--<a href="#">Lost your password?</a>--%>
                                </div>
                          </form:form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="footer.jsp"%>
</body>
<script src="${pageContext.request.contextPath}/resources/new/js/header.js"></script>
</html>
