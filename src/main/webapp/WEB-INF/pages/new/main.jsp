<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>main</title>
    <meta name="description" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap-grid-3.3.1.min.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/new/libs/font-awesome-4.2.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/fancybox/jquery.fancybox.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/owl-carousel/owl.carousel.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/countdown/jquery.countdown.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/fonts.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/main.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/media.css"/>
</head>
<body>
<c:set var="showTariffs" value="1"/>
<%@include file="header_img.jsp" %>

<div class="hidden">
    <form:form id="callback" method="POST" class="myform" modelAttribute="message">
        <h3>Application for connection</h3>
        <input id="email" type="email" name="email" placeholder="Email..." required=""/>
        <input id="add_number" type="text" name="number" placeholder="Phone number..." required=""/>
        <select class="app_select" id="tar_main" name="tittle">
            <c:forEach items="${tariffDtos}" var="tariffDto">
                <option id="tar_choose" value="${tariffDto.tittle}" name="tittle">${tariffDto.tittle}</option>
            </c:forEach>
        </select>
        <input class="button " type="submit" value="SEND"/>
    </form:form>
</div>

<section class="main_content">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="container main_container" id="content">
                    <c:if test="${ProblemsTariff != null }">
                        <div class="alert alert-danger">
                            <strong>${ProblemsTariff}</strong>
                        </div>
                    </c:if>
                    <%@include file="tariff_main.jsp" %>
                </div>
            </div>
            <div class="col-md-3">
                <aside class="right_aside">
                    <c:if test="${mesSendErr!= null }">
                        <div class="alert alert-danger">
                            <strong>${mesSendErr}</strong>
                        </div>
                    </c:if>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sell_descr">
                                <h3>Enable phone numbers</h3>
                            </div>
                        </div>
                    </div>
                    <div id="number_change" class="row">
                        <div class="col-md-12">
                            <div class="sell_number">
                                <a id="number"><i class="fa fa-refresh"></i></a>
                                <span id="target">${rndmNumber}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sell_right">
                                <a id="view_number" href="#callback" class="button fancybox">
                                    SEND
                                </a>
                            </div>
                        </div>
                    </div>
                </aside>
                <aside class="right_aside">
                    <div class="blog_item">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><a id="new_1" href="#">The Phone Co-op launches a new Foundation</a></h3>
                            </div>

                        </div>
                        <div class="row blog">
                          <img src="${pageContext.request.contextPath}/resources/new/img/man_2.jpg" alt="alt">
                        </div>
                    </div>
                </aside>
                <aside class="right_aside">
                    <div class="blog_item">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><a id="new_2" href="#">New Beginnings in historic old building for phone company</a></h3>
                            </div>
                        </div>
                        <div class="row blog">
                           <img src="${pageContext.request.contextPath}/resources/new/img/men_3.jpg" alt="alt">
                        </div>
                    </div>
                </aside>
            </div>
        </div>
        <div class="row">
            <button type="button"  class="up_but" onclick="up()"><i class="fa fa-chevron-up " ></i></button>
        </div>
    </div>
</section>

<%@include file="footer.jsp" %>


<script>

    <%--$("#new_1").click(function () {--%>
        <%--$("#content").html("new/news1")--%>
    <%--});--%>

    <%--$("#new_2").click(function () {--%>
        <%--$("#content").html("<jsp:include page="news2.jsp" />")--%>
    <%--});--%>

    $("#callback").submit(function (e) {
        e.preventDefault();
        var number = $("#add_number").val();
        var data = $("#callback").serialize();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/message/save/" + number,
            data: data
        }).done(function () {
            setTimeout(function () {
                $.fancybox.close();
            }, 1000);

        });
        $("#email").trigger("reset");

        return false;
    });

    function up() {
        $("body, html").animate({
            scrollTop: 0
        }, 800);
        return false;
    }

</script>

</body>
</html>
