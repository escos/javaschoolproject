<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/header.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/fonts.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap-grid-3.3.1.min.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/new/libs/font-awesome-4.2.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/main.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/media.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/users.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-ui.css"/>
</head>
<nav>
    <div class="logo"></div>

    <c:choose>
        <c:when test="${userLoginDto == null}">
            <ul>
                <li><a href="${pageContext.request.contextPath}/main"><i class="fa fa-home"></i></a></li>
                <li><a href="${pageContext.request.contextPath}/main/auth"><i class="fa fa-user"></i></a></li>
            </ul>
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${userLoginDto.role == 'ADMINISTRATOR' }">
                    <a href="${pageContext.request.contextPath}/main/admin">
                        ADMIN: ${userLoginDto.name}</a>
                </c:when>
                <c:when test="${userLoginDto.role == 'USER' }">
                    <a href="${pageContext.request.contextPath}/main/user">
                        USER: ${userLoginDto.name}</a>
                </c:when>
            </c:choose>
            <ul>
                <li><a href="#" id="basket"><i class="fa fa-shopping-cart"></i></a></li>
                <li><i id="basket_badge" class="badge">${basketList.size()}</i></li>
                <li><a href="${pageContext.request.contextPath}/main/stop"><i class="fa fa-sign-out"></i></a></li>
            </ul>
        </c:otherwise>
    </c:choose>

</nav>
<header>
    <div class="headline">
        <div class="inner">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <nav class="maian_mnu clearfix">
                            <c:if test="${userLoginDto.role == 'ADMINISTRATOR'}">
                                <button class="main_mnu_button hidden-md hidden-lg hidden-sm"><i class="fa fa-bars"></i>
                                </button>
                                <ul style="margin-bottom: -5px;">
                                    <li><a id="users" href="#">Users</a></li>
                                    <li><a id="contracts" href="#">Contracts</a></li>
                                    <li class="dropdown">
                                        <a class="droplink">Tariff menu</a>
                                        <div class="dropdown-content">
                                            <a id="admin_tariffs" href="#">Active tariffs</a>
                                            <a id="archive" href="#">Tariff archive</a>
                                            <a id="create_tariff" href="#">Create tariff</a>
                                        </div>
                                    </li>
                                    <li><a id="create" href="#">Create</a></li>
                                    <li><a id="inc_messages" href="#">Messages</a></li>
                                    <li>
                                        <form id="search" class="search-container" method="post"
                                              action="/mymobile/main/admin/search">
                                            <input id="num" name="numberId" placeholder=" enter number..." type="text"
                                                   maxlength="10" pattern="\d{10}" required=""/>
                                            <input class="btn btn-primary " type="submit" value="search"/>
                                        </form>
                                    </li>
                                </ul>
                            </c:if>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-3.2.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-3.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/js/header.js"></script>

<script>

    $("#search").submit(function (e) {
        e.preventDefault();
        var number = $("#search");
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/search",
            data: number.serialize(),
            success: function (response) {
                $("#admin_body").html(response);
                $("#search").trigger("reset");
            }
        });
    });

    $.ajax({
        type: "POST",
        url: "/mymobile/main/numbers",
        success: function (response) {
            var temp = JSON.parse(response);
            $("#num").autocomplete({
                source: temp,
                minLength: 2,
                onSelect: function () {
                    location.reload();
                }
            });
        }
    });

</script>