<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>Basket</title>
</head>

<c:set var="count" scope="page" value="0"/>
<c:set var="basketOptions" scope="session" value="${basketOptions}"/>

<div class="generic-container users-right" style="margin: 20px 0 0 170px">
    <div class="panel panel-default" style="padding: 5px; width: 80%">
        <div class="panel-heading" style="border-radius: 3px;"><span class="lead tittle_table">BASKET
            <c:if test="${status == 1}">
                <div class="alert alert-success">
                    <strong>Success!</strong> ${option.tittle} added to basket.
                </div>
            </c:if>
            <c:if test="${status == 0}">
                <div class="alert alert-danger">
                    <strong>Error!</strong> ${option.tittle} already in basket.
                </div>
            </c:if></span></div>
        <c:choose>
            <c:when test="${empty basketOptions}">
                <p class="tittle_empty">BASKET IS EMPTY</p>
            </c:when>
            <c:otherwise>
                <table>
                    <br>
                    <tr style="height: 50px;">
                        <th style="width: 300px;"><p style="text-align: center;">TITTLE</p></th>
                        <th style="width: 300px;"><p class="text-center">PRICE</p></th>
                        <th style="width: 300px;"><p class="text-center">COST</p></th>

                        <th><p class="text-center"></p></th>
                    </tr>
                    <c:forEach items="${basketOptions}" var="option">
                        <c:set var="count" value="${count+1}"/>
                        <tr style="height: 60px;">
                            <td><p class="text-center">${option.tittle}</p></td>
                            <td><p class="text-center">${option.price}</p></td>
                            <td><p class="text-center">${option.cost}</p></td>

                            <td style="width: 180px;">
                                <p class="text-center">
                                    <c:choose>
                                        <c:when test="${count == 0}">
                                            <button onclick="" value="${option.id}"
                                                    class="btn btn-danger custom-width disabled">remove
                                            </button>
                                        </c:when>
                                        <c:when test="${userLoginDto.role == 'ADMINISTRATOR'}">
                                            <button onclick="removeFromBasket(this)" value="${option.id}"
                                                    class="btn btn-danger custom-width active">remove
                                            </button>
                                        </c:when>
                                        <c:when test="${userLoginDto.role == 'USER' and blockStatus != 'ADMIN_BLOCKING'}">
                                            <button onclick="removeFromUserBasket(this)" value="${option.id}"
                                                    class="btn btn-danger custom-width active">remove
                                            </button>
                                        </c:when>
                                    </c:choose>
                                </p>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
        <br>
        <div id="basket" class="well" style="padding: 10px 10px 10px; margin: 0;">
            <div style="text-align: right">
                <c:if test="${userLoginDto.role == 'USER'}">
                    <button onclick="ToUserContract(this)" value="${contractId}"
                            class="btn btn-danger custom-width active">BACK
                    </button>
                </c:if>

                <c:if test="${userLoginDto.role == 'ADMINISTRATOR' and contractId != null}">
                    <button onclick="backToUserContract(this)" value="${contractId}"
                            class="btn btn-danger custom-width active">BACK
                    </button>
                </c:if>
                <c:if test="${userLoginDto.role == 'ADMINISTRATOR' and contractId == null}">
                    <button onclick="backToContracts()" class="btn btn-danger custom-width active">BACK</button>
                </c:if>


                <c:if test="${blockStatus != 'ADMIN_BLOCKING'}">
                    <c:choose>
                        <c:when test="${count != 0}">
                            <c:if test="${userLoginDto.role == 'ADMINISTRATOR'}">
                                <button onclick="connectOptions()"
                                        class="btn btn-warning custom-width active">connect
                                </button>
                            </c:if>
                            <c:if test="${userLoginDto.role == 'USER'}">
                                <button onclick="connectUserOptions()"
                                        class="btn btn-warning custom-width active">connect
                                </button>
                            </c:if>
                        </c:when>
                    </c:choose>
                </c:if>
            </div>
        </div>
    </div>
</div>

<script>

    function removeFromBasket(obj) {
        var option = obj.value;
        var size = $("#basket_badge").html();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/basket/option/remove/" + option,
            success: function (response) {
                $("#admin_body").html(response);
                var n = Number(size) - 1;
                $("#basket_badge").text(n);
            }
        })
    }

    function removeFromUserBasket(obj) {
        var option = obj.value;
        var size = $("#basket_badge").html();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/basket/option/remove/" + option,
            success: function (response) {
                $("#user_body").html(response);
                var n = Number(size) - 1;
                $("#basket_badge").text(n);
            }
        })
    }

    function backToUserContract(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/user/contract/" + contractId,
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    }

    function ToUserContract(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/" + contractId,
            success: function (response) {
                $("#user_body").html(response);
            }
        })
    }

    function backToContracts() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contracts",
            success: function (user) {
                $("#admin_body").html(user);
            }
        });
    }

    function connectOptions() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/options/connect",
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#admin_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        })
    }

    function connectUserOptions() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/options/connect",
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#user_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        })
    }


</script>