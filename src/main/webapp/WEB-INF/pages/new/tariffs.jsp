<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/tariff_option.css"/>


<div class="col-md-12">
    <div class="well">
        <h2>Tariffs and options</h2>
    </div>
    <c:if test="${tariffs != null }">
        <div class="alert alert-danger">
            <strong>${tariffs}</strong>
        </div>
    </c:if>
    <c:if test="${tariffsArchive != null }">
        <div class="alert alert-danger">
            <strong>${tariffsArchive}</strong>
        </div>
    </c:if>
    <c:if test="${tariffsArchive != null }">
        <div class="alert alert-danger">
            <strong>${tariffsArchive}</strong>
        </div>
    </c:if>

    <div id="pricing-table" class="clear">
        <c:forEach items="${tariffDtos}" var="tariffDto">
            <c:set var="optionDtos" value="${tariffDto.optionDtos}"/>
            <div class="plan" id="most-popular">
                <h3 style="font-size: 30px">${tariffDto.tittle}<span>${tariffDto.price} <i
                        class="fa fa-euro"></i></span>
                </h3>
                <c:if test="${userLoginDto.role != null and tariffDto.isDeleted == true}">
                    <button id="tariff_detail" class="signup" onclick="getTariff(this)"
                            value="${tariffDto.id}">
                        Show
                    </button>
                    <button id="tariff_remove" class="signup" onclick="removeTariff(this)"
                            value="${tariffDto.id}">
                        Remove
                    </button>
                </c:if>
                <select class="dis_opt" style="margin-top: 20px;margin-left: 0;padding-left: 15px;"
                        title="" name="optDto">
                    <c:forEach items="${optionDtos}" var="optionDto">
                        <c:if test="${optionDto.isDeleted == true}">
                            <option value="${optionDto.id}" name="optDto"
                                    id="optDto">${optionDto.tittle}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
        </c:forEach>
    </div>
</div>

<script>
    function getTariff(obj) {
        var tariffId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariff/" + tariffId,
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    }

    function removeTariff(obj) {
        var tariffId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariff/remove/" + tariffId,
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (response) {
                        $("#admin_body").html(response);
                    }
                })
            }
        })
    }
</script>
