<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<footer>
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <div id="footer" class="soc_buttons">
                    <a href="https://vk.com"><i class="fa fa-vk"></i></a>
                    <a href="http://facebook.com"><i class="fa fa-facebook-square"></i></a>
                    <a href="http://twitter.com"><i class="fa fa-twitter"></i></a><br><br>
                    <p ><i class="fa fa-mobile"></i>
                        8-800-000-00-00</p>
                    <p style="margin-top: -25px"><i class="fa fa-envelope-o"></i>
                        mobileCom@mc.com</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <c:if test="${userLoginDto == null}">
                    <nav>
                        <ul>
                            <li><a href="${pageContext.request.contextPath}/main">main</a></li>
                            <li><a id="lk" href="#">tariffs and options</a></li>
                            <li><a href="#">info</a></li>
                        </ul>
                    </nav>
                </c:if>
            </div>
        </div>
        <div class="row">
            <div class="copyright">
                <p>Copyright @ 2017 MobileCompany</p>
            </div>
        </div>
    </div>
</footer>

<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-3.2.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/fancybox/jquery.fancybox.pack.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/waypoints/waypoints-1.6.2.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/scrollto/jquery.scrollTo.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/owl-carousel/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/countdown/jquery.plugin.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/countdown/jquery.countdown.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/countdown/jquery.countdown-ru.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/landing-nav/navigation.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/js/common.js"></script>
