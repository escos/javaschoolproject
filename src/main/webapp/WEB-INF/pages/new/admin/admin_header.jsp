<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <meta charset="utf-8"/>
    <meta name="description" content=""/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap-grid-3.3.1.min.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/new/libs/font-awesome-4.2.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/fancybox/jquery.fancybox.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/owl-carousel/owl.carousel.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/countdown/jquery.countdown.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/fonts.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/main.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/media.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/users.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-ui.css"/>

</head>

<input hidden id="nums" value="${usedNumbers}" title=""/>
<header class="top_header">
    <div class="header_topline">
        <div class="container">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-4">
                        <div class="logo">mobileCom</div>
                    </div>
                    <div class="col-md-8">
                        <button class="auth_buttons hidden-md hidden-lg hidden-sm"><i class="fa fa-user"></i></button>

                        <div class="soc_buttons">
                                <c:choose>
                                    <c:when test="${userLoginDto.role == 'ADMINISTRATOR' }">
                                        <a href="${pageContext.request.contextPath}/main/admin">
                                            <i class="fa fa-user"></i> admin </a>
                                    </c:when>
                                    <c:when test="${userLoginDto.role == 'USER' }">
                                        <a href="${pageContext.request.contextPath}/main/user">
                                            <i class="fa fa-user"></i> user</a>
                                    </c:when>
                                </c:choose>
                            <a href="#" id="basket">
                                <i class="fa fa-shopping-cart"></i></a>
                            <i id="basket_badge" class="badge">${basketList.size()}</i>

                            <a href="${pageContext.request.contextPath}/main/stop"><i class="fa fa-sign-out"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <nav class="maian_mnu ">
                    <c:if test="${userLoginDto.role == 'ADMINISTRATOR'}">
                        <button class="main_mnu_button hidden-md hidden-lg hidden-sm"><i class="fa fa-bars"></i>
                        </button>
                        <ul style="margin-bottom: -10px;">
                            <li><a id="users" href="#">Users</a></li>
                            <li><a id="contracts" href="#">Contracts</a></li>
                            <li class="dropdown">
                                <a class="droplink">Tariff menu</a>
                                <div class="dropdown-content">
                                    <a id="admin_tariffs" href="#">Active tariffs</a>
                                    <a id="archive" href="#">Tariff archive</a>
                                    <a id="create_tariff" href="#">Create tariff</a>
                                </div>
                            </li>
                            <li><a id="create" href="#">Create</a></li>
                            <li><a id="inc_messages" href="#">Messages</a></li>
                            <li>
                                <form id="search" class="search-container" method="post"
                                      action="/mymobile/main/admin/search">
                                    <input id="num" name="numberId" placeholder=" enter number..." type="text"
                                           maxlength="10" pattern="\d{10}" required=""/>
                                    <input class="btn btn-primary " type="submit" value="search"/>
                                </form>
                            </li>
                        </ul>
                    </c:if>
                </nav>
            </div>
        </div>
    </div>
</header>

<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-3.2.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-ui.js"></script>


<script>

    $("#search").submit(function (e) {
        e.preventDefault();
        var number = $("#search");
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/search",
            data: number.serialize(),
            success: function (response) {
                $("#admin_body").html(response);
                $("#search").trigger("reset");
            }
        });
    });

    $.ajax({
        type: "POST",
        url: "/mymobile/main/numbers",
        success: function (response) {
            var temp = JSON.parse(response);
            $("#num").autocomplete({
                source: temp,
                minLength: 2,
                onSelect: function () {
                    location.reload();
                }
            });
        }
    });

</script>

