<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/users.css"/>
</head>

<div class="generic-container users-right" style="border: none">
    <div class="panel panel-default" style="border-radius: 3px; padding: 5px">
        <div class="panel-heading" style="border-radius: 3px;"><span style="color: #4d4a72; font-size: 25px;
                                                  font-family: RobotoBold,sans-serif" class="lead tittle_table">USER LIST</span>
            <%@include file="empty.jsp"%>
        </div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th><p class="text-center">Name</p></th>
                <th><p class="text-center">Surname</p></th>
                <th><p class="text-center">Email</p></th>
                <th><p class="text-center">Passport</p></th>
                <th><p class="text-center">BirthDate</p></th>
                <th><p class="text-center">Address</p></th>
                <th><p class="text-center">Role</p></th>
                <th width="40"></th>
                <th width="40"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${userDtos}" var="userDto">
                <tr>
                    <td><p class="text-center">${userDto.name}</p></td>
                    <td><p class="text-center">${userDto.surname}</p></td>
                    <td><p class="text-center">${userDto.email}</p></td>
                    <td><p class="text-center">${userDto.passport}</p></td>
                    <td><p class="text-center">${userDto.birthDate}</p></td>
                    <td><p class="text-center">${userDto.address}</p></td>
                    <td><p class="text-center">${userDto.role}</p></td>
                    <td>
                        <p class="text-center">
                            <button type="button" value="${userDto.id}" onclick="contracts(this)"
                                    class="btn btn-primary custom-width">contracts
                            </button>
                        </p>
                    </td>
                    <td>
                        <p class="text-center">
                            <button type="button" value="${userDto.id}" onclick="removeUser(this)"
                                    class="btn btn-danger custom-width">delete
                            </button>
                        </p>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<script>
    function contracts(obj) {
        var userId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contracts/" + userId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }

    function removeUser(obj) {
        var userId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/user/remove/" + userId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }

</script>

