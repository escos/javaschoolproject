<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/users.css"/>
</head>
<c:set var="errStatus" scope="session" value="0"/>
<div class="generic-container users-right contr_users">
    <div id="contr_panel" class="panel-heading"><span class="lead" style="color: #4d4a72;font-size: 25px;
                                                     font-family: RobotoBold,sans-serif; border-radius: 3px;">CONTRACT LIST</span>
      <%@include file="empty.jsp"%>
    </div>
    <table class="table table-hover" style="margin: 3px 0 20px 0;">
        <thead>
        <tr>
            <th><p class="text-center">Email</p></th>
            <th><p class="text-center">Number</p></th>
            <th><p class="text-center">Tariff</p></th>
            <th><p class="text-center">Status</p></th>

            <th width="100"></th>
            <th width="100"></th>
            <th width="100"></th>
        </tr>
        </thead>
        <tbody>
        <%--<c:set var="contractId" scope="session" value="${contractId}"/>--%>
        <input id="old_id" hidden value="${contractId}"  title=""/>
        <c:forEach items="${contractSimpleDtos}" var="contractSimpleDto">
            <tr>
                <td><p class="text-center">${contractSimpleDto.userEmail}</p></td>
                <td><p class="text-center">${contractSimpleDto.phoneNumber}</p></td>
                <td><p class="text-center">${contractSimpleDto.tariff}</p></td>
                <td><p class="text-center">${contractSimpleDto.blockStatus}</p></td>
                <td>
                    <button type="button" value="${contractSimpleDto.id}" onclick="contractById(this)"
                            class="btn btn-primary custom-width">SHOW
                    </button>
                </td>
                <td>
                    <button type="button" value="${contractSimpleDto.id}" onclick="blockById(this)"
                            class="btn btn-warning custom-width">block/unblock
                    </button>
                </td>
                <td>
                    <button type="button" value="${contractSimpleDto.id}" onclick="removeById(this)"
                            class="btn btn-danger custom-width">delete
                    </button>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <c:if test="${thisUser != null}">
        <div class="well">
            <div class="row">
                <div class="col-md-12">

                    <p style="float: right">
                        <button onclick="createContractGet()" class="btn btn-success btn-sm">CREATE</button>
                    </p>

                </div>
            </div>
        </div>
    </c:if>
</div>

<script>
    function contractById(obj) {
        var contractId = obj.value;
        var oldId = $("#old_id").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/user/contract/" + contractId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
                if (oldId !== contractId) {
                    $("#basket_badge").text(0);
                }
            }
        })
    }

    function blockById(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contract/block/" + contractId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }

    function removeById(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contract/remove/" + contractId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }

    function createContractGet() {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/admin/contract/create",
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }
</script>

