<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<form:form id="option_update" method="POST" modelAttribute="option" action="/mymobile/main/admin/option/update">
    <div id="option_part" class="col-md-4">
        <c:if test="${optUpErr != null }">
            <div class="alert alert-danger">
                <strong>${optUpErr}</strong>
            </div>
        </c:if>
        <p style="font-size: 20px; margin: -10px 0 10px 55px;;">Update option</p>
        <input id="removeId" hidden value="${option.id}" title="" name="id"/>
        <div class="row">
            <input id="option_tittle" style="margin: 0 0 0 50px;" type="text" placeholder="Option tittle" name="tittle"
                   value="${option.tittle}" pattern="[a-zA-z]{3,30}" minlength="3" maxlength="25" required=""/>
        </div>
        <div class="row">
            <input  id="option_price" style="margin-top: 20px " type="text" placeholder="Price" name="price"
                   value="${option.price}" required="" minlength="2" maxlength="4" pattern="[1-9]{1}[0-9]{1,3}"/>
            <i class="fa fa-euro"></i>
            <input id="option_cost" style="margin-top: 20px; margin-left: 0" type="text" placeholder="Cost" name="cost"
                   value="${option.cost}" required="" minlength="2" maxlength="4" pattern="[1-9]{1}[0-9]{1,3}"/>
            <i class="fa fa-euro"></i>
        </div>
        <div class="row">
            <input id="opt_update_submit" style=" margin: 20px 85px -30px 5px;" class="fa fa-repeat" type="submit" value="UPDATE"/>
        </div>
    </div>
</form:form>

<script>

    $("#option_update").submit(function (e) {
        e.preventDefault();
        var data = $("#option_update").serialize();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/option/update",
            data: data,
            success: function (response) {
                $("#target_update_option").html(response);
            }
        })
    });

</script>

