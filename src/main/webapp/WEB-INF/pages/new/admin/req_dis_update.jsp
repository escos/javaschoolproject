<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-md-6" style="margin: auto">
    <div class="row">
        <p>Required options</p>
    </div>
    <div class="row">
        <select  class="dis_opt" title="" name="req_opt" >
            <c:forEach items="${option.reqOptions}" var="reqOption">
                <option value="${reqOption}" name="req_opt" id="req">${reqOption}</option>
            </c:forEach>
        </select>
        <a onclick="removeReqOptByTittle(${option.id})" >
            <i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </div>
    <div class="row">
        <%--<input id="input_opt_dis" type="text" placeholder="Option" required=""/>--%>
        <select class="dis_opt" title="" name="req_opt">
            <c:forEach items="${option.reqOptions}" var="reqOption">
                <option value="${reqOption}" id="req_add"
                        name="req_opt">${reqOption}</option>
            </c:forEach>
        </select>
        <a href="#"><i class="fa fa-check-square"></i></a>
    </div>
</div>

<div class="col-md-6" style="margin: auto">
    <div class="row">
        <p>Incompatible options</p>
    </div>
    <div class="row">
        <select  class="dis_opt" title="" name="dis_opt">
            <c:forEach items="${option.disOptions}" var="disOption">
                <option id="dis" value="${disOption}" name="req_opt">${disOption}</option>
            </c:forEach>
        </select>
        <a id="remove_dis" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </div>
    <div class="row">
        <select class="dis_opt" title="" name="dis_opt">
            <c:forEach items="${option.disOptions}" var="disOption">
                <option id="dis_add" value="${disOption}" name="req_opt">${disOption}</option>
            </c:forEach>
        </select>
        <a href="#"><i class="fa fa-check-square" aria-hidden="true"></i></a>
    </div>
</div>