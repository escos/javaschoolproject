<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${updateErr != null }">
    <div class="alert alert-danger">
        <strong>${updateErr}</strong>
    </div>
</c:if>
<form:form id="tariff_form" modelAttribute="tariffDto" method="POST"
                       action="/mymobile/main/admin/tariff/update">
    <c:set value="${tariffDto.id}" var="updateId" scope="session"/>
    <input id="tariff_id" hidden value="${tariffDto.id}" title="" name="id"/>
    <div class="col-md-4" style=" border-right: 1px #ccc solid;">
        <p style="font-size: 20px; margin: 2px 0 10px 55px;">Tariff tittle</p>
        <div class="row">
            <input id="tariff_tittle" type="text" placeholder="Tariff" name="tittle"
                   value="${tariffDto.tittle}" pattern="[a-zA-z]{3,30}" minlength="3" maxlength="25" required=""/>
        </div>
        <p style="font-size: 20px; margin: 0 0 10px 55px;">Tariff price</p>
        <div class="row">
            <input id="tariff_price" type="text" placeholder="Price" name="price" minlength="2" maxlength="4"
                   pattern="[1-9]{1}[0-9]{1,3}" value="${tariffDto.price}" required=""/>
            <i class="fa fa-euro" style="margin-left: 10px"></i>
        </div>
        <div class="row">
            <input id="tarSubmit" class="update" type="submit" value="UPDATE"/>
        </div>
    </div>
</form:form>

<script>
    $("#tariff_form").submit(function (e) {
        e.preventDefault();
        var data = $("#tariff_form").serialize();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariff/update",
            data: data,
            success: function (response) {
                $("#updated_tariff").html(response);
            }
        });
    });
</script>