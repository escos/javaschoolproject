<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Administrator</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/admin.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/admin_messages.css"/>
</head>
<body>
<%@include file="admin_header.jsp" %>

<section class="main_content">
    <div class="container">
        <div class="col-md-12">
            <div class="container main_container" id="admin_body">

                <div class="col-md-9">
                    <div class="generic-container users-right" style="margin-left: -15px;">
                        <div class="panel panel-default" style="border-radius: 3px;padding: 5px">
                            <div class="panel-heading" style="border-radius: 3px;"><span
                                    class="lead">Incoming messages</span></div>
                            <div id="mesgs">
                                <table class="inner_table">
                                    <thead>
                                    <tr>
                                        <th><p class="text-center">User's email</p></th>
                                        <th><p class="text-center">Tariff plan</p></th>
                                        <th><p class="text-center">Phone number</p></th>
                                        <th><p class="text-center">Date/time</p></th>

                                        <th width="50"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:if test="${messages.size() == 0}">
                                            <div class="alert alert-danger">
                                                <strong>No messages</strong>
                                            </div>
                                    </c:if>
                                    <c:if test="${messages.size() != 0}">
                                        <c:forEach items="${messages}" var="message">
                                            <tr>
                                                <td style="width: 230px"><i class="fa fa-envelope"></i> ${message.email}</td>
                                                <td style="width: 200px"><i class="fa fa-file-text"></i>${message.tittle}</td>
                                                <td style="width: 170px"><i class="fa fa-mobile"></i> ${message.number}</td>
                                                <td style="width: 120px"> ${message.date}</td>
                                                <td>
                                                    <button type="button" id="disOption" value="${message.id}"
                                                            onclick="removeMessage(this)"
                                                            class="but_opt_add"><i class="fa fa-times-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div id="admin_main_mes" class="row option_create">
                        <div class="lead" style="font-size: 22px;">${userLoginDto.name} ${userLoginDto.surname}</div>
                        <ul class="first">
                            <li><span>Email:<em>${userLoginDto.email}</em></span></li>
                            <li><span>Address:<em>${userLoginDto.address}</em></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="../footer.jsp" %>
</body>

<script>

    (function poll() {
        $.ajax({
            url: "/mymobile/main/admin/messages",
            type: "GET",
            success: function (data) {
                $("#mesgs").html(data);
            },
            complete: setTimeout(function () {
                poll()
            }, 300000),
            timeout: 50000
        })
    })();

    function removeMessage(obj) {
        var messageId = obj.value;
        $.ajax({
            url: "/mymobile/main/admin/message/remove/"+messageId,
            type: "POST",
            success: function (data) {
                $("#mesgs").html(data);
            }
        })
    }


</script>
</html>
