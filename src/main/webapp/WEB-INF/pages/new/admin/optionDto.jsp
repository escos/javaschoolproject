<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="row option_create" style="padding-bottom: 45px;">
    <div class="row">
        <a onclick="removeOptionById()" href="#"><i class="fa fa-times-circle"></i></a>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <p style="font-size: 25px; ">OPTION</p>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div id="option" class="row">
        <div id="target_update_option">
            <jsp:include page="option_update.jsp"/>
        </div>

        <div id="req_dis_update" class="col-md-8">
            <div class="col-md-6" style="margin: auto">
                <div class="row">
                    <p>Required options</p>
                </div>
                <c:set value="${option.reqOptions}" var="reqOptions"/>
                <c:if test="${reqOptions.size() != 0}">
                    <div class="row">
                        <select id="req" class="dis_opt" title="" name="req_opt">
                            <c:forEach items="${option.reqOptions}" var="reqOption">
                                <option value="${reqOption}" name="req_opt">${reqOption}</option>
                            </c:forEach>
                        </select>
                        <button class="but_opt_add" onclick="removeReqOptByTittle(${option.id})">
                            <i class="fa fa-trash-o"></i></button>
                    </div>
                </c:if>
                <c:if test="${reqOptions.size() == 0}">
                    <div class="row">
                        <select class="dis_opt" title="" name="req_opt">
                            <option selected>No options</option>
                        </select>
                    </div>
                </c:if>
                <c:set value="${option.enableReqOptions}" var="enableOptions"/>
                <c:if test="${enableOptions.size() != 0}">
                    <div class="row">
                        <select id="req_add" class="dis_opt" title="" name="req_opt">
                            <c:forEach items="${enableOptions}" var="enableOption">
                                    <option value="${enableOption}" name="req_opt">${enableOption}</option>
                            </c:forEach>
                        </select>
                        <button class="but_opt_add" onclick="addReqOptByTittle(${option.id})">
                            <i class="fa fa-check-square"></i></button>
                    </div>
                </c:if>
                <c:if test="${enableOptions.size() == 0}">
                    <div class="row">
                        <select class="dis_opt" title="">
                            <option selected>No options</option>
                        </select>
                    </div>
                </c:if>
            </div>

            <div class="col-md-6" style="margin: auto">
                <div class="row">
                    <p>Incompatible options</p>
                </div>
                <div class="row">
                    <c:set value="${option.disOptions}" var="disOptions"/>
                    <c:if test="${disOptions.size() != 0}">
                        <select id="dis" class="dis_opt" title="" name="dis_opt" style="margin-left: 75px">
                            <c:forEach items="${disOptions}" var="disOption">
                                <option value="${disOption}" name="dis_opt">${disOption}</option>
                            </c:forEach>
                        </select>
                        <button class="but_opt_add" onclick="removeDisOptByTittle(${option.id})">
                            <i class="fa fa-trash-o"></i>
                        </button>
                    </c:if>
                    <c:if test="${disOptions.size() == 0}">
                        <div class="row">
                            <select class="dis_opt" title="" style="margin-left: 90px">
                                <option selected>No options</option>
                            </select>
                        </div>
                    </c:if>
                </div>
                <div class="row">
                    <c:set value="${option.enableDisOptions}" var="enableDisOptions"/>
                    <c:if test="${enableDisOptions.size() != 0}">
                        <select id="dis_add" class="dis_opt" title="" name="dis_opt" style="margin-left: 75px">
                            <c:forEach items="${enableDisOptions}" var="enableOption">
                                    <option value="${enableOption}" name="dis_opt">${enableOption}</option>
                            </c:forEach>
                        </select>
                        <button class="but_opt_add" onclick="addDisOptByTittle(${option.id})">
                            <i class="fa fa-check-square"></i></button>
                    </c:if>
                    <c:if test="${enableDisOptions.size() == 0}">
                        <div class="row">
                            <select class="dis_opt" title="" style="margin-left: 90px">
                                <option selected>No options</option>
                            </select>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
