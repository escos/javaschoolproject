<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set value="${newUser}" var="userDto" scope="session"/>
<div class="generic-container users-right">
    <div class="panel panel-default" style="border-radius: 3px;padding-right: 20px; border-color: #fff;">
        <div class="panel-heading" style="border-radius: 3px; margin-left: 5px;margin-right: -15px">
            <span class="lead tittle_table">CREATE CONTRACT</span>
        </div>

        <div class="row">
            <div class="col-md-9">
                <p class="text-center" style="font-size: 25px;color: #4d4a72; font-family: RobotoRegular,
                                 cursive">POSSIBLE TARIFFS</p>
                <table>
                    <c:forEach items="${tariffDtos}" var="tariffDto">
                        <%@include file="tariff_for_create_contract.jsp" %>
                    </c:forEach>
                </table>
            </div>
            <div class="col-md-3">
                <div class="row option_create">
                    <c:choose>
                        <c:when test="${newUser != null}">
                            <div class="lead" style="font-size: 22px;">${newUser.name} ${newUser.surname}</div>
                            <ul class="first">
                                <li><span>Email:<em>${newUser.email}</em></span></li>
                                <li><span>Address:<em>${newUser.address}</em></span></li>
                                <li><span>BirthDate:<em>${newUser.birthDate}</em></span></li>
                                <li><span>Passport:<em>${newUser.passport}</em></span></li>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <div class="lead" style="font-size: 22px;">${thisUser.name} ${thisUser.surname}</div>
                            <ul class="first">
                                <li><span>Email:<em>${thisUser.email}</em></span></li>
                                <li><span>Address:<em>${thisUser.address}</em></span></li>
                                <li><span>BirthDate:<em>${thisUser.birthDate}</em></span></li>
                                <li><span>Passport:<em>${thisUser.passport}</em></span></li>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="row option_create">
                    <div class="lead_opts" style="font-size: 22px;">Choose number</div>
                    <ul class="first first_opts">
                        <li>
                            <select id="number_id" name="phoneNumberDto" class="user_opt">
                                <c:forEach items="${numbers}" var="number">
                                    <option value="${number.id}" name="phoneNumberDto">${number.id}</option>
                                </c:forEach>
                            </select>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="well" style="margin: 5px 0 0 20px;    padding: 10px 20px 1px">
                <c:choose>
                    <c:when test="${newUser != null}">
                        <p style="text-align: right;">
                            <button id="cancel" class="btn btn-primary custom-width">CANCEL</button>
                        </p>
                    </c:when>
                    <c:when test="${newUser == null}">
                        <p style="text-align: right;">
                            <button id="cancel_contracts" class="btn btn-primary custom-width">CANCEL</button>
                        </p>
                    </c:when>
                </c:choose>
            </div>
        </div>
    </div>
</div>

<script>

    function createContract(obj) {
        var tariffId = obj.value;
        var number = $("#number_id").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contract/create",
            data: {tariffId: tariffId, number: number},
            success: function (data) {
                $("#admin_body").html(data);
            }
        });
    }
</script>


