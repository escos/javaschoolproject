<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/tariff_main.css"/>
</head>

<c:set var="optionDtos" value="${tariffDto.optionDtos}"/>
<div class="row tariff_main_form">
    <div class="col-md-1">
        <img src="${pageContext.request.contextPath}/resources/new/img/sim.png" alt="alt">
    </div>
    <div class="col-md-4 tar_sect">
        <h2 class="tariff_plan">${tariffDto.tittle}</h2>
        <ul class="main_sect_tariff">
            <li><span>Unlimited</span></li>
            <li><span>4G Network</span></li>
            <li><span>Month-to-month</span></li>
            <li><span>Included 120 international minutes</span></li>
        </ul>
    </div>
    <div class="col-md-5">
        <h3 class="options">Special options</h3>
        <ul class="second_sect_tariff">
            <c:forEach items="${optionDtos}" var="optionDto">
                <c:if test="${optionDto.isDeleted == true}">
                    <li><span>${optionDto.tittle} <em>${optionDto.price}
                            <i class="fa fa-eur" aria-hidden="true"></i></em></span>
                    </li>
                </c:if>
            </c:forEach>
        </ul>
    </div>
    <div class="col-md-2">
        <h2 class="price_tariff">${tariffDto.price} <i class="fa fa-eur" aria-hidden="true"></i></h2>
        <p style="font-size: 14px;font-family: monospace; color: darkgrey; margin: -10px 0 0 20px; ">per month</p>
        <button id="send_data" onclick="createContract(this)" value="${tariffDto.id}" class="show_tar_but">Create</button>
    </div>
</div>