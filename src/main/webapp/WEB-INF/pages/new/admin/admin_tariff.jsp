<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@include file="empty.jsp"%>

<div class="col-md-12">
    <div class="tariff_container">
        <div class="row">
            <div class="well">
                <h2 style="font-family: RobotoBold, sans-serif;color: #4d4a72;text-align: center;">
                    Tariff "${tariffDto.tittle}" in details
                </h2>
            </div>
        </div>
        <div id="create_form" class="row create_form">

            <h3>Update tariff</h3>
            <div id="tariff_part" class="row">
                <div id="updated_tariff">
                    <jsp:include page="tariff_update.jsp"/>
                </div>
                <div class="col-md-3">
                    <p style="font-size: 20px; margin: 0 0 10px 90px;">Choose option</p>
                    <div class="row">
                        <select onchange="loadOption(this)" id="opt_changer" class="dis_opt" title="" name="optDto">
                            <option selected disabled >Options</option>
                            <c:forEach items="${tariffDto.optionAdminDtos}" var="optionDto">
                                <c:if test="${optionDto.isDeleted == true}">
                                    <option value="${optionDto.id}" name="optDto"
                                            id="optDto">${optionDto.tittle}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                        <button id="wrap" class="add_option_button" onclick="addOptionShow()">ADD</button>
                    </div>
                    <c:if test="${optionErr != null }">
                        <div class="alert alert-danger">
                            <strong>${optionErr}</strong>
                        </div>
                    </c:if>
                    <c:if test="${removeOption != null }">
                        <div class="alert alert-danger">
                            <strong>${removeOption}</strong>
                        </div>
                    </c:if>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <c:if test="${optCr != null }">
                        <div class="alert alert-danger">
                            <strong>${optCr}</strong>
                        </div>
                    </c:if>
                    <div id="wrapp">
                        <form:form id="option_create" modelAttribute="optionDto" method="POST">
                                   <%--action="/mymobile/main/admin/option/create">--%>
                            <div class="row option_create cr" style="margin-top: -10px; padding-top: 15px; border: none;
                                                                     border-left: 1px solid #ccc" >
                                <div class="row">
                                    <p class="title_opt" style="font-size: 20px;font-family: RobotoRegular, sans-serif
                                     ;color: #333;margin: -5px 0 10px -10px; text-transform: none">New option</p>
                                </div>
                                <div class="row">
                                    <div class="row">
                                        <input id="new_option_tittle" type="text" style="margin-top: 0" placeholder="Option tittle"
                                               name="tittle" pattern="[a-zA-z]{3,30}" minlength="3" maxlength="25"
                                               required=""/>
                                    </div>
                                    <div class="row">
                                        <span class="option_data" >Price </span><span class="option_data" >Cost </span>
                                    </div>
                                    <div class="row">
                                        <input id="new_option_price" style="margin-top: 10px" type="text" placeholder="Price" name="price"
                                               minlength="2" maxlength="4" pattern="[1-9]{1}[0-9]{1,3}" required=""/>
                                        <i class="fa fa-euro"></i>
                                        <input id="new_option_cost" type="text" placeholder="Cost" name="cost"
                                               required="" minlength="2" maxlength="4" pattern="[1-9]{1}[0-9]{1,3}"/>
                                        <i class="fa fa-euro"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <input id="opt_create_submit" type="submit" value="CREATE"/>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
            <div id="target_option">

            </div>
            <br>
        </div>
    </div>
</div>

<script>

    function removeOptionById() {
        var optionId = $("#removeId").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/option/remove/" + optionId,
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#admin_body").html(data);
                    }
                });
            }
        })
    }

    function removeReqOptByTittle(optionId) {
        var reqOptionTittle = $("#req").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/option/remove_req/" + reqOptionTittle,
            data: {optionId: optionId},
            success: function (response) {
                $("#target_option").html(response);
            }
        })
    }

    function removeDisOptByTittle(optionId) {
        var disOptionTittle = $("#dis").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/option/remove_dis/" + disOptionTittle,
            data: {optionId: optionId},
            success: function (response) {
                $("#target_option").html(response);
            }
        })
    }

    $("#option_create").submit(function (e) {
        e.preventDefault();
        var data = $("#option_create").serialize();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/option/create",
            data: data,
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#admin_body").html(data);
                    }
                });
            }
        })
    });

    function addReqOptByTittle(optionId) {
        var reqOptionTittle = $("#req_add").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/option/add_req/" + reqOptionTittle,
            data: {optionId: optionId},
            success: function (response) {
                $("#target_option").html(response);
            }
        })
    }

    function addDisOptByTittle(optionId) {
        var disOptionTittle = $("#dis_add").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/option/add_dis/" + disOptionTittle,
            data: {optionId: optionId},
            success: function (response) {
                $("#target_option").html(response);
            }
        })
    }

    function addOptionShow() {
        $("#wrapp").show();
        $("#wrap").attr("hidden", true);
    }

    function loadOption(obj) {
        var optionId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/option/show",
            data: {optionDtoId: optionId},
            success: function (data) {
                $("#target_option").html(data);
            }
        });
    }

</script>


