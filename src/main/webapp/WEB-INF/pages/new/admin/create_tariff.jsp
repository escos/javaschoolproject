<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${tariffErr != null}">
    <div class="alert alert-error">
        <strong>${tariffErr}</strong>
    </div>
</c:if>
<div class="col-md-12">
    <div class="tariff_container">
        <div class="row">
            <div class="well tittle_table">
                <h2>Tariffs and options</h2>
            </div>
        </div>
        <div class="row create_form">
            <h3>Create tariff</h3>
            <form id="cr_tar_form" action="/mymobile/main/admin/tariff/create" method="POST">
                <div class="row">
                    <div class="col-md-4">
                        <p style=" font-size: 20px; margin: 25px 0 -10px 95px;">Tariff tittle</p>
                        <input id="tariff_tittle" type="text" name="tariffTittle" placeholder="Tariff tittle"
                               pattern="[a-zA-z]{3,30}" minlength="3" maxlength="25" required=""/>
                    </div>

                    <div class="col-md-3">
                        <div>
                            <p style=" font-size: 20px; margin: 25px 0 10px 55px;">Tariff price</p>
                            <input id="tariff_price" type="text" name="tariffPrice" minlength="2" maxlength="4"
                                   pattern="[1-9]{1}[0-9]{1,3}" placeholder="Price" required=""/><i
                                class="fa fa-euro"></i>
                        </div>
                    </div>
                    <div class="col-md-5" id="opt_create">
                        <div class="option_create " style="font-size: 25px" id="opt_add">
                            <p style="font-size: 20px">OPTION</p>
                            <input id="option_tittle" type="text" name="optionTittle" placeholder="Option tittle"
                                   pattern="[a-zA-Z]{3,25}" required=""/>
                            <input id="option_price" type="text" name="optionPrice" placeholder="Price"
                                   minlength="2" maxlength="4" pattern="[1-9]{1}[0-9]{1,3}" required=""/>
                            <i class="fa fa-euro" style="margin-left: 0"></i>
                            <input id="option_cost" type="text" name="optionCost" placeholder="Cost"
                                   style="margin-left: 0"
                                   minlength="2" maxlength="4" pattern="[1-9]{1}[0-9]{1,3}" required=""/>
                            <i class="fa fa-euro" style="margin-left: 0"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div>
                        <input id="cr_tar_submit" type="submit" value="CREATE"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-3.2.1.js"></script>

<script>

    $("#cr_tar_form").submit(function (e) {
        e.preventDefault();
        var tarTittle = $("#tariff_tittle").val();
        var tarPrice = $("#tariff_price").val();
        var optTittle = $("#option_tittle").val();
        var optPrice = $("#option_price").val();
        var optCost = $("#option_cost").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariff/create",
            data: {
                tariffTittle: tarTittle, tariffPrice: tarPrice,
                optionTittle: optTittle, optionPrice: optPrice, optionCost: optCost
            },
            success: function (response) {
                if (response === "error") {
                    window.location = "/mymobile/main/oops";
                }
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#admin_body").html(data);
                    }
                })
            }
        })
    });
</script>
