<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/user_tariff_details.css"/>
</head>
<%@include file="empty.jsp"%>

<main style="min-width: 320px;
    height: 580px;
    padding: 5px;
    margin: 0 auto;
    background: #fff;">
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1">User contract details</label>

    <input id="tab2" type="radio" name="tabs">
    <label for="tab2">Free tariffs</label>

    <section id="content1">
        <div class="tariff_container">
            <div class="col-md-9">
                <div id="user_content" class="generic-container users-right" style="border-radius: 3px;">
                    <div class="panel panel-default" style="border-radius: 3px;padding: 5px">
                        <c:if test="${tarSuccess != null }">
                            <div class="alert alert-success">
                                <strong>${tarSuccess}</strong>
                            </div>
                        </c:if>
                        <c:if test="${reqOptsError != null }">
                            <div class="alert alert-danger">
                                <strong>${reqOptsError}</strong>
                            </div>
                        </c:if>
                        <c:if test="${disOptsError != null }">
                            <div class="alert alert-danger">
                                <strong>${disOptsError}</strong>
                            </div>
                        </c:if>
                        <div class="panel-heading" style="border-radius: 10px;"><span class="lead tittle_table">Free options</span>
                        </div>
                        <c:choose>
                            <c:when test="${empty freeTittleOptions}">
                                <p style="text-align: center;margin: 3px;" class="tittle_empty"> NO FREE OPTIONS</p>
                            </c:when>
                            <c:otherwise>
                                <table class="inner_table">
                                    <thead>
                                    <tr>
                                        <th><p class="text-center">Option tittle</p></th>
                                        <th><p class="text-center" style="width: 160px">Option price,
                                            <i class="fa fa-eur"></i></p></th>
                                        <th><p class="text-center" style="width: 160px">Option cost,
                                            <i class="fa fa-eur"></i></p></th>
                                        <th width="100"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:set var="contractId" scope="session" value="${contractId}"/>
                                    <input id="userContract" type="hidden" name="id" value="${contractId}"/>
                                    <c:set var="count" value="0"/>
                                    <c:forEach items="${freeTittleOptions}" var="freeTittleOption">
                                        <c:set var="count" value="${count+1}"/>
                                        <c:if test="${freeTittleOption.isDeleted == true}">
                                            <c:if test="${count != 0}">
                                                <tr>
                                                    <td style="width: 300px">
                                                        <div class="dropdown">
                                                            <i class="fa fa-info-circle"></i>
                                                            <div class="dropdown-content drop">
                                                                <c:choose>
                                                                    <c:when test="${empty freeTittleOption.reqOptions
                                                                    and empty freeTittleOption.disOptions}">
                                                                        <p style="font-size: 17px; color: #4cae4c">
                                                                            OPTION FREE FOR CONNECTION</p>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <p>Required options</p>
                                                                        <ul class="options_list">
                                                                            <c:choose>
                                                                                <c:when test="${empty freeTittleOption.reqOptions }">
                                                                                    <p style="color: #28a4c9">NO
                                                                                        REQUIRED OPTIONS</p>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:forEach
                                                                                            items="${freeTittleOption.reqOptions}"
                                                                                            var="reqOption">
                                                                                        <li>${reqOption}</li>
                                                                                    </c:forEach>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </ul>
                                                                        <p>Incompatible options</p>
                                                                        <ul class="options_list">
                                                                            <c:choose>
                                                                                <c:when test="${empty freeTittleOption.disOptions }">
                                                                                    <li style="color: #28a4c9">NO
                                                                                        INCOMPATIBLE OPTIONS
                                                                                    </li>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:forEach
                                                                                            items="${freeTittleOption.disOptions}"
                                                                                            var="disOption">
                                                                                        <li>${disOption}</li>
                                                                                    </c:forEach>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </ul>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                        </div>
                                                            ${freeTittleOption.tittle}
                                                    </td>
                                                    <td style="width: 100px">${freeTittleOption.price}</td>
                                                    <td style="width: 100px">${freeTittleOption.cost}</td>
                                                    <td>
                                                        <button type="button" onclick="addToBasket(this)"
                                                                value="${freeTittleOption.id}"
                                                                class="but_opt_add">
                                                            <i class="fa fa-plus-circle"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row option_create">
                    <div class="lead ">${userShow.surname} ${userShow.name}</div>
                    <ul class="first">
                        <li><span>Tariff plan:<em>${tariff.tittle}</em></span></li>
                        <li><span>Number:<em>${number}</em></span></li>
                        <li><span>Status:<em>${status}</em></span></li>
                    </ul>
                </div>
                <c:if test="${connectedOptions.size() != 0}">
                    <div class="row option_create opts">
                        <div class="lead_opts">Connected options</div>
                        <ul class="first first_opts">
                            <c:forEach items="${connectedOptions}" var="connectedOption">
                                <li><span>${connectedOption.tittle}
                                <em><button type="button" id="disOption" value="${connectedOption.id}"
                                            onclick="disconnectOption(this)"
                                            class="but_opt_add"><i class="fa fa-power-off" aria-hidden="true"></i>
                                    </button></em></span>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </c:if>
            </div>
        </div>
    </section>

    <section id="content2">
        <div class="panel-heading" style="border-radius: 5px;">
            <span class="lead trf_lead">FREE TARIFFS FOR CONNECTION</span>
        </div>
        <div class="tariff_container">
            <div class="col-md-9">
                <c:forEach items="${freeTariffs}" var="freeTariff">
                    <div class="row tariff_main_form">
                        <div class="col-md-5 tar_sect">
                            <h2 class="tariff_plan">${freeTariff.tittle}</h2>
                            <ul class="main_sect_tariff">
                                <li><span>Tariff plan</span></li>
                                <li><span>4G Network</span></li>
                                <li><span>Month-to-month</span></li>
                                <li><span>Included 120 international minutes</span></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h3 class="options">Options</h3>
                            <ul class="second_sect_tariff">
                                <c:forEach items="${freeTariff.optionDtos}" var="optionDto">
                                    <li><span>${optionDto.tittle}</span></li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h2 class="price_tariff">${freeTariff.price} <i class="fa fa-eur"></i></h2>
                            <p style="font-size: 15px;font-family: monospace; color: darkgrey; margin-left: 30px;">per month</p>
                                <button id="change-but" type="button"
                                        onclick="changeTariff(this)" value="${freeTariff.id}"
                                        class="show_tar_but">CHANGE
                                </button>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div class="col-md-3">
                <div class="row option_create" style="margin-top: 5px">
                    <div class="lead ">${userShow.surname} ${userShow.name}</div>
                    <ul class="first">
                        <li><span>Tariff plan:<em>${tariff.tittle}</em></span></li>
                        <li><span>Number:<em>${number}</em></span></li>
                        <li><span>Status:<em>${status}</em></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="${pageContext.request.contextPath}/resources/new/js/header.js"></script>

<script>

    function addToBasket(obj) {
        var contractId = $("#userContract").val();
        var optionId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/basket/add",
            data: {id: contractId, optionId: optionId},
            success: function (response) {
                var size = $("#basket_badge").html();
                $.ajax({
                    type: "POST",
                    url: response[1],
                    success: function (data) {
                        $("#admin_body").html(data);
                        if (response[0] === "1") {
                            var n = Number(size) + 1;
                            $("#basket_badge").html(n);
                        }
                    }
                });
            }
        })
    }

    function disconnectOption(obj) {
        var id = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/option/disconnect",
            data: {optionDtoId: id},
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#admin_body").html(data);
                    }
                });
            }
        });
    }

    function changeTariff(obj) {
        var id = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/tariff/update",
            data: {tariffId: id},
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#admin_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        });
    }

</script>
