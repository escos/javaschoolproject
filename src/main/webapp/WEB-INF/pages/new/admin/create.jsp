<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<head>
    <title>Create_user</title>
</head>

<c:if test="${request != null}">
    <c:set value="${message}" var="messData" scope="session"/>
    <c:set var="statusMessage" value="1" scope="session"/>
</c:if>
<c:if test="${request == null}">
    <c:set value="${null}" var="messData" scope="session"/>
    <c:set var="statusMessage" value="0" scope="session"/>
</c:if>

<div class="col-md-12">
    <div class="registr_container">
        <div class="row">
            <div class="well">
                <h2>User registration form</h2>
            </div>
        </div>
        <div class="row reg_form">
            <form:form id="reg_form" class="form-horizontal" method="POST" modelAttribute="userDto"
                       action="${pageContext.request.contextPath}/main/admin/user/create">
                <c:if test="${status == -1}">
                    <div class="alert alert-danger">
                        <spring:message code="User.registration"/>
                    </div>
                </c:if>
                <c:if test="${status == -2}">
                    <div class="alert alert-danger">
                        <spring:message code="User.registration.fail"/>
                    </div>
                </c:if>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Name:</label>
                    <div class="col-sm-10">
                        <form:input type="text" class="form-control" placeholder="Enter name" minlength="2"
                                    maxlength="20" pattern="[a-zA-Z]{2,20}" name="name" path="name"/>
                        <div class="has-error">
                            <form:errors path="name" class="control-label"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="surname">Surname:</label>
                    <div class="col-sm-10">
                        <form:input path="surname" type="text" class="form-control" minlength="2" maxlength="20"
                                    pattern="[a-zA-Z]{2,20}" required="" placeholder="Enter surname" name="surname"/>
                        <div class="has-error">
                            <form:errors path="surname" class="control-label"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-10">
                        <form:input path="email" type="email" class="form-control" required="" placeholder="Enter email"
                                    name="email">${messData.email}</form:input>
                        <div class="has-error">
                            <form:errors path="email" class="control-label"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="birthDate">BirthDate:</label>
                    <div class="col-sm-10">
                        <form:input path="birthDate" type="text" class="form-control" required=""
                                    placeholder="Enter birthdate" name="birthDate"/>
                        <div class="has-error">
                            <form:errors path="birthDate" class="control-label"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="address">Address:</label>
                    <div class="col-sm-10">
                        <form:input path="address" type="text" class="form-control"
                                    placeholder="Enter address" required=""
                                    name="address"/>
                        <div class="has-error">
                            <form:errors path="address" class="control-label"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="passport">Passport:</label>
                    <div class="col-sm-10">
                        <form:input path="passport" type="text" class="form-control" required="" maxlength="11"
                                    placeholder="Enter passport"
                                    name="passport"/>
                        <div class="has-error">
                            <form:errors path="passport" class="control-label"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="password">Password:</label>
                    <div class="col-sm-10">
                        <form:input path="password" type="password" class="form-control" required=""
                                    placeholder="Enter password" name="password"/>
                        <div class="has-error">
                            <form:errors path="password" class="control-label"/>
                        </div>
                    </div>
                </div>
                <div class=" well">
                    <label>
                        <input type="radio" name="role" value="ADMINISTRATOR">Administrator
                    </label>
                    <label>
                        <input type="radio" name="role" value="USER">User
                    </label>
                    <div class="has-error">
                        <form:errors path="role" class="control-label"/>
                    </div>
                    <p style="float: right">
                        <input type="submit" value="NEXT"/>
                    </p>
                </div>
            </form:form>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/resources/new/js/user_create.js"></script>



