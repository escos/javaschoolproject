<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/header.css"/>

</head>
<nav>
    <div class="logo"></div>
    <ul>
        <li><a href="${pageContext.request.contextPath}/main"><i class="fa fa-home"></i></a></li>
        <li><a href="${pageContext.request.contextPath}/main/auth"><i class="fa fa-user"></i></a></li>
        <c:if test="${showTariffs == 1}">
            <li><a id="lk" href="#"><i class="fa fa-file-text"></i></a></li>
        </c:if>
    </ul>
</nav>
<header>
    <div class="headline">
        <div class="inner">

        </div>
    </div>
</header>


<script src="${pageContext.request.contextPath}/resources/new/libs/jquery/jquery-3.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/new/js/header.js"></script>