<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="panel-heading" style="border-radius: 3px;"><span class="lead tittle_table">CONTRACT LIST</span>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th><p class="text-center">Number</p></th>
        <th><p class="text-center">Tariff plan</p></th>
        <th><p class="text-center">Status</p></th>

        <th width="100"></th>
        <th width="100"></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${contractSimpleDtos}" var="contractSimpleDto">
        <tr>
            <td><p class="text-center">${contractSimpleDto.phoneNumber}</p></td>
            <td><p class="text-center">${contractSimpleDto.tariff}</p></td>
            <td><p class="text-center">${contractSimpleDto.blockStatus}</p></td>
            <td>
                <button class="btn btn-success" onclick="userContract(this)"
                        value="${contractSimpleDto.id}">SHOW
                </button>
            </td>
            <td><c:choose>
                <c:when test="${contractSimpleDto.blockStatus != 'ADMIN_BLOCKING'}">
                    <button onclick="blockById(this)" value="${contractSimpleDto.id}" type="button" class="btn btn-warning">
                        <c:choose>
                            <c:when test="${contractSimpleDto.blockStatus == 'UNBLOCKING'}">
                                BLOCK
                            </c:when>
                            <c:when test="${contractSimpleDto.blockStatus == 'USER_BLOCKING'}">
                                UNBLOCK
                            </c:when>
                        </c:choose>
                    </button>
                </c:when>
                <c:when test="${contractSimpleDto.blockStatus == 'ADMIN_BLOCKING'}">
                    <button type="button" class="btn btn-warning disabled">UNBLOCK</button>
                </c:when>
            </c:choose>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>