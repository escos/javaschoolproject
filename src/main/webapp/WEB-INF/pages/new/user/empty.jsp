<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${existErr != null}">
    <div class="alert alert-danger">
        <strong>${existErr}</strong>
    </div>
</c:if>
<c:if test="${parseErr != null}">
    <div class="alert alert-danger">
        <strong>${parseErr}</strong>
    </div>
</c:if>
<c:if test="${userErr != null}">
    <div class="alert alert-error">
        <strong>${userErr}</strong>
    </div>
</c:if>
<c:if test="${contractsErr != null}">
    <div class="alert alert-error">
        <strong>${contractsErr}</strong>
    </div>
</c:if>

<c:if test="${numbersErr != null}">
    <div class="alert alert-error">
        <strong>${numbersErr}</strong>
    </div>
</c:if>

<c:if test="${errRemove != null }">
    <div class="alert alert-danger">
        <strong>${errRemove}</strong>
    </div>
</c:if>

<c:if test="${status == -1}">
    <div class="alert alert-danger">
        <strong>Error!</strong> Removing failed.
    </div>
</c:if>
<c:if test="${status == 1 }">
    <div class="alert alert-success">
        <strong>Success!</strong> Contract with number ${number} created.
    </div>
</c:if>
<c:if test="${error != null }">
    <div class="alert alert-error">
        <strong>${error}</strong>
    </div>
</c:if>

<c:if test="${success != null }">
    <div class="alert alert-success">
        <strong>${success}</strong>
    </div>
</c:if>

<c:if test="${userBlock != null }">
    <div class="alert alert-success">
        <strong>${userBlock}</strong>
    </div>
</c:if>

<c:if test="${tariffErr != null }">
    <div class="alert alert-danger">
        <strong>${tariffErr}</strong>
    </div>
</c:if>

<c:if test="${updateSuccess != null}">
    <div class="alert alert-success">
        <strong>${updateSuccess}</strong>
    </div>
</c:if>

<c:if test="${contractErr != null}">
    <div class="alert alert-error">
        <strong>${contractErr}</strong>
    </div>
</c:if>

<c:if test="${optionErr != null }">
    <div class="alert alert-danger">
        <strong>${optionErr}</strong>
    </div>
</c:if>

<c:if test="${connectSucc != null }">
    <div class="alert alert-success">
        <strong>${connectSucc}</strong>
    </div>
</c:if>

<c:if test="${connectErr != null }">
    <div class="alert alert-danger">
        <strong>${connectErr}</strong>
    </div>
</c:if>

<c:if test="${disSucc != null }">
    <div class="alert alert-success">
        <strong>${disSucc}</strong>
    </div>
</c:if>

<c:if test="${disErr != null }">
    <div class="alert alert-success">
        <strong>${disErr}</strong>
    </div>
</c:if>