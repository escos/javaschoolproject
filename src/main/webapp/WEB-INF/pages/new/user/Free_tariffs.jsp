
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div class="panel-heading" style="border-radius: 3px;">
    <span class="lead trf_lead">FREE TARIFFS FOR CONNECTION</span>
</div>
<div class="tariff_container">
    <div class="col-md-9">
        <c:forEach items="${freeTariffs}" var="freeTariff">
            <div class="row tariff_main_form">
                <div class="col-md-5 tar_sect">
                    <h2 class="tariff_plan">${freeTariff.tittle}</h2>
                    <ul class="main_sect_tariff">
                        <li><span>Tariff plan</span></li>
                        <li><span>4G Network</span></li>
                        <li><span>Month-to-month</span></li>
                        <li><span>Included 120 international minutes</span></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="options">Options</h3>
                    <ul class="second_sect_tariff">
                        <c:forEach items="${freeTariff.optionDtos}" var="optionDto">
                            <li><span>${optionDto.tittle}</span></li>
                        </c:forEach>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h2 class="price_tariff">${freeTariff.price} <i class="fa fa-eur"></i></h2>
                    <p style="font-size: 15px;font-family: monospace; color: darkgrey; margin-left: 30px;">
                        per
                        month</p>
                    <c:if test="${blockStatus != 'ADMIN_BLOCKING'}">
                        <button id="change-but" type="button"
                                onclick="changeTariff(this)" value="${freeTariff.id}"
                                class="show_tar_but">CHANGE
                        </button>
                    </c:if>
                </div>
            </div>
        </c:forEach>
    </div>
    <div class="col-md-3">
        <div class="row option_create">
            <div class="lead ">${userLoginDto.surname} ${userLoginDto.name}</div>
            <ul class="first">
                <li><span>Tariff plan:<em>${tariff.tittle}</em></span></li>
                <li><span>Number:<em>${number}</em></span></li>
                <li><span>Status:<em>${blockStatus}</em></span></li>
            </ul>
        </div>
    </div>
</div>