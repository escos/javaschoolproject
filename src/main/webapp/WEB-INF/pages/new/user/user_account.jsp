<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>User</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/user.css"/>
</head>

<body>
<%@include file="../admin/admin_header.jsp" %>
<c:set var="errStatus" scope="session" value="0"/>
<div class="main_content">
    <div class="container">
        <div class="col-md-12">
            <div class="container main_container" id="user_body">

                <div class="tariff_container">
                    <div class="row">
                        <div class="well">
                            <h3>User account</h3>
                        </div>
                    </div>
                    <div class="row base_form">
                        <div class="col-md-9">
                            <div class="generic-container users-right" style="margin: 5px -10px" >
                                <div id="user_table" class="panel panel-default" style="border-radius: 3px;padding: 5px">
                                    <div class="panel-heading" style="border-radius: 3px;">
                                        <span class="lead tittle_table">CONTRACT LIST</span>
                                    </div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th><p class="text-center">Number</p></th>
                                            <th><p class="text-center">Tariff plan</p></th>
                                            <th><p class="text-center">Status</p></th>

                                            <th width="100"></th>
                                            <th width="100"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%--<c:set var="contractId" scope="session" value="${contractId}"/>--%>
                                        <input id="oldId" value="${contractId}" hidden/>
                                        <c:forEach items="${contractSimpleDtos}" var="contractSimpleDto">
                                            <tr>
                                                <td><p class="text-center">${contractSimpleDto.phoneNumber}</p></td>
                                                <td><p class="text-center">${contractSimpleDto.tariff}</p></td>
                                                <td><p class="text-center">${contractSimpleDto.blockStatus}</p></td>
                                                <td>
                                                    <button class="btn btn-success" onclick="userContract(this)"
                                                            value="${contractSimpleDto.id}">SHOW
                                                    </button>
                                                </td>
                                                <td><c:choose>
                                                    <c:when test="${contractSimpleDto.blockStatus != 'ADMIN_BLOCKING'}">
                                                        <button onclick="blockById(this)" value="${contractSimpleDto.id}" type="button" class="btn btn-warning">
                                                            <c:choose>
                                                                <c:when test="${contractSimpleDto.blockStatus == 'UNBLOCKING'}">
                                                                    BLOCK
                                                                </c:when>
                                                                <c:when test="${contractSimpleDto.blockStatus == 'USER_BLOCKING'}">
                                                                    UNBLOCK
                                                                </c:when>
                                                            </c:choose>
                                                        </button>
                                                    </c:when>
                                                    <c:when test="${contractSimpleDto.blockStatus == 'ADMIN_BLOCKING'}">
                                                        <button type="button" class="btn btn-warning disabled">UNBLOCK</button>
                                                    </c:when>
                                                </c:choose>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="row option_create">
                                <div class="lead ">${userLoginDto.name} ${userLoginDto.surname}</div>
                                <ul class="first">
                                    <li><span>Email: <em>${userLoginDto.email}</em></span>
                                    </li>
                                    <li><span >Address: <em>${userLoginDto.address}</em></span>
                                    </li>
                                    <li><span>Passport: <em>${userLoginDto.passport}</em></span></li>
                                    <li><span>BirthDate: <em>${userLoginDto.birthDate}</em></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../footer.jsp" %>
</body>

<script>
    function blockById(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/block/" + contractId,
            success: function (response) {
                $("#user_table").html(response);
            }
        })
    }

    function userContract(obj) {
        var contractId = obj.value;
        var oldId = $("#oldId").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/" + contractId,
            success: function (response) {
                if( oldId !== contractId){
                    $("#basket_badge").text(0);
                }
                $("#user_body").html(response);
            }
        })
    }


</script>
</html>


