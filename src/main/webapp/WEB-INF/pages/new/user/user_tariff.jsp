<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/user_tariff_details.css"/>
</head>
<%@include file="empty.jsp"%>
<main>
    <input id="tab1" type="radio" name="tabs" checked>
    <label for="tab1">User contract details</label>

    <input id="tab2" type="radio" name="tabs">
    <label for="tab2">Free tariffs</label>

    <section id="content1">
        <div class="tariff_container">
            <div class="col-md-9">
                <div id="user_content" class="generic-container users-right" style="border-radius: 5px;">
                    <div class="panel panel-default" style="border-radius: 5px;padding: 5px">
                        <c:if test="${tarSuccess != null }">
                            <div class="alert alert-success">
                                <strong>${tarSuccess}</strong>
                            </div>
                        </c:if>
                        <c:if test="${reqOptsError != null }">
                            <div class="alert alert-danger">
                                <strong>${reqOptsError}</strong>
                            </div>
                        </c:if>
                        <c:if test="${disOptsError != null }">
                            <div class="alert alert-danger">
                                <strong>${disOptsError}</strong>
                            </div>
                        </c:if>
                        <div class="panel-heading" style="border-radius: 10px;"><span class="lead tittle_table">Free options</span>
                        </div>
                        <c:choose>
                            <c:when test="${empty freeTittleOptions}">
                                <p style="text-align: center;margin: 10px;" class="tittle_empty"> NO FREE OPTIONS</p>
                            </c:when>
                            <c:otherwise>
                                <table class="inner_table">
                                    <thead>
                                    <tr>
                                        <th><p class="text-center">Option tittle</p></th>
                                        <th><p class="text-center">Option price, <i class="fa fa-eur"></i></p></th>
                                        <th><p class="text-center">Option cost, <i class="fa fa-eur"></i></p></th>
                                        <th width="100"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:set var="contractId" scope="session" value="${contractId}"/>
                                    <input id="userContract" type="hidden" name="id" value="${contractId}"/>
                                    <c:set var="count" value="0"/>
                                    <c:forEach items="${freeTittleOptions}" var="freeTittleOption">
                                        <c:set var="count" value="${count+1}"/>
                                        <c:if test="${freeTittleOption.isDeleted == true}">
                                            <c:if test="${count != 0}">
                                                <tr>
                                                    <td style="width: 300px">
                                                        <div class="dropdown">
                                                            <i class="fa fa-info-circle"></i>
                                                            <div class="dropdown-content drop">
                                                                <c:choose>
                                                                    <c:when test="${empty freeTittleOption.reqOptions
                                                                    and empty freeTittleOption.disOptions}">
                                                                        <p style="font-size: 20px; color: #4cae4c">
                                                                            OPTION FREE FOR CONNECTION</p>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <p>Required options</p>
                                                                        <ul class="options_list">
                                                                            <c:choose>
                                                                                <c:when test="${empty freeTittleOption.reqOptions }">
                                                                                    <p style="color: #28a4c9">NO
                                                                                        REQUIRED OPTIONS</p>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:forEach
                                                                                            items="${freeTittleOption.reqOptions}"
                                                                                            var="reqOption">
                                                                                        <li>${reqOption}</li>
                                                                                    </c:forEach>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </ul>
                                                                        <p>Incompatible options</p>
                                                                        <ul class="options_list">
                                                                            <c:choose>
                                                                                <c:when test="${empty freeTittleOption.disOptions }">
                                                                                    <li style="color: #28a4c9">NO
                                                                                        INCOMPATIBLE OPTIONS
                                                                                    </li>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:forEach
                                                                                            items="${freeTittleOption.disOptions}"
                                                                                            var="disOption">
                                                                                        <li>${disOption}</li>
                                                                                    </c:forEach>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </ul>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                        </div>
                                                            ${freeTittleOption.tittle}
                                                    </td>
                                                    <td style="width: 100px">${freeTittleOption.price}</td>
                                                    <td style="width: 100px">${freeTittleOption.cost}</td>
                                                    <td>
                                                        <c:if test="${count != 0 and blockStatus != 'ADMIN_BLOCKING'}">
                                                            <button type="button" onclick="addToBasket(this)"
                                                                    value="${freeTittleOption.id}"
                                                                    class="but_opt_add">
                                                                <i class="fa fa-plus-circle"></i>
                                                            </button
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row option_create">
                    <div class="lead ">${userLoginDto.surname} ${userLoginDto.name}</div>
                    <ul class="first">
                        <li><span>Tariff plan:<em>${tariff.tittle}</em></span></li>
                        <li><span>Number:<em>${number}</em></span></li>
                        <li><span>Status:<em>${blockStatus}</em></span></li>
                    </ul>
                </div>
                <c:if test="${connectedOptions.size() != 0}">
                    <div class="row option_create opts">
                        <div class="lead_opts">Connected options</div>
                        <ul class="first first_opts">
                            <c:forEach items="${connectedOptions}" var="connectedOption">
                                <li><span>${connectedOption.tittle}
                                <em><c:if test="${blockStatus != 'ADMIN_BLOCKING'}">
                                    <button type="button" id="disOption" value="${connectedOption.id}"
                                            onclick="disconnectOption(this)"
                                            class="but_opt_add"><i class="fa fa-power-off" aria-hidden="true"></i>
                                    </button>
                                </c:if>
                            </em></span>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </c:if>
            </div>
        </div>
    </section>

    <section id="content2">
        <%@include file="Free_tariffs.jsp"%>
    </section>
</main>


<script src="${pageContext.request.contextPath}/resources/new/js/header.js"></script>
<script>
    function addToBasket(obj) {
        var contractId = $("#userContract").val();
        var optionId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/basket/add",
            data: {id: contractId, optionId: optionId},
            success: function (response) {
                var size = $("#basket_badge").html();
                $.ajax({
                    type: "POST",
                    url: response[1],
                    success: function (data) {
                        $("#user_body").html(data);
                        if (response[0] === "1") {
                            var n = Number(size) + 1;
                            $("#basket_badge").html(n);
                        }
                    }
                });
            }
        })
    }

    function disconnectOption(obj) {
        var id = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/option/disconnect",
            data: {optionDtoId: id},
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#user_body").html(data);
                    }
                });
            }
        });
    }

    function changeTariff(obj) {
        var id = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/tariff/update",
            data: {tariffId: id},
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#user_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        });
    }

    function showTariffs() {
        $("#hide_tariffs").show();
        $("#href_tar").attr("hidden", true);
    }

    $("#basket").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/user/basket",
            success: function (rndmNumber) {
                $("#user_body").html(rndmNumber);
            }
        });
    });


</script>
