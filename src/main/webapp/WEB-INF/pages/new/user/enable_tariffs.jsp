<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="panel panel-default " style="border-radius: 3px;">
    <div class="panel-heading" style="border-radius: 3px;">
        <span class="lead trf_lead">FREE TARIFFS FOR CONNECTION</span>
    </div>
    <table class="table table-hover">
        <tr style="height: 50px; font-size: 20px;">
            <th style="width: 400px;"><p style="text-align: center;">TITTLE</p></th>
            <th style="width: 300px;"><p style="text-align: center;">PRICE</p></th>
            <th style="width: 300px;"><p style="text-align: center;">OPTIONS</p></th>
            <th style="width: 200px;"><p style="text-align: center;"></p></th>
        </tr>
        <c:forEach items="${freeTariffs}" var="freeTariff">
            <tr style="height: 60px;">
                <td><p class="text-center">${freeTariff.tittle}</p></td>
                <td><p class="text-center">${freeTariff.price}</p></td>
                <td>
                    <c:set var="optionDtos" value="${freeTariff.optionDtos}"/>
                    <p class="text-center">
                        <select name="optionDtoId" class="user_options">
                            <c:forEach items="${optionDtos}" var="optionDto">
                                <c:if test="${optionDto.isDeleted == true}">
                                    <option value="${optionDto.id}"
                                            name="optionDtoId">${optionDto.tittle}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </p>
                </td>
                <td style="width: 180px;">
                    <c:choose>
                        <c:when test="${param.blockStatus != 'ADMIN_BLOCKING'}">
                            <button id="change-but" type="button"
                                    onclick="changeTariff(this)" value="${freeTariff.id}"
                                    class="btn btn-success custom-width active">CHANGE TARIFF
                            </button>
                        </c:when>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
    </table>
    <div class="well">
        <div class="container">
            <div class="col-md-6">
                <button type="button" href="${pageContext.request.contextPath}/main/user_account"
                        class="btn btn-primary custom-width active">BACK
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    function changeTariff(obj) {
        var id = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/tariff/update",
            data: {tariffId: id},
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#user_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        });
    }
</script>