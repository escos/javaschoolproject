<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${errRemove != null }">
    <div class="alert alert-danger">
        <strong>${errRemove}</strong>
    </div>
</c:if>
<table class="inner_table">
    <thead>
    <tr>
        <th><p style="color: #69c" class="text-center"><strong>User's email</strong></p></th>
        <th><p style="color: #69c" class="text-center"><strong>Tariff plan</strong></p></th>
        <th><p style="color: #69c" class="text-center"><strong>Phone number</strong></p></th>
        <th><p style="color: #69c" class="text-center"><strong>Time</strong></p></th>

        <th ></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${messages}" var="message">
        <tr>
            <td style="width: 230px"><i class="fa fa-envelope"></i> ${message.email}</td>
            <td style="width: 200px"><i class="fa fa-file-text"></i>${message.tittle}</td>
            <td style="width: 170px"><i class="fa fa-mobile"></i> ${message.number}</td>
            <td style="width: 120px"> ${message.date}</td>
            <td>
                <button type="button" id="disOption" value="${message.id}"
                        onclick="removeMessage(this)"
                        class="but_opt_add"><i class="fa fa-times-circle"></i>
                </button>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script>

    function removeMessages(obj) {
        var messageId = obj.value;
        $.ajax({
            url: "/mymobile/main/message/remove/"+messageId,
            type: "POST",
            success: function (data) {
                $("#mesgs").html(data);
            }
        });
    }
</script>