<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div id="new1">
    <div class="row">
        <div class="col-md-12">
            <h3>The Phone Co-op launches a new Foundation for Co-operative Innovation</h3>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <img src="${pageContext.request.contextPath}/resources/new/img/men_3.jpg" alt="alt">
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p>
                Press release
                The Phone Co-op is launching a new initiative to help drive the development of a more vibrant
                co-operative economy.
                Vivian Woodell has stepped down as The Phone Co-op’s Chief Executive in order to lead the development of
                the new Foundation.
                It is intended hat the Foundation will become an important nexus in the wider co-operative movement and
                a focus for co-operative innovation. It is envisaged that the Foundation will complement and support the
                Co-operatives UK National Co-operative Development Strategy.
                The Phone Co-op’s Board is committed to the new initiative and is delighted that it will be led by
                Vivian Woodell, building on his excellent track record as a co-operative entrepreneur.
                The Phone Co-op’s board has announced that Peter Murley, who has wide-ranging experience in the telecoms
                sector and more widely, will act as the interim Chief Executive. Peter will take up his position later
                this month.
                Robert Denbeigh, The Phone Co-op’s Chair said: “These are very exciting times for The Phone Co-op. The
                Foundation will build upon the success of The Phone Co-op. In particular, we see the Foundation taking
                some of the initiatives we have launched to support the growth of the co-operative model to another
                level.
                The Phone Co-op is a huge success story. Its achievement in securing 19 years of profitable growth is
                something of which we are all very proud. No other consumer co-operative launched in the UK in the last
                50 years has achieved this. The Phone Co-op has won many awards and is widely recognised in the
                co-operative movement,
                nationally and internationally as an example of a successful, innovative co-operative business.
                The Board expresses heartfelt thanks to Vivian for his vision in starting The Phone Co-op and for his
                work in getting The Phone Co-op to where it is now. We know he will bring the same energy and
                imagination to his new role.”
                Vivian Woodell said: “At The Phone Co-op, we set out to demonstrate that a co-operative business can be
                successful because of its values and because it does business differently from its competitors in the
                private sector. Our success proves this is possible.
                My colleagues are an excellent team with great strengths and strong co-operative commitment.
                It has been a privilege to work with them all. I know that The Phone Co-op will continue to flourish in
                the future, building on those same principles and I am meanwhile delighted to be leading the launch of
                the Foundation, which I believe will be able to make a real difference, supporting new co-operative
                solutions.”
            </p>
        </div>
    </div>
</div>