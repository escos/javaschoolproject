<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 14.09.2017
  Time: 5:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="new_1">
    <div class="row">
        <div class="col-md-12">
            <h3>New Beginnings in historic old building for phone company</h3>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <img src="${pageContext.request.contextPath}/resources/new/img/men_3.jpg" alt="alt">
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p>
                After starting life in 1998 in a spare bedroom at founder Vivian Woodell’s home, the Phone
                Co-op is set to move into a landmark building in Chipping Norton’s Market Place.
                The organisation, which now has an annual turnover of £12m and more than 30,000 customers,
                has acquired the former HSBC Bank and the offices over the banking hall and has received
                planning permission from West Oxfordshire District Council
                at the end of last month to modernise the building and construct an extension
                to house its customer service team.
                At the same time the Phone Co-op is restructuring its operations, launching a foundation to stand
                alongside the business, managing investments in sectors other than telecoms and providing a focus for
                innovation in the co-operative movement.
                The foundation will be led by Vivian Woodell, with experienced telecom industry executive Peter Murley
                joining as interim chief executive to manage the phone and broadband business until a permanent appointment is made.
                The move from the Elmsfield Industrial Estate to the centre of Chipping Norton represents a £2m investment by the ethical phone, mobile and broadband provider, which is one of the town’s largest employers, with about 50 staff and 24 more are based in Manchester.
                Phone Co-op chairman Robert Denbeigh said: “We are bursting at the seams here and we want to continue to
                grow. When we heard the building was available, we felt that it would adequately house us with a bit of building work, and be a lot more presentable.
                “It’s also a clear sign that we are staying in Chipping Norton.
                The move represents the end of a long search for new premises in the town, dating back more than a decade,
                with two locations off Station Road and the land allocated for commercial use at the site of the former Parker Knoll furniture factory all considered at various times.
                Work to update the building will start soon, with the first staff expected to move in early next year.
            </p>
        </div>
    </div>
</div>

