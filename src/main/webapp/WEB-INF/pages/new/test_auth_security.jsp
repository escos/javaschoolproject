<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/libs/font-awesome-4.2.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/fonts.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/main.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/login_form.css" />
    <title>authorization</title>
</head>
<body>

<section class="main_content">
    <div class="container">
        <div class="col-md-12">
            <div class="container main_container" >
                <div class="row">
                    <div class="form_container">
                        <section id="cont">
                            <form method="POST" action="/mymobile/main/test_auth" class="form-horizontal">
                                <h1>Login Form</h1>
                                <div>
                                    <input type="email" class="form-control" placeholder="Enter email"
                                           required="" name="email"/>
                                </div>
                                <div>
                                    <input type="password" class="form-control"
                                           required="" placeholder="Enter password" name="password"/>
                                </div>
                                <div>
                                    <input class="but_login" type="submit" value="Log in" />
                                </div>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


</body>

</html>