<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new/newcss/tariff_main.css"/>
</head>

<div class="well">
    <h2>Tariffs and options</h2>
</div>
<c:if test="${problems != null }">
    <div class="alert alert-danger">
        <strong>${problems}</strong>
    </div>
</c:if>
<c:forEach items="${tariffDtos}" var="tariffDto">
    <c:set var="optionDtos" value="${tariffDto.optionDtos}"/>
    <div class="row tariff_main_form">
        <div class="col-md-1">
            <img src="${pageContext.request.contextPath}/resources/new/img/sim.png" alt="alt">
        </div>
        <div class="col-md-4 tar_sect">
            <h2 class="tariff_plan">${tariffDto.tittle}</h2>
            <ul class="main_sect_tariff">
                <li><span>Tariff plan</span></li>
                <li><span>4G Network</span></li>
                <li><span>Month-to-month</span></li>
                <li><span>Included 120 international minutes</span></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h3 class="options">Special options</h3>
            <ul class="second_sect_tariff">
                <c:forEach items="${optionDtos}" var="optionDto">
                    <c:if test="${optionDto.isDeleted == true}">
                        <li><span>${optionDto.tittle} <em>${optionDto.price}
                            <i class="fa fa-eur" aria-hidden="true"></i></em></span>
                        </li>
                    </c:if>
                </c:forEach>
            </ul>
        </div>
        <div class="col-md-2 data_inet">
            <h4 class="inet">1${tariffDto.id}Gb</h4>
            <p> DATA</p>
            <p> Extra Data 10 <i class="fa fa-eur" aria-hidden="true"></i>/1GB</p>
        </div>
        <div class="col-md-2">
            <h2 class="price_tariff">${tariffDto.price} <i class="fa fa-eur" aria-hidden="true"></i></h2>
            <p style="font-size: 14px;font-family: monospace; color: darkgrey; margin: -10px 0 0 20px; ">per month</p>
        </div>
    </div>
</c:forEach>
