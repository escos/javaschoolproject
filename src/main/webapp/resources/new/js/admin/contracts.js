/**
 * Created by Admin on 11.09.2017.
 */
$(document).ready(function () {
    function contractById(obj) {
        var contractId = obj.value;
        var oldId = $("#old_id").html();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/user/contract/" + contractId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
                if (oldId !== contractId) {
                    $("#basket_badge").text(0);
                }
            }
        })
    }

    function blockById(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contract/block/" + contractId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }

    function removeById(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contract/remove/" + contractId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }

    function createContractGet() {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/admin/contract/create",
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    }
});