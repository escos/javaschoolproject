/**
 * Created by Admin on 11.09.2017.
 */
$(document).ready(function () {
    $("#search").submit(function (e) {
        e.preventDefault();
        var number = $("#search");
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/search",
            data: number.serialize(),
            success: function (response) {
                $("#admin_body").html(response);
                $("#search").trigger("reset");
            }
        });
    });

    $.ajax({
        type: "POST",
        url: "/mymobile/main/numbers",
        success: function (response) {
            var temp = JSON.parse(response);
            $("#num").autocomplete({
                source: temp,
                minLength: 2,
                onSelect: function () {
                    location.reload();
                }
            });
        }
    });
});