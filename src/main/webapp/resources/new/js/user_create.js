$(document).ready(function () {

    $("#reg_form").submit(function (e) {
        e.preventDefault();
        var form = $("#reg_form").serialize();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/user/create",
            data: form,
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    });

    $("#cancel").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/admin/user/create",
            success: function (response) {
                $("#admin_body").html(response);
            }

        })
    });

    $("#cancel_contracts").click(function cancel_user() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contracts/",
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    });

    $("#contract").click(function (e) {
        e.preventDefault();
        var form = $("#contract").serialize();
        alert(form);
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contract/create",
            data: form,
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    });

    function createContract(obj) {
        var tariffId = obj.value;
        var number = $("#number_id").val();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contract/create",
            data: {tariffId: tariffId, number: number},
            success: function (data) {
                $("#admin_body").html(data);
            }
        });
    }
});

