/**
 * Created by Admin on 11.09.2017.
 */

$(document).ready(function () {
    $("#callback").submit(function (e) {
        e.preventDefault();
        var number = $("#add_number").val();
        var data = $("#callback").serialize();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/message/save/" + number,
            data: data
        }).done(function () {
            setTimeout(function () {
                $.fancybox.close();
            }, 1000);
        });
        return false;
    });

    function up() {
        $("body, html").animate({
            scrollTop: 0
        }, 800);
        return false;
    }
});