/**
 * Created by Admin on 11.09.2017.
 */
$(document).ready(function () {
    function removeFromBasket(obj) {
        var option = obj.value;
        var size = $("#basket_badge").html();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/basket/option/remove/" + option,
            success: function (response) {
                $("#admin_body").html(response);
                var n = Number(size) - 1;
                $("#basket_badge").text(n);
            }
        })
    }

    function removeFromUserBasket(obj) {
        var option = obj.value;
        var size = $("#basket_badge").html();
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/basket/option/remove/" + option,
            success: function (response) {
                $("#user_body").html(response);
                var n = Number(size) - 1;
                $("#basket_badge").text(n);
            }
        })
    }

    function backToUserContract(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/user/contract/" + contractId,
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    }

    function ToUserContract(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/" + contractId,
            success: function (response) {
                $("#user_body").html(response);
            }
        })
    }

    function backToContracts() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contracts",
            success: function (user) {
                $("#admin_body").html(user);
            }
        });
    }

    function connectOptions() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/options/connect",
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#admin_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        })
    }

    function connectUserOptions() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/options/connect",
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#user_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        })
    }
});