/**
 * Created by Admin on 11.09.2017.
 */
$(document).ready(function () {
    function removeMessages(obj) {
        var messageId = obj.value;
        $.ajax({
            url: "/mymobile/main/message/remove/"+messageId,
            type: "POST",
            success: function (data) {
                $("#mesgs").html(data);
            }
        });
    }

    (function poll() {
        $.ajax({
            url: "/mymobile/main/admin/messages",
            type: "GET",
            success: function (data) {
                $("#mesgs").html(data);
            },
            complete: setTimeout(function () {
                poll()
            }, 300000),
            timeout: 50000
        })
    })();

    function removeMessage(obj) {
        var messageId = obj.value;
        $.ajax({
            url: "/mymobile/main/admin/message/remove/"+messageId,
            type: "POST",
            success: function (data) {
                $("#mesgs").html(data);
            }
        })
    }
});