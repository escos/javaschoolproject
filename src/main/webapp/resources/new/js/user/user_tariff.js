/**
 * Created by Admin on 11.09.2017.
 */
$(document).ready(function () {
    function addToBasket(obj) {
        var contractId = $("#userContract").val();
        var optionId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/basket/add",
            data: {id: contractId, optionId: optionId},
            success: function (response) {
                var size = $("#basket_badge").html();
                $.ajax({
                    type: "POST",
                    url: response[1],
                    success: function (data) {
                        $("#user_body").html(data);
                        if (response[0] === "1") {
                            var n = Number(size) + 1;
                            $("#basket_badge").html(n);
                        }
                    }
                });
            }
        })
    }

    function disconnectOption(obj) {
        var id = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/option/disconnect",
            data: {optionDtoId: id},
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#user_body").html(data);
                    }
                });
            }
        });
    }

    function changeTariff(obj) {
        var id = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/tariff/update",
            data: {tariffId: id},
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (data) {
                        $("#user_body").html(data);
                        $("#basket_badge").text(0);
                    }
                });
            }
        });
    }

    function showTariffs() {
        $("#hide_tariffs").show();
        $("#href_tar").attr("hidden", true);
    }

    $("#basket").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/user/basket",
            success: function (rndmNumber) {
                $("#user_body").html(rndmNumber);
            }
        });
    });


});
