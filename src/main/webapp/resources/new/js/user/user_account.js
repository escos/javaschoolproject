/**
 * Created by Admin on 11.09.2017.
 */
$(document).ready(function () {
    function blockById(obj) {
        var contractId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/block/" + contractId,
            success: function (response) {
                $("#user_table").html(response);
            }
        })
    }

    function userContract(obj) {
        var contractId = obj.value;
        alert(contractId);
        var oldId = $("#oldId").val();
        alert(oldId);
        $.ajax({
            type: "POST",
            url: "/mymobile/main/user/contract/" + contractId,
            success: function (response) {
                if( oldId !== contractId){
                    $("#basket_badge").text(0);
                }
                $("#user_body").html(response);
            }
        })
    }
});