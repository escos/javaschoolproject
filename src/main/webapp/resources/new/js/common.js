$(document).ready(function () {

    $(".auth_buttons").click(function () {
        $(this).next().slideToggle();
    });

    $(".main_mnu_button").click(function () {
        $(".maian_mnu ul").slideToggle();
    });

    //Попап менеджер FancyBox

    $(".fancybox").fancybox();

    //Навигация по Landing Page
    //$(".top_mnu") - это верхняя панель со ссылками.
    //Ссылки вида <a href="#contacts">Контакты</a>
    $(".top_mnu").navigation();

    //Добавляет классы дочерним блокам .block для анимации

    $(".block").waypoint(function (direction) {
        if (direction === "down") {
            $(".class").addClass("active");
        } else if (direction === "up") {
            $(".class").removeClass("deactive");
        }
    }, {offset: 100});

    //Плавный скролл до блока .div по клику на .scroll

    $("a.scroll").click(function () {
        $.scrollTo($(".div"), 800, {
            offset: -90
        });
    });

    //Каруселька

    var owl = $(".carousel");
    owl.owlCarousel({
        items: 1,
        autoHeight: true
    });
    owl.on("mousewheel", ".owl-wrapper", function (e) {
        if (e.deltaY > 0) {
            owl.trigger("owl.prev");
        } else {
            owl.trigger("owl.next");
        }
        e.preventDefault();
    });
    $(".next_button").click(function () {
        owl.trigger("owl.next");
    });
    $(".prev_button").click(function () {
        owl.trigger("owl.prev");
    });


    //Кнопка "Наверх"



    $("#number").click(function () {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/number",
            success: function (rndmNumber) {
                $("#target").html(rndmNumber);
            }
        });
    });

    $("#view_number").click(function () {
        var num = $("#target").html();
        $("#add_number").val(num).prop("disabled", true);
    });

    //Аякс выгрузка тарифов на главную

    $("#lk").click(function () {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/tariffs",
            success: function (rndmNumber) {
                $("#content").html(rndmNumber);
            }
        });
    });

    //Аякс выгрузка юзеров

    $("#users").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/admin/users",
            success: function (user) {
                $("#admin_body").html(user);
            }
        });
    });

    //Аякс выгрузка контрактов

    $("#contracts").click(function () {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contracts",
            success: function (user) {
                $("#admin_body").html(user);
            }
        });
    });

    $("#inc_messages").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/main_messages",
            success: function (messages) {
                $("#admin_body").html(messages);
            }
        });
    });


    //Аякс выгрузка формы регистрации

    $("#create").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/admin/user/create",
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        });
    });

    //загрузка тарифов в формы

    $("#admin_tariffs").click(function () {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariffs",
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        });
    });

    //форма создания тарифа

    $("#create_tariff").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/admin/tariff/create",
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    });

    $("#basket").click(function () {
        $.ajax({
            type: "GET",
            url: "/mymobile/main/user/basket",
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        });
    });

    $("#user_contracts").click(function () {
        var userId = $("#user_contracts").value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/contracts/" + userId,
            success: function (rndmNumber) {
                $("#admin_body").html(rndmNumber);
            }
        })
    });

    $("#archive").click(function archive() {
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariff_archive",
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    });

});

