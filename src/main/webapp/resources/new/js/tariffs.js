/**
 * Created by Admin on 11.09.2017.
 */
$(document).ready(function () {
    function getTariff(obj) {
        var tariffId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariff/" + tariffId,
            success: function (response) {
                $("#admin_body").html(response);
            }
        })
    }

    function removeTariff(obj) {
        var tariffId = obj.value;
        $.ajax({
            type: "POST",
            url: "/mymobile/main/admin/tariff/remove/" + tariffId,
            success: function (response) {
                $.ajax({
                    type: "POST",
                    url: response,
                    success: function (response) {
                        $("#admin_body").html(response);
                    }
                })
            }
        })
    }
});